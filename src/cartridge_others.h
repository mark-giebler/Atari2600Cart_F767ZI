/*
 * cartridge_others.h
 *
 *  Created on: Dec 29, 2023
 *      Author: mgiebler
 *
 * Additional emulators from cartPlus code.
 *
 * From CartPlus project:
 *  Created on: 03.11.2019
 *      Author: stubig
 */

#ifndef CARTRIDGE_OTHERS_H_
#define CARTRIDGE_OTHERS_H_

#include <stdint.h>
#include <stdbool.h>
#include "global.h"

enum Transmission_State{
	No_Transmission,
	Send_Start,
	Send_Prepare_Header,
	Send_Header,
	Send_Content,
	Send_Finished,
	Receive_Header,
	Receive_Length,
	Receive_Content,
	Receive_Finished
};

// _mg_ Dummy out WiFi only functions:
#define process_transmission()
#ifdef USE_WIFI
//Todo make setup_plus_rom_functions empty if no WiFi
#define setup_plus_rom_functions() \
		uint8_t receive_buffer_write_pointer = 0, receive_buffer_read_pointer = 0, content_counter = 0; \
		uint8_t out_buffer_write_pointer = 0, out_buffer_send_pointer = 0; \
		uint8_t receive_buffer[256], out_buffer[256]; \
		uint8_t prev_c = 0, prev_prev_c = 0, i, c;	\
		uint16_t content_len;						\
		int content_length_pos = header_length - 5; \
		enum Transmission_State huart_state = No_Transmission; \

#else
#define setup_plus_rom_functions()
#endif
// _mg_ FIXME: This is a duplicate of structure defined in cp_cartridge_setup.h. It is different from UnoCart one in emulator.c files. Merge them together?
typedef struct {
    uint8_t* banks[64];
} cartridge_layout;

bool plus_setup_cartridge_image(const char*, uint32_t, uint8_t*, cartridge_layout* , MENU_ENTRY *, enum cart_base_type  );

/* 'Standard' Bankswitching */
void emulate_standard_cartridge( int header_length, bool withPlusFunctions, uint16_t lowBS, uint16_t highBS, int isSC );

void emulate_UA_cartridge( uint32_t cart_size_bytes, uint8_t *buffer );
void emulate_ar_cartridge( const char* filename, uint32_t image_size, uint8_t* buffer, uint8_t tv_mode, MENU_ENTRY* menu_entry_d );
void emulate_SB_cartridge( const char* filename, uint32_t image_size, uint8_t* buffer, MENU_ENTRY *d  );

// _mg_  original cartPlus prototypes:  I reverted them back to original UnoCart arguments and functions.


/* FA (CBS RAM plus) Bankswitching */
//void emulate_FA_cartridge(int, bool);

/* FE Bankswitching */
//void emulate_FE_cartridge();

/* 3F (Tigervision) Bankswitching */
//void emulate_3F_cartridge();

/* 3E (3F + RAM) Bankswitching */
//void emulate_3E_cartridge(int, bool);

/* 3E+ Bankswitching by Thomas Jentzsch */
//void emulate_3EPlus_cartridge(int, bool);

/* E0 Bankswitching */
//void emulate_E0_cartridge();

//void emulate_0840_cartridge();

/* CommaVid Cartridge*/
//void emulate_CV_cartridge();

/* F0 Bankswitching */
//void emulate_F0_cartridge();

/* E7 Bankswitching */
//void emulate_E7_cartridge();

/* DPC (Pitfall II) Bankswitching */
//void emulate_DPC_cartridge(uint32_t);

/* Pink Panther */
//void emulate_pp_cartridge(uint8_t* ram);

#endif /* CARTRIDGE_OTHERS_H_ */
