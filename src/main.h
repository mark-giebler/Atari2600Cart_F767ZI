/*
 * main.h
 *
 *  Created on: Mar 26, 2022
 *      Author: mgiebler
 */

#ifndef MAIN_H_
#define MAIN_H_


#include <mgDebugHelper.h>
#include "stm32f7xx_hal.h"
#include "bsp.h"
#include "global.h"	// for CARTPLUS_ENABLE


/* _mg_ Override fs_utils.h defaults */
#ifndef CARTPLUS_ENABLE
/* _mg_ made long file name storage configurable. Note: CartPlus uses the default of 255. */
#define LONG_FILENAME_LEN	(72)

#define NUM_DIR_ITEMS	80	// Todo: _mg_ This crashed 6502 code -> make this larger 256?
							// _mg_ This worked -> could go to 85 and still fit in menu_ram[] array. See cartridge_firmware.c and readDirectoryForAtari()
							// _mg_ menu_ram[] MUST be 1024 in size, the 6502 code uses this array as a 1K virtual RAM.
/* Override short filename length to match what the Atari ROM supports. */
#define ATARICART_NAME_LEN	(12)
#define SHORT_FILENAME_LEN	(ATARICART_NAME_LEN)
#else	// CartPlus:
/* _mg_ made long file name storage configurable.  Note: CartPlus uses the default of 255. */
#define LONG_FILENAME_LEN	(255)

#define NUM_DIR_ITEMS	80	// Todo: _mg_ This crashed 6502 code -> make this larger 256?
							// _mg_ This worked -> could go to 85 and still fit in menu_ram[] array. See cartridge_firmware.c and readDirectoryForAtari()
							// _mg_ menu_ram[] MUST be 1024 in size, the 6502 code uses this array as a 1K virtual RAM.
/* Override short filename length to match what the Atari ROM supports. */
#define ATARICART_NAME_LEN	(12)
#define SHORT_FILENAME_LEN	(ATARICART_NAME_LEN)

#endif

/* _mg_ Override uart.h defaults */
#define ENABLE_STDIO_SUPPORT	1

//_mg_ had trouble with the TM_... code on the F767 - issue with the SPI peripheral not configuring correctly. Stopped work on debugging it and
//_mg_ I replaced the TM_.. code with the SD card code from here:  https://01001000.xyz/2020-08-09-Tutorial-STM32CubeIDE-SD-card/
#define nCS_Pin GPIO_PIN_12
#define nCS_GPIO_Port GPIOB

// used in user_diskio_spi.h
#define SD_CS_GPIO_Port	nCS_GPIO_Port
#define SD_CS_Pin		nCS_Pin
#define SD_SPI_HANDLE	hspi2



#define A0_Pin GPIO_PIN_0
#define A0_GPIO_Port GPIOF
#define A1_Pin GPIO_PIN_1
#define A1_GPIO_Port GPIOF
#define A2_Pin GPIO_PIN_2
#define A2_GPIO_Port GPIOF
#define A3_Pin GPIO_PIN_3
#define A3_GPIO_Port GPIOF
#define A4_Pin GPIO_PIN_4
#define A4_GPIO_Port GPIOF
#define A5_Pin GPIO_PIN_5
#define A5_GPIO_Port GPIOF
#define A6_Pin GPIO_PIN_6
#define A6_GPIO_Port GPIOF
#define A7_Pin GPIO_PIN_7
#define A7_GPIO_Port GPIOF
#define A8_Pin GPIO_PIN_8
#define A8_GPIO_Port GPIOF
#define A9_Pin GPIO_PIN_9
#define A9_GPIO_Port GPIOF
#define A10_Pin GPIO_PIN_10
#define A10_GPIO_Port GPIOF
#define A11_Pin GPIO_PIN_11
#define A11_GPIO_Port GPIOF
#define A12_Pin GPIO_PIN_12
#define A12_GPIO_Port GPIOF
#define A13_dummy_Pin GPIO_PIN_13
#define A13_dummy_GPIO_Port GPIOF
#define A14_dummy_Pin GPIO_PIN_14
#define A14_dummy_GPIO_Port GPIOF
#define A15_dummy_Pin GPIO_PIN_15
#define A15_dummy_GPIO_Port GPIOF

#define DB0_Pin GPIO_PIN_0
#define DB0_GPIO_Port GPIOD
#define DB1_Pin GPIO_PIN_1
#define DB1_GPIO_Port GPIOD
#define DB2_Pin GPIO_PIN_2
#define DB2_GPIO_Port GPIOD
#define DB3_Pin GPIO_PIN_3
#define DB3_GPIO_Port GPIOD
#define DB4_Pin GPIO_PIN_4
#define DB4_GPIO_Port GPIOD
#define DB5_Pin GPIO_PIN_5
#define DB5_GPIO_Port GPIOD
#define DB6_Pin GPIO_PIN_6
#define DB6_GPIO_Port GPIOD
#define DB7_Pin GPIO_PIN_7
#define DB7_GPIO_Port GPIOD

#define PAL60_Pin GPIO_PIN_0
#define PAL60_GPIO_Port GPIOG
#define PAL_Pin GPIO_PIN_1
#define PAL_GPIO_Port GPIOG

#define DB0_alt_Pin GPIO_PIN_8
#define DB0_alt_GPIO_Port GPIOE
#define DB1_alt_Pin GPIO_PIN_9
#define DB1_alt_GPIO_Port GPIOE
#define DB2_alt_Pin GPIO_PIN_10
#define DB2_alt_GPIO_Port GPIOE
#define DB3_alt_Pin GPIO_PIN_11
#define DB3_alt_GPIO_Port GPIOE
#define DB4_alt_Pin GPIO_PIN_12
#define DB4_alt_GPIO_Port GPIOE
#define DB5_alt_Pin GPIO_PIN_13
#define DB5_alt_GPIO_Port GPIOE
#define DB6_alt_Pin GPIO_PIN_14
#define DB6_alt_GPIO_Port GPIOE
#define DB7_alt_Pin GPIO_PIN_15
#define DB7_alt_GPIO_Port GPIOE


#define STLK_RX_Pin GPIO_PIN_8
#define STLK_RX_GPIO_Port GPIOD
#define STLK_TX_Pin GPIO_PIN_9
#define STLK_TX_GPIO_Port GPIOD

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);
void Error_Handler_Blink(uint32_t blinks);

void myprintf(const char *fmt, ...);

// emulators that are located in main.c
void emulate_2k_cartridge();
void emulate_4k_cartridge();
void emulate_4ksc_cartridge();
void emulate_FxSC_cartridge(uint16_t lowBS, uint16_t highBS, int isSC);
void emulate_FA_cartridge();
void emulate_FE_cartridge();
void emulate_E0_cartridge();
void emulate_0840_cartridge();
void emulate_CV_cartridge();
void emulate_F0_cartridge();
void emulate_E7_cartridge();


#endif /* MAIN_H_ */
