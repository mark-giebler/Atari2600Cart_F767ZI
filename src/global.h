/**
 * @file globals.h
 *
 *  Created on: Dec 27, 2023
 *      Author: mgiebler
 *
 * From CartPlus project.
 *
 * These are mostly CartPlus related macros and prototypes.
 */

#ifndef GLOBAL_H_
#define GLOBAL_H_

#include <stdint.h>
#include <stdbool.h>

// define		CARTPLUS_ENABLE		to enable cartPlus features. Todo: _mg_ Move to IDE build option.
//#define 		CARTPLUS_ENABLE


// define		KEYBOARD_ENABLE		to enable keyboard items in cartPlus code.
//#define		KEYBOARD_ENABLE

// define		USE_SD_CARD			to enable SD Card in cartPlus code. No effect in UnoCart code.
#define 		USE_SD_CARD


// define		BUS_DUMPER		Have DIP switch HW to select to capture power up address sequence of the 6507 to a file on the sd card named: /bus_dump.bin
//#define 		BUS_DUMPER

// define		BUS_DUMPER_NOPS	to enable driving 6502 NOP on the data bus during bus dumper capture
//#define 		BUS_DUMPER_NOPS

// define		ATARI_POWER_HW  if Atari power checking hardware is available.
#define 		ATARI_POWER_HW

// define		NTSC_ONLY		to only support NTSC and remove all PAL code to save execution time.
#define 		NTSC_ONLY

//#define PLUSSTORE_API_HOST        "pluscart.firmaplus.de"

#define CHARS_PER_LINE					32	// 5 pixels wide per char, 160 total pixels in a line.
#define STATUS_MESSAGE_LENGTH           256

#define NUM_MENU_ITEMS			      	1024

#define MENU_TEXT_GO_BACK                   "(Go Back)"
#define MENU_TEXT_DELETE_CHAR               "Delete Character"
#define MENU_TEXT_SD_CARD_CONTENT           "SD-Card Content"
//#define MENU_TEXT_OFFLINE_ROMS              "Offline ROMs"	// _mg_ offline ROMS are wifi downloaded and stored in MCU FLASH.
//#define MENU_TEXT_DETECT_OFFLINE_ROMS       "Detect Offline ROMs"
//#define MENU_TEXT_DELETE_OFFLINE_ROMS       "Erase Offline ROMs"
#define MENU_TEXT_SETUP                     "Setup"
//#define MENU_TEXT_WIFI_SETUP                "WiFi Connection"
//#define MENU_TEXT_WIFI_SELECT               "Scan for WiFi Access Points"
//#define MENU_TEXT_WIFI_WPS_CONNECT          "WiFi WPS Connect"
//#define MENU_TEXT_WIFI_MANAGER              "Start WiFi Manager Portal"
//#define MENU_TEXT_ESP8266_RESTORE           "Delete WiFi Connections"
//#define MENU_TEXT_ESP8266_UPDATE            "WiFi Firmware Update"
#define MENU_TEXT_APPEARANCE                "Appearance"
#define MENU_TEXT_DISPLAY                   "Display Preferences"

//#define MENU_TEXT_WIFI_RECONNECT            "WiFi Retry"
#define MENU_TEXT_TV_MODE_SETUP             "TV Mode"
#define MENU_TEXT_TV_MODE_NTSC              "  NTSC"
#define MENU_TEXT_TV_MODE_PAL               "  PAL"
#define MENU_TEXT_TV_MODE_PAL60             "  PAL 60 Hz"
#define MENU_TEXT_FONT_SETUP                "Font Style"
#define MENU_TEXT_FONT_TJZ                  "  Small Caps"
#define MENU_TEXT_FONT_TRICHOTOMIC12        "  Trichotomic-12"
#define MENU_TEXT_FONT_CAPTAIN_MORGAN_SPICE "  Captain Morgan Spice"
#define MENU_TEXT_FONT_GLACIER_BELLE        "  Glacier Belle"

#define MENU_TEXT_FORMAT_SD_CARD            "Format SD-Card"
#define MENU_TEXT_FORMAT_EEPROM             "Format User Settings"

#define MENU_TEXT_SPACING_SETUP             "Line Spacing"
#define MENU_TEXT_SPACING_DENSE             "  Dense"
#define MENU_TEXT_SPACING_MEDIUM            "  Regular"
#define MENU_TEXT_SPACING_SPARSE            "  Sparse"

//#define MENU_TEXT_PRIVATE_KEY               "Private Key"
//#define MENU_TEXT_FIRMWARE_UPDATE           "** WiFi Firmware Update **"
#define MENU_TEXT_SD_FIRMWARE_UPDATE        "** SD-Card Firmware Update **"
//#define MENU_TEXT_OFFLINE_ROM_UPDATE        "Download Offline ROMs"
//#define MENU_TEXT_PLUS_CONNECT              "PlusStore Connect"
//#define MENU_TEXT_PLUS_REMOVE               "PlusStore Disconnect"

#define MENU_TEXT_SYSTEM_INFO               "System Info"
#define MENU_TEXT_SEARCH_FOR_ROM            "Search ROM"
#define MENU_TEXT_SPACE                     "Space"
#define MENU_TEXT_LOWERCASE                 "Lowercase"
#define MENU_TEXT_UPPERCASE                 "Uppercase"
#define MENU_TEXT_SYMBOLS                   "Symbols"


#define URLENCODE_MENU_TEXT_SYSTEM_INFO     "System%20Info"
//#define URLENCODE_MENU_TEXT_PLUS_CONNECT    "PlusStore%20Connect"
#define URLENCODE_MENU_TEXT_SETUP           "Setup"

#define AUTOSTART_FILENAME_PREFIX           "Autostart."

#define PATH_SEPERATOR '/' /*CHAR_SELECTION*/

#define FIRMWARE_MAX_RAM                    0x1C000	// 114688 := 112K

//#define SIZEOF_WIFI_SELECT_BASE_PATH        sizeof(MENU_TEXT_SETUP) + sizeof(MENU_TEXT_WIFI_SETUP) + sizeof(MENU_TEXT_WIFI_SELECT)

#ifdef USE_WIFI
extern UART_HandleTypeDef huart1;
#endif
extern char http_request_header[];
extern char stm32_udid[];

extern uint8_t buffer[];
extern unsigned int cart_size_bytes;

enum eStatus_bytes_id {
	STATUS_StatusByteReboot,
	STATUS_CurPage,
	STATUS_MaxPage,
	STATUS_ItemsOnActPage,
	STATUS_PageType,
	STATUS_Unused1,
	STATUS_Unused2,

	STATUS_MAX
};

enum eStatus_bytes_PageTypes {
	Directory,
	Menu,
	Keyboard
};

enum MENU_ENTRY_Type {
	Root_Menu = -1,
	Leave_Menu,
	Sub_Menu,  // should be PlusStore or WiFi_Sub_Menu
	Cart_File,
	Input_Field,
//	Keyboard_Char,
//	Keyboard_Row,
	Menu_Action,
//	Delete_Keyboard_Char,
	Offline_Cart_File,  // should be Flash_Sub_Menu
	Offline_Sub_Menu,  // should be Flash_Sub_Menu
	Setup_Menu,
//	Leave_SubKeyboard_Menu,
	SD_Cart_File,
	SD_Sub_Menu,
};

enum cart_base_type{
	base_type_Load_Failed = -1,
	base_type_None,	// 0
	base_type_2K,
	base_type_4K,
	base_type_4KSC,	// 4k+SC
	base_type_F8,	// 8k
	base_type_F6,	// 16k
	base_type_F4,	// 32k
	base_type_F8SC,	// 8k+ram	_mg_
	base_type_F6SC,	// 16k+ram	_mg_
	base_type_F4SC,	// 32k+ram	_mg_
	base_type_FE,	// 8k
	base_type_3F,	// varies (examples 8k)
	base_type_3E,	// varies (only example 32k)

	base_type_E0,	// 8k
	base_type_0840,	// 8k
	base_type_CV,	// 2k+ram
	base_type_EF,	// 64k
	base_type_F0,	// 64k+ram
	base_type_FA,	// 12k
	base_type_E7,	// 16k+ram
	base_type_DPC,	// 8k+DPC(2k)
	base_type_AR,	// Arcadia Supercharger (variable size)
	base_type_PP,	// Pink Panther Prototype
	base_type_DF,
	base_type_DFSC,
	base_type_BF,
	base_type_BFSC,
	base_type_3EPlus,// 3E+ 1-64K + 32K ram
	base_type_DPCplus,
	base_type_SB,
	base_type_UA,	// Brazil

	base_type_ACE	// ARM Custom Executable
};

typedef struct {	// _mg_ reorganized order of declaration to minimize structure size. 52 -> 48 bytes.
	enum MENU_ENTRY_Type type;
	uint8_t font;
	char entryname[CHARS_PER_LINE+1];	// _mg_ name to display to user via Atari screen gen.
	uint32_t filesize;
	uint32_t flash_base_address;
	char* full_filename;				// _mg_ Added to point to allocated memory with full file name for display names that had to be truncated.
} MENU_ENTRY;

typedef struct {
	uint8_t tv_mode;
	uint8_t first_free_flash_sector;
	uint8_t font_style;
	uint8_t line_spacing;
} USER_SETTINGS;


extern USER_SETTINGS user_settings;
extern const uint8_t numMenuItemsPerPage[];

// For PlusCart code.  Originally MENU_TYPE was defined in the IDE settings.  Todo: _mg_ Put this and CARTPLUS_ENABLE in IDE settings
/* Menu Type: UNOCART		(1)  Or  CARTPLUS	(2) */
#define MENU_TYPE	(2)


#ifdef CARTPLUS_ENABLE
// items in this conditional need to be hidden when building original UnoCart code.

// _mg_ macros for backward compatibility to UnoCart code.
#define CART_TYPE_NONE	(base_type_None)


//#define BUFFER_SIZE				96   // kilobytes // _mg_ duplicate. Defined in ram_storage.h
// for BUFFER_SIZE & CCM_RAM macros see ram_storage.h


// _mg_ This enum must be manually coordinated with the strings in status_message[]
enum e_status_message {
	STATUS_NONE = -2,
	STATUS_MESSAGE_STRING,
	STATUS_ROOT,
//	select_wifi_network,
//	wifi_not_connected,
//	wifi_connected,
//	esp_timeout,
//	insert_password,
//	plus_connect,
//	STATUS_YOUR_MESSAGE,
	offline_roms_deleted,
	not_enough_menory,
	romtype_ACE_unsupported,
	romtype_unknown,
	done,
	failed,
	download_failed,
	offline_roms_detected,
	no_offline_roms_detected,
	romtype_DPCplus_unsupported,
	exit_emulation,
	rom_download_failed,

	STATUS_SETUP,
	STATUS_SETUP_TV_MODE,
	STATUS_SETUP_FONT_STYLE,
	STATUS_SETUP_LINE_SPACING,
	STATUS_SETUP_SYSTEM_INFO,
	STATUS_SEARCH_FOR_ROM,
	STATUS_SEARCH_DETAILS,
	STATUS_CHOOSE_ROM,

	STATUS_APPEARANCE,

};

int cp_main(void);	// main entry point for cartPlus

#endif	// CARTPLUS_ENABLE



#endif /* GLOBAL_H_ */
