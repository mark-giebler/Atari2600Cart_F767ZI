#include <string.h>

#include "cartridge_firmware.h"

#include "firmware_pal_rom.h"
#include "firmware_pal60_rom.h"
#include "firmware_ntsc_rom.h"

#include "main.h"		// For CARTPLUS_ENABLE and other macros, etc.


#ifndef CARTPLUS_ENABLE
// Todo: _mg_ do we need to increase menu_ram[] if NUM_DIR_ITEMS dir entries increased?
// _mg_ tried NUM_DIR_ITEMS at 200 with a larger menu_ram[] and the 6502 code crashes when entering a large depth directory.
// _mg_ menu_ram[] MUST be 1024 in size, it is used as a 1K RAM for the 6502 code to access directly. Todo: _mg_ Move menu_ram to CCMRAM for speed increase?
__attribute__ ((section (".noinit")))  static unsigned char menu_ram[1024];	// _mg_ put this in .noinit section
__attribute__ ((section (".noinit")))  static char menu_status[16];		// _mg_ put this in .noinit section.
static unsigned const char *firmware_rom = firmware_ntsc_rom;

void set_menu_status_msg(const char* message) {
	strncpy(menu_status, message, 15);
}

void set_menu_status_byte(char status_byte) {
	menu_status[15] = status_byte;
}

void set_tv_mode(int tv_mode) {
	switch (tv_mode) {
		case TV_MODE_NTSC:
			firmware_rom = firmware_ntsc_rom;
			break;

		case TV_MODE_PAL:
			firmware_rom = firmware_pal_rom;
			break;

		case TV_MODE_PAL60:
			firmware_rom = firmware_pal60_rom;
			break;
	}
}

/**
 * Return a pointer to the menu array.
 */
uint8_t* get_menu_ram() {
	return menu_ram;
}

/**
 * Return size of menu array in units of bytes.
 */
size_t get_menu_ram_size()
{
	// _mg_ function to allow changing array size
	return sizeof(menu_ram)/sizeof(char);
}

/**
 * @brief Exit an emulated cartridge and back to the UnoCart firmware cartridge.
 * Used in conjunction with the cartridge emulation escape method @ref check_if_escape_from_cart().
 *
 * From: CartPlus code.
 */
void exit_cartridge(uint16_t addr)
{
	DATA_OUT = DATA_OUT_SHIFT(0xEA);		// (NOP) or data for SWCHA/SWCHB
	SET_DATA_MODE_OUT;
	while (ADDR_IN == addr);

	addr = ADDR_IN;
	DATA_OUT = DATA_OUT_SHIFT(0x00);		// (BRK) Fetch next exec address from $FFFE
	while (ADDR_IN == addr);				// Todo: _mg_ Modifiy UnoCart firmware to have vector at $FFFE same as RESET vector

	emulate_firmware_cartridge();
}

// We require the menu to do a write to $1FF4 to unlock the comms area.
// This is because the 7800 bios accesses this area on console startup, and we wish to ignore these
// spurious reads until it has started the cartridge in 2600 mode.
bool comms_enabled = false;

int emulate_firmware_cartridge() {
	__disable_irq();	// Disable interrupts
	register __IO uint32_t * p_addr_in = &ADDR_IN;
	register uint16_t addr = *p_addr_in; //ADDR_IN;
	register uint16_t addr_prev = 0xffff;

	// _mg_ UnoCart: added GPIO monitor for Atari 2600 5v rail. loop until 5v on.
	wait_atari_power_on();
	// show progress of reset with LEDs.  Assume NOP is on the bus right now.
	// blue off == 0x1ffc  hit

	while (1)
	{
		while ((addr = ADDR_IN) != addr_prev)
			addr_prev = addr;
		// got a stable address
		if (addr & 0x1000)
		{ // A12 high
			if (comms_enabled)
			{	// normal mode, once the cartridge code has done its init.
				// on a 7800, we know we are in 2600 mode now.
				if ((addr & 0x1F00) == 0x1E00)
					break;	// atari 2600 has sent a command
				if (addr >= 0x1800 && addr < 0x1C00)
					DATA_OUT = DATA_OUT_SHIFT(((uint16_t)menu_ram[addr&0x3FF]));
				else if ((addr & 0x1FF0) == CART_STATUS_BYTES)
					DATA_OUT = DATA_OUT_SHIFT(((uint16_t)menu_status[addr&0x0F]));
				else
					DATA_OUT = DATA_OUT_SHIFT(((uint16_t)firmware_rom[addr&0x0FFF]));
				SET_DATA_MODE_OUT
				// wait for address bus to change
				while (ADDR_IN == addr) ;
				SET_DATA_MODE_IN
			}
			else
			{	// prior to an access to $1FF4, we might be running on a 7800 with the CPU at
				// ~1.8MHz so we've got less time than usual - keep this short.
				DATA_OUT = DATA_OUT_SHIFT(((uint16_t)firmware_rom[addr&0x0FFF]));
				SET_DATA_MODE_OUT
				// wait for address bus to change
				while (ADDR_IN == addr) ;
				SET_DATA_MODE_IN

				if (addr == 0x1FF4)
					comms_enabled = true;
			}
		}
	}

	__enable_irq();
	return addr;
}

// _mg_ for cartPlus mode this is replaced with version in cartridge_firware_plus.c
bool reboot_into_cartridge() {
	set_menu_status_byte(1);
	return emulate_firmware_cartridge() == CART_CMD_START_CART;
}

#endif	// CARTPLUS_ENABLE
