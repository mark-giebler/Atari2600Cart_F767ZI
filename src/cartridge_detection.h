/**
 * @file cartridge_detection.h
 *
 *  Created on: Dec 29, 2023
 *      Author: mgiebler
 *
 *
 * Make code more modular: Locate the UnoCart detection functions in one file.
 */

#ifndef CARTRIDGE_DETECTION_H_
#define CARTRIDGE_DETECTION_H_

int searchForBytes(unsigned char *bytes, int size, unsigned char *signature, int sigsize, int minhits);
int isProbablySC(int size, unsigned char *bytes);
int isProbablyFE(int size, unsigned char *bytes);
int isProbably3F(int size, unsigned char *bytes);
int isProbably3E(int size, unsigned char *bytes);
int isProbably3EPlus(int size, unsigned char *bytes);
int isProbablyE0(int size, unsigned char *bytes);
int isProbably0840(int size, unsigned char *bytes);
int isProbablyCV(int size, unsigned char *bytes);
int isProbablyEF(int size, unsigned char *bytes);
int isProbablyE7(int size, unsigned char *bytes);
int isProbablyBF(unsigned char *tail);
int isProbablyBFSC(unsigned char *tail);
int isProbablyDF(unsigned char *tail);
int isProbablyDFSC(unsigned char *tail);
int isProbably4KSC(unsigned char *bytes);
int isProbablyPLS(unsigned int size, unsigned char *bytes);
int isPotentialF8(unsigned int size, unsigned char *bytes);
int isProbablyUA(unsigned int size, unsigned char *bytes);
int isProbablyDPCplus(unsigned int size, unsigned char *bytes);

#endif /* CARTRIDGE_DETECTION_H_ */
