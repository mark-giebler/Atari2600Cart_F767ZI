/**
 * @file arm_cortex_M4_fault_handler.c
 *
 *  Created on: Apr 22, 2018
 *      @author Mark Giebler
 *
 * This is open source code. Covered by Apache 2.0 license.
 *
 * Licensed under the Apache License, Version 2.0 (the License); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Mark Giebler's enhanced HardFault handler based on:
 * 			https://www.freertos.org/Debugging-Hard-Faults-On-Cortex-M-Microcontrollers.html
 * 	For capturing more fault details, see this code:
 * 			http://www.chibios.com/forum/viewtopic.php?t=743
 * 	Further enhanced by using the mbed OS version.
 *
 * 	This file is use in conjunction with the file exception.s
 *
 *  The linker script needs to be modified to define RAM section ".noinit.armFaultData"
 *
 */

#include "arm_cortex_M4_fault_handler.h"
#include "stm32f7xx_it.h"
#include "stm32f7xx.h"
//#include "cmsis_os.h"
#include "bsp.h"

/** uncomment 	NANO_LOGGER		if using my nano logger: https://gitlab.com/mark-giebler/nanoLogger */
//#define 		NANO_LOGGER
#ifdef	NANO_LOGGER
#include "log.h"
#endif
/* These prototypes are specified as a naked function such that gcc
 * does not generate any function entry prologue code - that is to say;
 * this is just an assembly function. */
void HardFault_Handler( void ) __attribute__( ( naked ) );
void arm_fault_handler( void ) __attribute__( ( naked ) );
/** Fault_Handler() function located in exception.s */
void Fault_Handler( void )  __attribute__( ( naked ) );

//void getRegistersFromStack( uint32_t *pulFaultStackAddress ); /* old way to get MCU context */

#ifndef MBED_FAULT_HANDLER_DISABLED
/// Global for populating the hard fault context in exception handler.
/// They are placed in the .noinit.armFaultData section such that their contents will survive a soft reset.
/// the linker script file is used to create the .armFaultData alignment attribute in the .noinit section
volatile arm_fault_context_t arm_fault_context  __attribute__ ((section (".noinit.armFaultData")));

//volatile uint32_t osRtxInfo  __attribute__ ((section (".noinit.armFaultData")));	// for mbed OS only

#endif

/******************************************************************************/
/*           Cortex-M4 Processor Interruption and Exception Handlers          */
/******************************************************************************/

/*
 * Remove the following functions from the CubeMX generated ISR handlers in
 * the file stm32xxxx_it.c, e.g stm32f4xx_it.c, stm32l4xx_it.c, stm32g4xx_it.c:
 * 		NMI_Handler(void)
 * 		HardFault_Handler(void)
 * 		MemManage_Handler(void)
 * 		BusFault_Handler(void)
 * 		UsageFault_Handler(void)
 *
 *
 */

uint32_t count__NMIevents = 0;	//!< Count of the number of NMI events.
/**
* @brief This function handles Non maskable interrupt.
*/
void NMI_Handler(void)
{
/* USER CODE BEGIN NonMaskableInt_IRQn 0 */
	/* Check RCC CSSF interrupt flag  */
	if(__HAL_RCC_GET_IT(RCC_IT_CSS))
	{
		// we can get here if the HSE oscillator generates a clock security interrupt
		// we must reset the pending interrupt or we will be stuck in an NMI loop.
		count__NMIevents++;
		if(count__NMIevents < 3)
		{
			//RCC->CICR = RCC_CICR_CSSC;
			__HAL_RCC_CLEAR_IT(RCC_IT_CSS);
			return;
		}
		// if fault happens multiple times, or if NMI from other cause,
		// then force a hardfault.
	}
#if(1)
	__asm volatile
		(
			// vector to our handler in exception.s
			" .equ     FAULT_TYPE_NMI_FAULT,			0xDEAD0060 	\n"
			" LDR      R3,=FAULT_TYPE_NMI_FAULT   				\n"
			" B        Fault_Handler               				\n"
		);
#endif
/* USER CODE END NonMaskableInt_IRQn 0 */
/* USER CODE BEGIN NonMaskableInt_IRQn 1 */

/* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
* @brief This function handles Hard fault interrupt.
* The prototype is specified as a naked function.
*/
void HardFault_Handler(void)
{
	/* USER CODE BEGIN HardFault_IRQn 0 */
		// _mg_ implement my HardFault_Handler  ISR.
	// For STM32x4xx   See Section 4.2.1 in PM0214  DM00046982  for fault types, status registers, config registers.
	// _mg_ light up all the LEDs idicate something bad happened.
	LL_LedGreen_On();
	LL_LedBlue_On();
	LL_LedRed_On();

	#if (1)
		/* Cortex M4 */
		__asm volatile
			(
				// vector to our handler in exception.s
				" .equ     FAULT_TYPE_HARD_FAULT,		0xDEAD0010 	\n"
				" LDR      R3,=FAULT_TYPE_HARD_FAULT   				\n"
				" B        Fault_Handler               				\n"
			);
	#else	/* old way to handle faults */
		__asm volatile
			(
				" tst lr, #4                                                \n"
				" ite eq                                                    \n"
				" mrseq r0, msp                                             \n"
				" mrsne r0, psp                                             \n"
				" ldr r1, [r0, #24]                                         \n"
				" ldr r2, =getRegistersFromStack                            \n"
				" bx r2                                                     \n"
			);
		return;


	/* USER CODE END HardFault_IRQn 0 */
	while (1)
	{
		/* USER CODE BEGIN W1_HardFault_IRQn 0 */
		/* USER CODE END W1_HardFault_IRQn 0 */
	}
	/* USER CODE BEGIN HardFault_IRQn 1 */

	#endif
	/* USER CODE END HardFault_IRQn 1 */
}

/**
* @brief This function handles Memory management fault.
*/
void MemManage_Handler(void)
{
	/* USER CODE BEGIN MemoryManagement_IRQn 0 */
	__asm volatile
	(
		// vector to our handler in exception.s
		" .equ     FAULT_TYPE_MEMMANAGE_FAULT,    0xDEAD0020	\n"
		" LDR      R3,=FAULT_TYPE_MEMMANAGE_FAULT				\n"
		" B        Fault_Handler								\n"
	);
	/* USER CODE END MemoryManagement_IRQn 0 */
	while (1)
	{
		/* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
		/* USER CODE END W1_MemoryManagement_IRQn 0 */
	}
	/* USER CODE BEGIN MemoryManagement_IRQn 1 */

	/* USER CODE END MemoryManagement_IRQn 1 */
}

/**
* @brief This function handles Prefetch fault, memory access fault.
*/
void BusFault_Handler(void)
{
	/* USER CODE BEGIN BusFault_IRQn 0 */
	__asm volatile
	(
		// vector to our handler in exception.s
		" .equ     FAULT_TYPE_BUS_FAULT,    0xDEAD0030			\n"
		" LDR      R3,=FAULT_TYPE_BUS_FAULT						\n"
		" B        Fault_Handler								\n"
	);

	/* USER CODE END BusFault_IRQn 0 */
	while (1)
	{
		/* USER CODE BEGIN W1_BusFault_IRQn 0 */
		/* USER CODE END W1_BusFault_IRQn 0 */
	}
	/* USER CODE BEGIN BusFault_IRQn 1 */

	/* USER CODE END BusFault_IRQn 1 */
}

/**
* @brief This function handles Undefined instruction or illegal state.
*/
void UsageFault_Handler(void)
{
	/* USER CODE BEGIN UsageFault_IRQn 0 */
	__asm volatile
	(
		// vector to our handler in exception.s
		" .equ     FAULT_TYPE_USAGE_FAULT,    0xDEAD0040		\n"
		" LDR      R3,=FAULT_TYPE_USAGE_FAULT					\n"
		" B        Fault_Handler								\n"
	);

	/* USER CODE END UsageFault_IRQn 0 */
	while (1)
	{
		/* USER CODE BEGIN W1_UsageFault_IRQn 0 */
		/* USER CODE END W1_UsageFault_IRQn 0 */
	}
	/* USER CODE BEGIN UsageFault_IRQn 1 */

	/* USER CODE END UsageFault_IRQn 1 */
}



/// @warning IMPORTANT!
/// The following assembly language declaration needs to be copied into the startup_stm32xxx.s file
/// at an appropriate isr vector table location.
/// An appropriate location is an unused vector AFTER the sysTick vector.
/// -
///	.word	Assert_Handler			/* _mg_ added to the IRQ position @ref SW_INT_VEC_ */

/** ********************************************************************
 * @brief Used to catch assert failures and setup for assert software IRQ
 * called by configASSERT(x) macro in FreeRTOSConfig.h when x == 0
 *
 * @param  func: The file name (or function name) as string.
 * @param  line: The line in file as a number.
 * @param  callerLR: return address to the caller of the function that assert failed.
 * @param  data1: data to log with the exception event.
 *
 * @note To resolve other macros in configASSERT() you must include cmsis_os.h
 * ********************************************************************/
void _Assert_Catcher( const char * func, int line , uint32_t callerLR, void * data1)
{
	arm_fault_context.caller_LR = callerLR;
#ifdef	NANO_LOGGER
	// log assert location to the log file
	logEventData16buDecConstString(L_ASSERT_FAIL, (uint16_t)line, (const char*) func);
#endif

#if (config_DEBUG_BREAK_ON_ASSERT_FAIL == 1)
	// if running debugger, force a break.
	if (line>0 && (CoreDebug->DHCSR & CoreDebug_DHCSR_C_DEBUGEN_Msk))
	{
		__asm("BKPT #0\n") ; // Break into the debugger
							// when debugging allow return in case more info can be ascertained about the fault.
		return;
	}
#endif
	__asm("NOP");

	/* When the USERSETMPEND bit in the SCR is set to 1, unprivileged software can access the STIR */
	HAL_NVIC_SetPriority( ASSERT_IRQn, 5, 0 );
	NVIC_EnableIRQ( ASSERT_IRQn );
	// configASSERT macro in FreeRTOSConfig.h will trigger the software interrupt by writing SW_INT_VEC_
	// to the NVIC->STIR register after calling this function using the macro @ref EXEC_SW_INT_VEC().
	// Triggering the fault in the macro allows capturing the fault context of the executing module the macro is in (instead of this function)
	return;
}

/**
 * Software IRQn SW_INT_VEC_ vectored to here.
 * The configASSERT() method will do a software interrupt to IRQn SW_INT_VEC_ in _Assert_Catcher() above.
 *
 * See note in header file about redefining configASSERT macros.
 *
 */
void Assert_Handler( void )
{
#if (config_DEBUG_BREAK_ON_ASSERT_FAIL == 1)
	// if running debugger, force a break.
	if (CoreDebug->DHCSR & CoreDebug_DHCSR_C_DEBUGEN_Msk)
	{

		__asm("BKPT #0\n") ; // Break into the debugger

							// when debugging allow return in case more info can be ascertained about the fault.
		return;				// not a "naked" function, so use normal return.
//		__asm("BX LR\n");	// "return" - since this is a "naked" function, we need to use assembly code to return.
	}
#endif
	// not debug, so hardfault to capture registers.
	__asm volatile	(
			" .equ     FAULT_TYPE_ASSERT_FAULT,		0xDEAD0050 \n"
			" LDR      R3,=FAULT_TYPE_ASSERT_FAULT \n"
			" B        Fault_Handler \n"
			);
}

extern uint32_t _estack;	//!< defined in linker script STM32L476GTx_FLASH.ld  This is top of RAM.

/** This is a handler function called from Fault_Handler() in exception.s  provide further information via UI.
 * The prototype is specified as a naked function such that gcc
 * does not generate any function entry prologue code - that is to say;
 * this is just an assembly function.
 *
 * Good info on ARM faults: https://interrupt.memfault.com/blog/cortex-m-fault-debug
 */
void  __attribute__( ( naked ) ) arm_fault_handler( void )
{
	//
#if (config_DEBUG_BREAK_AT_HARDFAULT == 1)
	// if running debugger, force a break.
	if (CoreDebug->DHCSR & CoreDebug_DHCSR_C_DEBUGEN_Msk)
	{
		__asm("BKPT #0\n") ; // Break into the debugger
							// when debugging allow return in case more info can be ascertained about the fault.
		__asm("BX LR\n");	// "return" - since this is a "naked" function, we need to use assembly code to return.
	}
#endif

	// try and capture caller's stack
	register uint32_t stack;
	register uint32_t indx=0;
	stack = arm_fault_context.SP;
	while(indx < ARM_FAULT_STACK_SAVE_SIZE && stack >= (SRAM1_BASE) && (stack+4UL) <= (uint32_t)((&_estack)))
	{
		if( ( MEMV32u(stack) & 0xFF000000) == 0x08000000 )
			// save what looks like an address for code.
			arm_fault_context.stack[indx++] = MEMV32u(stack);
		stack += 4;
	}
	// Since this is a naked function, can't use local variables without dropping into assembly language
/*	if( arm_fault_context.SP >= (SRAM1_BASE+28UL) && arm_fault_context.SP <= (uint32_t)((&_estack)-12UL))
	{
		arm_fault_context.stack[0] = *((uint32_t *) (arm_fault_context.SP + 0));
		arm_fault_context.stack[1] = *((uint32_t *) (arm_fault_context.SP + 4));
		arm_fault_context.stack[2] = *((uint32_t *) (arm_fault_context.SP + 8));
		arm_fault_context.stack[3] = *((uint32_t *) (arm_fault_context.SP + 12));
		arm_fault_context.stack[4] = *((uint32_t *) (arm_fault_context.SP - 4));
		arm_fault_context.stack[5] = *((uint32_t *) (arm_fault_context.SP - 8));
		arm_fault_context.stack[6] = *((uint32_t *) (arm_fault_context.SP - 12));
		arm_fault_context.stack[7] = *((uint32_t *) (arm_fault_context.SP - 16));
		arm_fault_context.stack[8] = *((uint32_t *) (arm_fault_context.SP - 20));
		arm_fault_context.stack[9] = *((uint32_t *) (arm_fault_context.SP - 24));
		arm_fault_context.stack[10] = *((uint32_t *) (arm_fault_context.SP - 28));
//		arm_fault_context.stack[11] = *((uint32_t *) (arm_fault_context.SP - 28));

	} */
	/* under normal operation: infinite loop to ensure this routine never returns */
	for(;;)
	{
		__asm("NOP");	// _mg_ wait for watchdog timer to reset system
	}
}


// -------------------------------------------
// compile in to use the hardfault tester
// -------------------------------------------
// Note we can force an illegal instruction fault using:
// 		__asm volatile (".word 0xe7f0def0\n"); /* arm+thumb illegal instruction */
// See here:  https://stackoverflow.com/questions/16081618/programmatically-cause-undefined-instruction-exception
//


#define FAULTY_ADDRESS	0xa5a5aaa5a5	//!< Defines an address that will cause a hardfault.
/**
	 * @brief
	* Call this to cause a hardfault as a way to test populating the hardfault context structure.
	*
	* @returns 42 if function is not enabled. Otherwise will not return.
	*/
uint32_t hardFault_tester(void)
{
#if (config_ENABLE_HARDFAULT_TEST == 1)
	/* this will not return to caller */
	/* test HardFault handler by forcing a fault */

	__asm volatile	(
			" .equ     FAULTY_ADDRESS, 0xa5a5aaa5a5 \n"
			" ldr      R0,=0xdead0000        \n"
			" ldr      R1,=0xbeef0001        \n"
			" ldr      R2,=0xfeed0002        \n"
			" ldr      R3,=0xbeef0003        \n"
			" ldr      R4,=0xfeed0004        \n"
			" ldr      R5,=0xface0005        \n"
			" ldr      R6,=0xbeef0006        \n"
			" ldr      R7,=0xface0007        \n"
			" ldr      R8,=0xdead0008        \n"
			" ldr      R9,=0xcafe0009        \n"

			" ldr      R12,=FAULTY_ADDRESS   \n"
			" str      r0,[R12]              \n"	/* force fault */

			/* busy work to keep PC in this function if hardfault happens
			 * later due to cache  (imprecise mode ) */
			" ldr      r10,=8                \n"
			" ldr      r11,=11               \n"
			" ADDS     R10,#2                \n"
	);

#endif /* config_ENABLE_HARDFAULT_TEST */
	return 42;
}





/* **************************************************************************
 * arm fault handler data dumper helper routines.
 *
 * If a previous hard fault occurred, data will be left in arm_fault_context
 * and arm_fault_type will have a flag.  This data persists after a reset.
 *
 */
#ifndef MBED_FAULT_HANDLER_DISABLED


#if THREAD_INFO
//  _mg_ modify to support freeRTOS thread structures. Not needed; have this support in cmdRtos file
void print_threads_info(osRtxThread_t *);
void print_thread(osRtxThread_t *thread);
#endif

#if DEVICE_SERIAL
extern int stdio_uart_inited;
extern serial_t stdio_uart;
#endif

#ifndef s_EOL
#define s_EOL "\n"	//!< End of line character(s), normally just one char. But some die-hard Windows users might like two.
#endif

/// @brief This is a function to print the arm fault context information out.
///
/// This can be run in fault context if using special functions
/// (defined here) to print the information without using C-lib support.
///
/// @param serial_putchar  a pointer to a function to output one character to a serial port.
void dump_arm_fault(void (*serial_putchar)(char))
{
	char* str ="";

	fault_print_init();

	fault_print_str(serial_putchar, s_EOL "++ ARM Fault Handler ++" s_EOL, NULL);

	fault_print_str(serial_putchar, s_EOL "FaultType: ",NULL);
	switch( arm_fault_type ) {
	case HARD_FAULT_EXCEPTION:
		str = "HardFault";
		break;
	case MEMMANAGE_FAULT_EXCEPTION:
		str = "MemManageFault";
		break;
	case BUS_FAULT_EXCEPTION:
		str = "BusFault";
		break;
	case USAGE_FAULT_EXCEPTION:
		str = "UsageFault";
		break;
	case FAULT_TYPE_ASSERT_FAULT:
		str = "AssertFault";
		break;
	default:
		fault_print_str(serial_putchar, "No Fault",NULL);
		fault_print_str(serial_putchar,": %", (uint32_t*)&arm_fault_type);
		fault_print_str(serial_putchar,s_EOL s_EOL ,NULL);
		fault_print_str(serial_putchar, s_EOL "-- ARM Fault Handler --" s_EOL, NULL);
		return;
	}
	fault_print_str(serial_putchar,str,NULL);

	fault_print_str(serial_putchar,s_EOL s_EOL "Context:",NULL);
	print_context_info(serial_putchar);
#if THREAD_INFO
	fault_print_str(s_EOL s_EOL "Thread Info:" s_EOL "Current:",NULL);
	print_thread(((osRtxInfo_t *)osRtxInfoIn)->thread.run.curr);

	fault_print_str(s_EOL "Next:",NULL);
	print_thread(((osRtxInfo_t *)osRtxInfoIn)->thread.run.next);

	fault_print_str(s_EOL "Wait Threads:",NULL);
	osRtxThread_t *threads = ((osRtxInfo_t *)osRtxInfoIn)->thread.wait_list;
	print_threads_info(threads);

	fault_print_str(s_EOL "Delay Threads:",NULL);
	threads = ((osRtxInfo_t *)osRtxInfoIn)->thread.delay_list;
	print_threads_info(threads);

	fault_print_str(s_EOL "Idle Thread:",NULL);
	threads = ((osRtxInfo_t *)osRtxInfoIn)->thread.idle;
	print_threads_info(threads);
#endif
	fault_print_str(serial_putchar,s_EOL s_EOL ,NULL);


	fault_print_str(serial_putchar, s_EOL "-- ARM Fault Handler --" s_EOL, NULL);

	// _mg_ customize fault data output
	// print out the caller's return address
	fault_print_str(serial_putchar,s_EOL "Caller LR: %" ,(uint32_t *)&arm_fault_context.caller_LR);
	for(int i=0; i< ARM_FAULT_STACK_SAVE_SIZE; i++)
	{
		fault_print_str(serial_putchar,s_EOL "sk : %",(uint32_t *)(&arm_fault_context.stack)+i);
	}
/*	fault_print_str(s_EOL "sp+0 : %"
					s_EOL "sp+4 : %"
					s_EOL "sp+8 : %"
					s_EOL "sp+12: %"
					s_EOL "sp-4 : %"
					s_EOL "sp-8 : %"
					s_EOL "sp-12: %"
					s_EOL "sp-16: %"
					s_EOL "sp-20: %"
					s_EOL "sp-24: %"
					s_EOL "sp-28: %"
					s_EOL,(uint32_t *)&arm_fault_context.stack);*/

	fault_print_str(serial_putchar, s_EOL, NULL);
}

/**
 * @brief Output the hardfault context to a serial port.
 *
 * @param serial_putchar  a pointer to function to output one character to a serial port.
 *
 */
void print_context_info(void (*serial_putchar)(char))
{
	//Context Regs - List items and order must be manually coordinated with the layout of the arm_fault_context_t structure
	fault_print_str(serial_putchar,
					s_EOL "R0   : %"
					s_EOL "R1   : %"
					s_EOL "R2   : %"
					s_EOL "R3   : %"
					s_EOL "R4   : %"
					s_EOL "R5   : %"
					s_EOL "R6   : %"
					s_EOL "R7   : %"
					s_EOL "R8   : %"
					s_EOL "R9   : %"
					s_EOL "R10  : %"
					s_EOL "R11  : %"
					s_EOL "R12  : %"
					s_EOL "SP   : %"
					s_EOL "LR   : %"
					s_EOL "PC   : %"
					s_EOL "xPSR : %"
					s_EOL "PSP  : %"
					s_EOL "MSP  : %", (uint32_t *)&arm_fault_context.R0);

	//Capture CPUID to get core/cpu info
	fault_print_str(serial_putchar, s_EOL "CPUID: %",(uint32_t *)&SCB->CPUID);

#if !defined(TARGET_M0) && !defined(TARGET_M0P)
	//dump fault information registers to infer the cause of exception
	uint32_t FSR[7] = {0};

	FSR[0] = arm_fault_context._HFSR;
	//Split/Capture CFSR into MMFSR, BFSR, UFSR
	FSR[1] = 0xFF & arm_fault_context._CFSR;//MMFSR
	FSR[2] = (0xFF00 & arm_fault_context._CFSR) >> 8;//BFSR
	FSR[3] = (0xFFFF0000 & arm_fault_context._CFSR) >> 16;//UFSR
	FSR[4] = arm_fault_context._DFSR;
	FSR[5] = arm_fault_context._AFSR;
	FSR[6] = arm_fault_context._SHCSR;
	fault_print_str(serial_putchar,
					s_EOL "HFSR : %"
					s_EOL "MMFSR: %"
					s_EOL "BFSR : %"
					s_EOL "UFSR : %"
					s_EOL "DFSR : %"
					s_EOL "AFSR : %"
					s_EOL "SHCSR: %",FSR);

	//Print MMFAR only if its valid as indicated by MMFSR
	if(FSR[1] & 0x80) {
		fault_print_str(serial_putchar, s_EOL "MMFAR: %",(uint32_t *)&arm_fault_context._MMFAR);
	}
	//Print BFAR only if its valid as indicated by BFSR
	if(FSR[2] & 0x80) {
		fault_print_str(serial_putchar, s_EOL "BFAR : %",(uint32_t *)&arm_fault_context._BFAR);
	}
#endif
	//Print Mode
	if(arm_fault_context.EXC_RETURN & 0x8) {
		fault_print_str(serial_putchar, s_EOL "Mode : Thread", NULL);
		//Print Priv level in Thread mode - We capture CONTROL reg which reflects the privilege.
		//Note that the CONTROL register captured still reflects the privilege status of the
		//thread mode even though we are in Handler mode by the time we capture it.
		if(arm_fault_context.CONTROL & 0x1) {
			fault_print_str(serial_putchar, s_EOL "Priv : User", NULL);
		} else {
			fault_print_str(serial_putchar, s_EOL "Priv : Privileged", NULL);
		}
	} else {
		fault_print_str(serial_putchar, s_EOL "Mode : Handler", NULL);
		fault_print_str(serial_putchar, s_EOL "Priv : Privileged", NULL);
	}
	//Print Return Stack
	if(arm_fault_context.EXC_RETURN & 0x4) {
		fault_print_str(serial_putchar, s_EOL "Stack: PSP", NULL);
	} else {
		fault_print_str(serial_putchar,s_EOL "Stack: MSP", NULL);
	}
}



#if THREAD_INFO
/* Prints thread info from a list */
void print_threads_info(void (*serial_putchar)(char), osRtxThread_t *threads)
{
	while(threads != NULL) {
		print_thread(serial_putchar, threads );
		threads = threads->thread_next;
	}
}

/* Prints info of a thread(using osRtxThread_t struct)*/
void print_thread(void (*serial_putchar)(char), osRtxThread_t *thread)
{
	uint32_t data[5];

	data[0]=thread->state;
	data[1]=thread->thread_addr;
	data[2]=thread->stack_size;
	data[3]=(uint32_t)thread->stack_mem;
	data[4]=thread->sp;
	fault_print_str(serial_putchar, s_EOL "State: % EntryFn: % Stack Size: % Mem: % SP: %", data);
}
#endif

/** Initializes std uart for spitting the info out (if needed) */
void fault_print_init()
{
#if DEVICE_SERIAL
	if (!stdio_uart_inited) {
		serial_init(&stdio_uart, STDIO_UART_TX, STDIO_UART_RX);
	}
#endif
}

/** Limited print functionality which prints the string out to
stdout/uart without using stdlib by directly calling serial-api
and also uses less resources.

@param serial_putchar  a pointer to a function to output one character to a serial port.
@param fmtstr contains the format string for printing and for every %
found in that it fetches a uint32 value from values buffer
and prints it in hex format.
@param values a pointer to an array of uint32_t types.
*/
void fault_print_str( void (*serial_putchar)(char), char *fmtstr, uint32_t *values )
{
#if DEVICE_SERIAL
	int i = 0;
	int idx = 0;
	int vidx = 0;
	char hex_str[9]={0};

	while(fmtstr[i] != '\0') {
		if(fmtstr[i]=='%') {
			hex_to_str(values[vidx++],hex_str);
			for(idx=7; idx>=0; idx--) {
				serial_putc(&stdio_uart, hex_str[idx]);
			}
		} else {
			serial_putc(&stdio_uart, fmtstr[i]);
		}
		i++;
	}
#else
	int i = 0;
	int idx = 0;
	int vidx = 0;
	char hex_str[9];

	if(values != NULL){
		while(fmtstr[i] != '\0') {
			if(fmtstr[i]=='%') {
				hex_to_str(values[vidx++],hex_str);
				for(idx=0; idx<=7; idx++) {
					(*serial_putchar)(hex_str[idx]);
				}
			} else {
				(*serial_putchar)(fmtstr[i]);
			}
			i++;
		}
	}else
	{
		while(fmtstr[i] != '\0') {
			(*serial_putchar)(fmtstr[i++]);
		}
	}

#endif
}
/*!
 * Converts a uint32 to hex char string
 * hex_str must be a string at least 9 bytes long
 */
void hex_to_str(uint32_t value, char *hex_str)
{
	int i = 0;
	uint8_t nibble;
	//Return without converting if hex_str is not provided
	if(hex_str == NULL)
		return;

	for(i=7; i>=0; i--) {
		nibble = (value & (0x000f << (i * 4))) >> (i * 4);
		hex_str[7-i] = (nibble < 0xa ) ? nibble+0x30 : nibble + 0x37 ;
	}
	hex_str[8] = 0;	// terminate string.
}

#endif /* MBED_FAULT_HANDLER_DISABLED */


