/**
 * @file main_plus.c
 *
 *  Created on: Apr 13, 2022
 *      Author: mgiebler
 *
 * From CartPlus project.
 *
 * The main code for suppoting the cartPlus features.
 *
 */
/**
  ******************************************************************************
  * File            : cp_main.c
  * Brief           : PlusCart(+) Firmware
  * Author          : Wolfgang Stubig <w.stubig@firmaplus.de>
  * Website         : https://gitlab.com/firmaplus/atari-2600-pluscart
  ******************************************************************************
  * (c) 2019 Wolfgang Stubig (Al_Nafuur)
  * based on: UnoCart2600 by Robin Edwards (ElectroTrains)
  *           https://github.com/robinhedwards/UnoCart-2600
  *           and
  *           UnoCart2600 fork by Christian Speckner (DirtyHairy)
  *           https://github.com/DirtyHairy/UnoCart-2600
  ******************************************************************************
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

#ifdef CARTPLUS_ENABLE
// If  CARTPLUS_ENABLE is not defined, then this file from here to the end is ignored.

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <global.h>
#include <string.h>
#include <inttypes.h>	// for PRIu32 definition used in sprintf.

#include "font.h"
#ifdef USE_WIFI
#include "esp8266.h"
#endif
#ifdef USE_SD_CARD
#include "fs_utils.h"
#include "fatfs.h"
#endif

//_mg_ #include "stm32_udid.h"
#include "flash.h"
#include "cartridge_io.h"
#include "cartridge_firmware_plus.h"
#include "cartridge_supercharger.h"
#include "cartridge_dpc.h"
#include "cartridge_3f.h"
#include "cartridge_3e.h"
#include "cartridge_3ep.h"
#include "cartridge_bf.h"
#include "cartridge_df.h"
#include "cartridge_ace.h"
#include "cartridge_pp.h"

#include "ram_storage.h"

#include "cartridge_detection.h"
#include "cartridge_others.h"

#include "uart.h"
void MX_USART3_UART_Init(void);
void MX_USART3_DeInit(void);

//void generate_udid_string(void);

void truncate_curPath(/*uint8_t count*/);

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/

typedef struct {
	enum cart_base_type base_type;
	bool withSuperChip;
	bool withPlusFunctions;
	bool uses_ccmram;
	bool uses_systick;
	uint32_t flash_part_address;
} CART_TYPE;

const typedef struct {
	const char *ext;		// filename extension
	const CART_TYPE cart_type;
} cp_EXT_TO_CART_TYPE_MAP;

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define MG_VERSION	" v3.09"	// _mg_ my version.

#ifndef USE_MONOLITHIC_FS_UTILS
#define read_directory fs_read_directory
#define get_filename_ext fs_get_filename_ext
#endif

#define VERSION	"MG"
#define FS_total_size	0
#define FS_used_size	1


const cp_EXT_TO_CART_TYPE_MAP cp_ext_to_cart_type_map[]__attribute__((section(".flash01"))) = {
	{"ROM",  { base_type_None,    false, false, false, false }},
	{"BIN",  { base_type_None,    false, false, false, false }},
	{"A26",  { base_type_None,    false, false, false, false }},
	{"2K",   { base_type_2K,      false, false, false, false }},
	{"4K",   { base_type_4K,      false, false, false, false }},
	{"4KS",  { base_type_4K,      true,  false, false, false }},
	{"F8",   { base_type_F8,      false, false, false, false }},
	{"F6",   { base_type_F6,      false, false, false, false }},
	{"F4",   { base_type_F4,      false, false, false, false }},
	{"F8S",  { base_type_F8,      true,  false, false, false }},
	{"F6S",  { base_type_F6,      true,  false, false, false }},
	{"F4S",  { base_type_F4,      true,  false, false, false }},
	{"FE",   { base_type_FE,      false, false, false, false }},
	{"3F",   { base_type_3F,      false, false, false, false }},
	{"3E",   { base_type_3E,      false, false, false, false }},
	{"E0",   { base_type_E0,      false, false, false, false }},
	{"084",  { base_type_0840,    false, false, false, false }},
	{"CV",   { base_type_CV,      false, false, false, false }},
	{"EF",   { base_type_EF,      false, false, false, false }},
	{"EFS",  { base_type_EF,      true,  false, false, false }},
	{"F0",   { base_type_F0,      false, false, false, false }},
	{"FA",   { base_type_FA,      false, false, false, false }},
	{"E7",   { base_type_E7,      false, false, false, false }},
	{"DPC",  { base_type_DPC,     false, false, true,  true  }},
	{"AR",   { base_type_AR,      false, false, false, false }},
	{"BF",   { base_type_BF,      false, false, true,  false }},
	{"BFS",  { base_type_BFSC,    false, false, true,  false }},
	{"ACE",  { base_type_ACE,     false, false, false, false }},
	{"WD",   { base_type_PP,      false, false, false, false }},
	{"DF",   { base_type_DF,      false, false, true,  false }},
	{"DFS",  { base_type_DFSC,    false, false, true,  false }},
	{"3EP",  { base_type_3EPlus,  false, false, false, false }},
	{"DPCP", { base_type_DPCplus, false, false, false, true  }},
	{"SB",   { base_type_SB,      false, false, true,  false }},
	{"UA",   { base_type_UA,      false, false, false, false }},

	{0,{0,0,0}}
};


// _mg_ These message string MUST be manually coordinated with the enum e_status_message
const char *status_message[]__attribute__((section(".flash01#"))) = {

#if MENU_TYPE == UNOCART
	"UnoCart 2600" MG_VERSION,
#else
	"Mark's PlusCart(+)" MG_VERSION,
#endif
//	"Select WiFi Network",
//	"No WiFi",
//	"WiFi connected",
//	"Request timeout",
//	"Enter WiFi Password",
//	"Enter email or username",
//	"Your Chat Message",
	"Offline ROMs erased",
	"ROM file too big!",
	"ACE file unsupported",
	"Unknown/invalid ROM",
	"Done",
	"Failed",
	"Firmware download failed",
	"Offline ROMs detected",
	"No offline ROMs detected",
	"DPC+ is not supported",
	"Emulation exited",
	"ROM Download Failed",

	"Setup",
	"Select TV Mode",
	"Select Font",
	"Select Line Spacing",
	"Setup/System Info",
	MENU_TEXT_SEARCH_FOR_ROM,
	"Enter search details",
	"Search results",

	MENU_TEXT_APPEARANCE,
};

const uint8_t numMenuItemsPerPage[] = {
		// ref: SPACING enum
		14,									// dense
		12,									// medium
		10									// sparse
};

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

int no_init_section_init_done = 0;	/* _mg_ used to determine if objects in the .noinit section need to be initialized to safe values */
int num_menu_entries = 0;			// todo: _mg_ replace with fs_utils call to get this number? CartPlus not using fs_utils.c much.
unsigned int cart_size_bytes = 0;	// _mg_ gets set in cp_identify_cartridge()
uint8_t plus_store_status[1];

/* .noinit objects ----------------------------------------------------------*/
char http_request_header[512] __attribute__ ((section (".noinit")));

//#define 	STM32UDID_ENABLE		// _mg_ Don't need this feature.
char stm32_udid[25] __attribute__ ((section (".noinit")));

// _mg_  The declaration size of the buffer[] array must be manually coordinated with the size allocated to the .buffer section in the linker file.
uint8_t buffer[BUFFER_SIZE_KB * 1024] __attribute__((section(".buffer")));

USER_SETTINGS user_settings  __attribute__ ((section (".noinit")));

char curPath[256] __attribute__ ((section (".noinit")));
char input_field[STATUS_MESSAGE_LENGTH] __attribute__ ((section (".noinit")));

int inputActive __attribute__ ((section (".noinit")));


// _mg_ Was in section .ccmram but was getting placed in normal RAM. It is 52K in size.
MENU_ENTRY menu_entries[NUM_MENU_ITEMS] __attribute__((section(".menuram")));

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/

/* USER CODE BEGIN PFP */
enum e_status_message buildMenuFromPath( MENU_ENTRY * )__attribute__((section(".flash0"))) ;
void append_entry_to_path(MENU_ENTRY *);



/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/**
 * _mg_  Initialize objects in the .noinit section such that they are safe to use.
 * I.e. no wild pointers off in the weeds that might cause a hardfault.
 *
 * This will take a while to complete, so only call after 6507 is running its
 * bootstrap program in the Atari on board RAM.
 */
void init_the_no_init(void)
{
	http_request_header[0] = 0;
	memset((uint8_t*)menu_entries, 0, sizeof(menu_entries));
	curPath[0] = 0;
	memset((int8_t*)input_field, 0, sizeof(input_field));
	inputActive = 0;
	//
	user_settings.line_spacing = SPACING_DEFAULT;
	user_settings.font_style = FONT_DEFAULT;
	user_settings.tv_mode = TV_MODE_DEFAULT;	// NTSC.
	user_settings.first_free_flash_sector = FLASH_SECTOR_7;	// FIXME: _mg_ Flash memory sector size different on F767. Therefore set this very high up in FLASH.
	no_init_section_init_done = 1;
}

/**
 * @brief _mg_ Lookup the filename extension that is associated with
 * the passed in cartridge type.
 *
 * @param type - the cartridge type to lookup.
 * @return pointer to extension type if found, or "" if not found.
 */
const char* lookup_ext_from_type(enum cart_base_type type)
{
	cp_EXT_TO_CART_TYPE_MAP* mptr = cp_ext_to_cart_type_map;

	while(mptr->ext){
		if(mptr->cart_type.base_type == type)
			return mptr->ext;
		++mptr;
	}
	return "";
}

void generate_udid_string(){
	int i;
	uint8_t c;
	memset(stm32_udid, '0', 24);
	stm32_udid[24] = '\0';
	for (int j = 2; j > -1; j--){
		uint32_t content_len = STM32_UDID[j];
		i = (j * 8) + 7;
		while (content_len != 0 && i > -1) {
			c = content_len % 16;
			stm32_udid[i--] = (char)((c > 9)? (c-10) + 'a' : c + '0');
			content_len = content_len/16;
		}
	}
}

/*************************************************************************
 * Menu Handling
 *************************************************************************/

/**
 * Populate a menu entry element and then increment to the next menu entry.
 *
 * @param dst - pointer to MENU_ENTRY pointer, on exit: incremented to next available entry.
 * @param name - the name displayed to the user for the menu entry.
 * @param type - MENU_ENTRY_Type; Action_Menu, Sub_Menu, Leave_Menu, Setup_Menu, SD_Cart_File, etc.
 * @param font - font to use
 */
void make_menu_entry_font( MENU_ENTRY **dst, const char *name, int type, uint8_t font) {
	(*dst)->type = type;
	strcpy((*dst)->entryname, name);
	(*dst)->filesize = 0U;
	(*dst)->font = font;
	// make sure init to NULL pointer.
	(*dst)->full_filename=0;
	(*dst)++;
	num_menu_entries++;
}


void make_menu_entry( MENU_ENTRY **dst, const char *name, int type){
	make_menu_entry_font(dst, name, type, user_settings.font_style);
}

#ifdef KEYBOARD_ENABLE
// _mg_ keyboard not used in our build.
const char *keyboardUppercase[]__attribute__((section(".flash0#"))) = {
	" 1  2  3  4  5  6  7  8  9  0",
	"  Q  W  E  R  T  Y  U  I  O  P",
	"   A  S  D  F  G  H  J  K  L",
	"    Z  X  C  V  B  N  M",
	"     " MENU_TEXT_SPACE " !  ?  ,  .",
	0
};

const char *keyboardLowercase[]__attribute__((section(".flash0#"))) = {
	" 1  2  3  4  5  6  7  8  9  0",
	"  q  w  e  r  t  y  u  i  o  p",
	"   a  s  d  f  g  h  j  k  l",
	"    z  x  c  v  b  n  m",
	"     " MENU_TEXT_SPACE " !  ?  ,  .",
	0
};

const char *keyboardSymbols[]__attribute__((section(".flash0#"))) = {
	" " MENU_TEXT_SPACE "   ( )  { }  [ ]  < >",
	"  !  ?  .  ,  :  ;  \"  '  `",
	"   @  ^  |  \\  ~  #  $  %  &",
	"    +  -  *  /  =  _",
	0
};

enum keyboardType {
	KEYBOARD_UPPERCASE,
	KEYBOARD_LOWERCASE,
	KEYBOARD_SYMBOLS,
	KEYBOARD_NONE,
};

static const char **keyboards[]__attribute__((section(".flash0#"))) = {
	keyboardUppercase,
	keyboardLowercase,
	keyboardSymbols,
	0
};

enum keyboardType lastKb = KEYBOARD_UPPERCASE;


void make_keyboardFromLine(MENU_ENTRY **dst, char *line) {

	make_menu_entry(dst, MENU_TEXT_GO_BACK, Leave_SubKeyboard_Menu);
	char item[33];
	while (*line) {
		char *entry = item;
		while (*line && *line == ' ')
			line++;
		while (*line && *line != ' ')
			*entry++ = *line++;
		*entry = 0;
		if (*item)
			make_menu_entry(dst, item, Keyboard_Char);
	}
}


void make_keyboard(MENU_ENTRY **dst, enum keyboardType selector){

	make_menu_entry(dst, MENU_TEXT_GO_BACK, Leave_Menu);

	for (const char **kbRow = keyboards[selector]; *kbRow; kbRow++)
		make_menu_entry(dst, *kbRow, Keyboard_Row);

	if (selector != KEYBOARD_LOWERCASE)
		make_menu_entry(dst, MENU_TEXT_LOWERCASE, Keyboard_Row);
	if (selector != KEYBOARD_UPPERCASE)
		make_menu_entry(dst, MENU_TEXT_UPPERCASE, Keyboard_Row);
	if (selector != KEYBOARD_SYMBOLS)
		make_menu_entry(dst, MENU_TEXT_SYMBOLS, Keyboard_Row);

	if (*input_field)
		make_menu_entry(dst, MENU_TEXT_DELETE_CHAR, Delete_Keyboard_Char);

	make_menu_entry(dst, "Enter", Menu_Action);
}

enum e_status_message generateKeyboard(
		MENU_ENTRY **dst,
		MENU_ENTRY *d,
		enum e_status_message menuStatusMessage,
		enum e_status_message new_status) {

	// Scan for any keyboard rows, and if found then generate menu for row
	for (const char ***kb = keyboards; *kb; kb++)
		for (const char **row = *kb; *row; row++)
			if (!strcmp(*row, d->entryname)) {
				make_keyboardFromLine(dst, d->entryname);
				truncate_curPath();
				return menuStatusMessage;
			}

	// look for change of keyboard
	if (!strcmp(d->entryname, MENU_TEXT_LOWERCASE))
		lastKb = KEYBOARD_LOWERCASE;
	else if (!strcmp(d->entryname, MENU_TEXT_UPPERCASE))
		lastKb = KEYBOARD_UPPERCASE;
	else if (!strcmp(d->entryname, MENU_TEXT_SYMBOLS))
		lastKb = KEYBOARD_SYMBOLS;
	else {

		// initial case - use previous keyboard
		menuStatusMessage = new_status;
		strcat(curPath, "/");				// trimmed off, below
	}

	make_keyboard(dst, lastKb);
	truncate_curPath();
	return menuStatusMessage;
}

// ---- END of Keyboard items ---------
#endif		// KEYBOARD_ENABLE


int compVersions ( const char * version1, const char * version2 ) {
    unsigned major1 = 0, minor1 = 0, bugfix1 = 0;
    unsigned major2 = 0, minor2 = 0, bugfix2 = 0;
    sscanf(version1, "%u.%u.%u", &major1, &minor1, &bugfix1);
    sscanf(version2, "%u.%u.%u", &major2, &minor2, &bugfix2);
    if (major1 < major2) return -1;
    if (major1 > major2) return 1;
    if (minor1 < minor2) return -1;
    if (minor1 > minor2) return 1;
    if (bugfix1 < bugfix2) return -1;
    if (bugfix1 > bugfix2) return 1;
    return 0;
}



MENU_ENTRY* generateSetupMenu(MENU_ENTRY *dst) {
	make_menu_entry(&dst, MENU_TEXT_GO_BACK, Leave_Menu);
#ifdef USE_WIFI
	make_menu_entry(&dst, MENU_TEXT_WIFI_SETUP, Setup_Menu);
#endif
	make_menu_entry(&dst, MENU_TEXT_DISPLAY, Setup_Menu);
	make_menu_entry(&dst, MENU_TEXT_SYSTEM_INFO, Setup_Menu /*Sub_Menu*/);

#ifdef	MENU_TEXT_DELETE_OFFLINE_ROMS
	if (flash_has_downloaded_roms())
		make_menu_entry(&dst, MENU_TEXT_DELETE_OFFLINE_ROMS, Menu_Action);
	else
		make_menu_entry(&dst, MENU_TEXT_DETECT_OFFLINE_ROMS, Menu_Action);
#endif	// MENU_TEXT_DELETE_OFFLINE_ROMS
#ifdef USE_SD_CARD
	make_menu_entry(&dst, MENU_TEXT_FORMAT_SD_CARD, Menu_Action);
#endif
	make_menu_entry(&dst, MENU_TEXT_FORMAT_EEPROM, Menu_Action);

	return dst;
}

MENU_ENTRY* generateSystemInfo(MENU_ENTRY *dst) {
#if (MENU_TYPE == 2 /*PLUSCART*/)
	make_menu_entry(&dst, "PlusCart Device", Leave_Menu);
#elif (MENU_TYPE == 1 /*UNOCART*/)
	make_menu_entry(&dst, "UnoCart Device", Leave_Menu);
#endif
#ifdef STM32UDID_ENABLE
	sprintf(input_field, "ID:    %s", stm32_udid);
	make_menu_entry(&dst, input_field, Leave_Menu);
#endif
	make_menu_entry(&dst, "STM32 Firmware:   " MG_VERSION, Leave_Menu);

#ifdef USE_WIFI
	sprintf(input_field, "WiFi Firmware      %s", esp8266_at_version);
	make_menu_entry(&dst, input_field, Leave_Menu);
#endif

	sprintf(input_field, "Flash Size:       %d KB", (int)(flash_size_bytes()/1024ul));
	make_menu_entry(&dst, input_field, Leave_Menu);

	sprintf(input_field, "Flash Used:        %d KB", //((int)user_settings.first_free_flash_sector - 4 ) * 128);
			(int)(flash_code_size()/1024ul));
	make_menu_entry(&dst, input_field, Leave_Menu);

	sprintf(input_field, "Buffer Size:       %d KB", (int)(max_buffer_size()/1024ul));
	make_menu_entry(&dst, input_field, Leave_Menu);
	sprintf(input_field, "CCMRAM Size:       %d KB", (int)(CCM_SIZE_KB));
	make_menu_entry(&dst, input_field, Leave_Menu);

#ifdef USE_SD_CARD
	uint32_t* sd_stat = fs_statistic();
   	sprintf(input_field, "SD-Card Size:  %d KB", (int)sd_stat[FS_total_size] );
   	make_menu_entry(&dst, input_field, Leave_Menu);
   	sprintf(input_field, "SD-Card Used:  %d KB", (int)sd_stat[FS_used_size] );
   	make_menu_entry(&dst, input_field, Leave_Menu);
#endif

	*input_field = 0;
	return dst;
}



int entry_compare(const void* p1, const void* p2){
	MENU_ENTRY* e1 = (MENU_ENTRY*)p1;
	MENU_ENTRY* e2 = (MENU_ENTRY*)p2;
	if (e1->type == Leave_Menu) return -1;
	else if (e2->type == Leave_Menu) return 1;
	else if (e1->type == SD_Sub_Menu && e2->type != SD_Sub_Menu) return -1;
	else if (e1->type != SD_Sub_Menu && e2->type == SD_Sub_Menu) return 1;
	else return strcasecmp(e1->entryname, e2->entryname);
}




const char const * const menuFontNames[] __attribute__((section(".flash01"))) = {
		// same ordering as font IDs
		MENU_TEXT_FONT_TJZ,
		MENU_TEXT_FONT_TRICHOTOMIC12,
		MENU_TEXT_FONT_CAPTAIN_MORGAN_SPICE,
		MENU_TEXT_FONT_GLACIER_BELLE
};

const char const * const tvModes[] __attribute__((section(".flash01"))) = {
		0,
		MENU_TEXT_TV_MODE_NTSC,		// -->1
		MENU_TEXT_TV_MODE_PAL,		// -->2
		MENU_TEXT_TV_MODE_PAL60,	// -->3
};

const char const * const spacingModes[] __attribute__((section(".flash01"))) = {
		MENU_TEXT_SPACING_DENSE,		// referenced by SPACING enum
		MENU_TEXT_SPACING_MEDIUM,
		MENU_TEXT_SPACING_SPARSE,
};

// _mg_ Lots of const's to try and force this old GCC version to put const string in FLASH, not RAM!!
const char const * const error_in_main  __attribute__((section(".flash01"))) = "Oops: main_plus.c";

enum e_status_message buildMenuFromPath( MENU_ENTRY *d )  {
	bool loadStore = false; // ToDo rename to loadPath (could be SD, flash or WiFi path)

	if( num_menu_entries !=0)
	{
		// _mg_ If this is not our first rodeo, then we may need to free up some memory allocated in previous run.
		for(int i = 0; i< num_menu_entries; i++)
		{
			if( menu_entries[i].full_filename){
				free(menu_entries[i].full_filename);
				menu_entries[i].full_filename=0;
			}
		}
	}
	num_menu_entries = 0;
	enum e_status_message menuStatusMessage = STATUS_NONE;


	MENU_ENTRY *dst = (MENU_ENTRY *)&menu_entries[0];
	dst->full_filename=0;
		// and this caters for the trailing slash in the setup string (if present)
//	char *mtsap = mts + sizeof(MENU_TEXT_APPEARANCE);
#ifdef KEYBOARD_ENABLE
	if (d->type == Input_Field || d->type == Keyboard_Char || d->type == Keyboard_Row ||
			d->type == Delete_Keyboard_Char || d->type == Leave_SubKeyboard_Menu ){
		// toDo  Input_Field to Leave_SubKeyboard_Menu consecutive!
		int new_status = 1;
		if (strstr(curPath, MENU_TEXT_SEARCH_FOR_ROM) == curPath){
		    new_status = STATUS_SEARCH_DETAILS;
		}else{  // All Setup menu stuff here!
			char *mts = curPath + sizeof(MENU_TEXT_SETUP);   // does a +1 because of MENU_TEXT_SETUP trailing 0
		    if(strstr(mts, MENU_TEXT_PLUS_CONNECT) == mts)
		        new_status = plus_connect;
		    else if(strstr(mts, MENU_TEXT_WIFI_SETUP) == mts)
		        new_status = insert_password;
		    else
		        new_status = STATUS_YOUR_MESSAGE;
		}
		if (d->type == Input_Field)
			*input_field = 0;

		menuStatusMessage = generateKeyboard(&dst, d, menuStatusMessage, new_status);
	}
	else
#endif	// KEYBOARD_ENABLE
		if(strstr(curPath, MENU_TEXT_SETUP) == curPath)
	{
		char *mts = curPath + sizeof(MENU_TEXT_SETUP);   // does a +1 because of MENU_TEXT_SETUP trailing 0

		if (!strcmp(curPath, MENU_TEXT_SETUP)){
			menuStatusMessage = STATUS_SETUP;
			dst = generateSetupMenu(dst);
			loadStore = true;
		}

//		else if (strstr(mts, MENU_TEXT_APPEARANCE) == mts) {
//			dst = generateAppearanceMenu(dst);
//			loadStore = true;
//		}
#ifdef USE_WIFI	// _mg_ Bug fix; if WiFi not enable, the system-info sub-menu did not get built.
		else if (strstr(mts, URLENCODE_MENU_TEXT_SYSTEM_INFO) == mts) {
			menuStatusMessage = STATUS_SETUP_SYSTEM_INFO;
			dst = generateSystemInfo(dst);
			loadStore = true;
		}
#else
		else if (strstr(mts, MENU_TEXT_SYSTEM_INFO) == mts) {
			menuStatusMessage = STATUS_SETUP_SYSTEM_INFO;
			HAL_Delay(1500ul);	// _mg_ let the spinner spin as a test.
			dst = generateSystemInfo(dst);
			loadStore = true;
		}
#endif
#ifdef USE_SD_CARD
		else if (strstr(mts, MENU_TEXT_FORMAT_SD_CARD) == mts) {
			menuStatusMessage = fs_format_exFAT() ? done : failed;
			dst = generateSetupMenu(dst);
			loadStore = true;
		}
#endif
#ifdef USE_WIFI
		// WiFi Setup
		else if (strstr(mts, MENU_TEXT_WIFI_SETUP) == mts) {

			int i = sizeof(MENU_TEXT_SETUP) + sizeof(MENU_TEXT_WIFI_SETUP) - 1;

			if ( strlen(curPath) <= i ){

				set_menu_status_msg(curPath);

				make_menu_entry(&dst, MENU_TEXT_GO_BACK, Leave_Menu);
				make_menu_entry(&dst, MENU_TEXT_WIFI_SELECT, Setup_Menu);
				make_menu_entry(&dst, MENU_TEXT_WIFI_WPS_CONNECT, Menu_Action);
				make_menu_entry(&dst, MENU_TEXT_WIFI_MANAGER, Menu_Action);
				make_menu_entry(&dst, MENU_TEXT_ESP8266_RESTORE, Menu_Action);
				if(compVersions(esp8266_at_version, CURRENT_ESP8266_FIRMWARE) == -1)
					make_menu_entry(&dst, MENU_TEXT_ESP8266_UPDATE, Menu_Action);

			}

			else {

				mts += sizeof(MENU_TEXT_WIFI_SETUP);

				if (strstr(mts, MENU_TEXT_WIFI_SELECT) == mts) {

					i += (int) sizeof(MENU_TEXT_WIFI_SELECT);
					if (strlen(curPath) > i){

						if(d->type == Menu_Action){ // if actual Entry is of type Menu_Action -> Connect to WiFi
							// curPath is:
							// MENU_TEXT_SETUP "/" MENU_TEXT_WIFI_SETUP "/" MENU_TEXT_WIFI_SELECT "/" ssid[33] "/" Password "/Enter" '\0'
							truncate_curPath(); // delete "/Enter" at end of Path

							// TODO before we send them to esp8266 escape , " and \ in SSID and Password..
					        while( curPath[i] != 30 && i < ( SIZEOF_WIFI_SELECT_BASE_PATH + 31) ){
					            i++;
					        }
					        curPath[i] = 0;

					    	if(esp8266_wifi_connect( &curPath[SIZEOF_WIFI_SELECT_BASE_PATH  ],
					    			&curPath[SIZEOF_WIFI_SELECT_BASE_PATH + 33])){
					        	menuStatusMessage = wifi_connected;
					    	}else{
					        	menuStatusMessage = wifi_not_connected;
					    	}
							curPath[0] = '\0';
						}
					}

					else {
						menuStatusMessage = select_wifi_network;
						make_menu_entry(&dst, MENU_TEXT_GO_BACK, Leave_Menu);
						if( esp8266_wifi_list( &dst, &num_menu_entries) == false){
				    		return esp_timeout;
				    	}
					}

				}
				else if (strstr(mts, MENU_TEXT_WIFI_WPS_CONNECT) == mts) {

			    	menuStatusMessage = esp8266_wps_connect() ? wifi_connected : wifi_not_connected;
					*curPath = 0;
					HAL_Delay(2000);
				}

				else if (strstr(mts, MENU_TEXT_WIFI_MANAGER) == mts) {

					menuStatusMessage = done;
					esp8266_AT_WiFiManager();
			    	*curPath = 0;
				}

				else if (strstr(mts, MENU_TEXT_ESP8266_RESTORE) == mts) {

					menuStatusMessage = esp8266_reset(true) ? done : failed;
					*curPath = 0;
				}
				else if (strstr(mts, MENU_TEXT_ESP8266_UPDATE) == mts) {
					esp8266_update();
					truncate_curPath();
					menuStatusMessage = buildMenuFromPath(d);
				}
			}
		}
#endif
		else if (strstr(mts, MENU_TEXT_FORMAT_EEPROM) == mts) {
			flash_erase_eeprom();
			truncate_curPath();
			buildMenuFromPath(d);
			menuStatusMessage = done;
		}
		// Display
		else if (strstr(mts, MENU_TEXT_DISPLAY) == mts) {


			int i = sizeof(MENU_TEXT_SETUP) + sizeof(MENU_TEXT_DISPLAY) - 1;
			if (strlen(curPath) <= i) {

				plus_set_menu_status_msg(curPath);

				make_menu_entry(&dst, MENU_TEXT_GO_BACK, Leave_Menu);
				make_menu_entry(&dst, MENU_TEXT_SPACING_SETUP, Setup_Menu);
				make_menu_entry(&dst, MENU_TEXT_FONT_SETUP, Setup_Menu);
				make_menu_entry(&dst, MENU_TEXT_TV_MODE_SETUP, Setup_Menu);
			}

			else {

				mts += sizeof(MENU_TEXT_DISPLAY);
				if (strstr(mts, MENU_TEXT_TV_MODE_SETUP) == mts) {

					if(d->type == Menu_Action){

						uint8_t tvMode = TV_MODE_NTSC;
						while (!strstr(tvModes[tvMode], d->entryname + 1))
							tvMode++;

						plus_set_tv_mode(tvMode);

						if(user_settings.tv_mode != tvMode){
							user_settings.tv_mode = tvMode;
							flash_set_eeprom_user_settings(user_settings);
						}

						truncate_curPath();
						menuStatusMessage = buildMenuFromPath(d);
					}

					else {

						menuStatusMessage = STATUS_SETUP_TV_MODE;
						plus_set_menu_status_msg(curPath);

						make_menu_entry(&dst, MENU_TEXT_GO_BACK, Leave_Menu);

						for (int tv = 1; tv < sizeof tvModes / sizeof *tvModes; tv++) {
							make_menu_entry(&dst, tvModes[tv], Menu_Action);
							if (user_settings.tv_mode == tv)
								*(dst-1)->entryname = CHAR_SELECTION;
						}
					}

				}

				else if (strstr(mts, MENU_TEXT_FONT_SETUP) == mts) {

					if(d->type == Menu_Action){

						uint8_t fontStyle = 0;
						while (!strstr(menuFontNames[fontStyle], d->entryname + 1))
							fontStyle++;

						if(user_settings.font_style != fontStyle){
							user_settings.font_style = fontStyle;
							flash_set_eeprom_user_settings(user_settings);
						}

						truncate_curPath();
						menuStatusMessage = buildMenuFromPath(d);
					}

					else{

						menuStatusMessage = STATUS_SETUP_FONT_STYLE;
						make_menu_entry(&dst, MENU_TEXT_GO_BACK, Leave_Menu);

						for (uint8_t font=0; font < sizeof menuFontNames / sizeof *menuFontNames; font++) {
							make_menu_entry_font(&dst, menuFontNames[font], Menu_Action, font);
							if (user_settings.font_style == font)
								*(dst-1)->entryname = CHAR_SELECTION;
						}
					}

				}

				// Text line spacing
				else if (strstr(mts, MENU_TEXT_SPACING_SETUP) == mts) {

					if(d->type == Menu_Action) {

						uint8_t lineSpacing = 0;
						while (!strstr(spacingModes[lineSpacing], d->entryname + 1))
							lineSpacing++;

						if(user_settings.line_spacing != lineSpacing) {
							user_settings.line_spacing = lineSpacing;
							flash_set_eeprom_user_settings(user_settings);
						}

						truncate_curPath();
						menuStatusMessage = buildMenuFromPath(d);
					}

					else {

						menuStatusMessage = STATUS_SETUP_LINE_SPACING;

						make_menu_entry(&dst, MENU_TEXT_GO_BACK, Leave_Menu);

						for (uint8_t spacing = 0; spacing < sizeof spacingModes / sizeof *spacingModes; spacing++) {
							make_menu_entry(&dst, spacingModes[spacing], Menu_Action);
							if (user_settings.line_spacing == spacing)
								*(dst-1)->entryname = CHAR_SELECTION;
						}
					}
				}
			}
		}

#ifdef MENU_TEXT_OFFLINE_ROM_UPDATE
		else if (strstr(mts, MENU_TEXT_OFFLINE_ROM_UPDATE) == mts) {
#if USE_WIFI
			if( flash_download("&r=1", d->filesize , 0 , false ) != DOWNLOAD_AREA_START_ADDRESS)
				menuStatusMessage = download_failed;
			else {
				menuStatusMessage = done;
	        	*curPath = 0;
			}
#endif
#ifdef USE_SD_CARD
			menuStatusMessage = done;
			*curPath = 0;
#endif
		}
#endif	// MENU_TEXT_OFFLINE_ROM_UPDATE
#ifdef MENU_TEXT_DELETE_OFFLINE_ROMS
		else if (strstr(mts, MENU_TEXT_DELETE_OFFLINE_ROMS) == mts) {

			flash_erase_storage((uint8_t)FLASH_SECTOR_5);
			user_settings.first_free_flash_sector = (uint8_t) FLASH_SECTOR_5;
		    flash_set_eeprom_user_settings(user_settings);
		    menuStatusMessage = offline_roms_deleted;
        	*curPath = 0;
		}
#endif	// MENU_TEXT_DELETE_OFFLINE_ROMS
#ifdef MENU_TEXT_DETECT_OFFLINE_ROMS
		else if (strstr(mts, MENU_TEXT_DETECT_OFFLINE_ROMS) == mts) {

			uint32_t last_address = flash_check_offline_roms_size();
			if(last_address > DOWNLOAD_AREA_START_ADDRESS + 1024){
			    user_settings.first_free_flash_sector = (uint8_t) (((last_address - ADDR_FLASH_SECTOR_5) / 0x20000 ) + 6);
			    flash_set_eeprom_user_settings(user_settings);
			    menuStatusMessage = offline_roms_detected;
			}

			else
				menuStatusMessage = no_offline_roms_detected;

    		num_menu_entries = 0;
        	*curPath = 0;
		}
#endif	// MENU_TEXT_DETECT_OFFLINE_ROMS
		else{
			// unknown entry must be from PlusStore API, so load from store.
			loadStore = true;
		}
	}

	else if (d->type == Menu_Action){
#ifdef MENU_TEXT_FIRMWARE_UPDATE
		if (strstr(curPath, MENU_TEXT_FIRMWARE_UPDATE) == curPath) {	// _mg_ WiFi FW update
			uint32_t bytes_to_read = d->filesize - 0x4000;
#ifdef USE_WIFI
			strcpy(curPath, "&u=1");
			uint32_t bytes_to_ram = d->filesize > FIRMWARE_MAX_RAM ? FIRMWARE_MAX_RAM : d->filesize;
			uint32_t bytes_read = esp8266_PlusStore_API_file_request( buffer, curPath, 0, 0x4000 );

			bytes_read += esp8266_PlusStore_API_file_request( &buffer[0x4000], curPath, 0x8000, (bytes_to_ram - 0x8000));
			if (d->filesize > FIRMWARE_MAX_RAM ){
				bytes_read += esp8266_PlusStore_API_file_request( CCM_RAM, curPath, FIRMWARE_MAX_RAM, ( d->filesize - FIRMWARE_MAX_RAM) );
			}

#else
			uint32_t bytes_read = 0;
#endif

			if(bytes_read == bytes_to_read ){
				__disable_irq();
				HAL_FLASH_Unlock();
				flash_firmware_update(bytes_read);
			}else{
				menuStatusMessage = download_failed;
			}
			*curPath = 0;
		}
		else
#endif	// MENU_TEXT_FIRMWARE_UPDATE
			if (strstr(curPath, MENU_TEXT_SD_FIRMWARE_UPDATE) == curPath)
		{
			uint32_t bytes_to_read = d->filesize - 0x4000;
#ifdef USE_SD_CARD
			uint32_t bytes_to_ram = d->filesize > FIRMWARE_MAX_RAM ? FIRMWARE_MAX_RAM : d->filesize;
			uint32_t bytes_read = sd_card_file_request( buffer, MENU_TEXT_SD_CARD_CONTENT "/firmware.bin", 0, 0x4000 );
			bytes_read += sd_card_file_request( &buffer[0x4000], MENU_TEXT_SD_CARD_CONTENT "/firmware.bin", 0x8000, (bytes_to_ram - 0x8000) );
			if (d->filesize > FIRMWARE_MAX_RAM ){
				bytes_read += sd_card_file_request( CCM_RAM,  MENU_TEXT_SD_CARD_CONTENT "/firmware.bin", FIRMWARE_MAX_RAM, ( d->filesize - FIRMWARE_MAX_RAM) );
			}
#else
			uint32_t bytes_read = 0;
#endif

			if(bytes_read == bytes_to_read ){
				__disable_irq();
				HAL_FLASH_Unlock();
				flash_firmware_update(bytes_read);
			}else{
				menuStatusMessage = download_failed;
			}
			*curPath = 0;
		}
#ifdef USE_SD_CARD
		else if (strstr(curPath, MENU_TEXT_SEARCH_FOR_ROM) == curPath) {
			// Cart with SD and WiFi will search only here (SD) ! -> maybe use "Search SD ROM" ?
			loadStore = false;
			truncate_curPath(); // delete "/Enter"
			make_menu_entry(&dst, "..", Leave_Menu);
			http_request_header[0] = '\0';
			sd_card_find_file( http_request_header, &curPath[sizeof(MENU_TEXT_SEARCH_FOR_ROM)], &dst, &num_menu_entries );
		}
#endif
#ifdef USE_WIFI
		else if (strstr(curPath, MENU_TEXT_WIFI_RECONNECT) == curPath){
			loadStore = true;
			*curPath = 0;
		}else{

			loadStore = true;
		}
#endif

	}

	else {
		plus_set_menu_status_msg(curPath);
		loadStore = true;
	}


	// Test we should load store and if connected to AP
    if(	loadStore || strlen(curPath) == 0 ){
    	int trim_path = 0;
    	if(strlen(curPath) == 0){
#ifdef USE_SD_CARD
    		// check for firmware.bin file in SD root
    		int firmware_file_size = fs_file_size("firmware.bin");
    		if(firmware_file_size > 0){
    			// ToDo make_menu_entry_filesize();
    			dst->filesize = (uint32_t)firmware_file_size;
    			strcpy(dst->entryname, MENU_TEXT_SD_FIRMWARE_UPDATE);
        		dst->type = Menu_Action;
        		dst->font = user_settings.font_style;
                dst++;
                num_menu_entries++;
    		}
#endif
    	}
#ifdef MENU_TEXT_OFFLINE_ROMS
    	if (d->type == Offline_Sub_Menu || strstr(curPath, MENU_TEXT_OFFLINE_ROMS) == curPath) {
    		make_menu_entry(&dst, "..", Leave_Menu);
    		flash_file_list(&curPath[sizeof(MENU_TEXT_OFFLINE_ROMS) - 1], &dst, &num_menu_entries);
    	}
#endif	// MENU_TEXT_OFFLINE_ROMS

#ifdef USE_SD_CARD
    	else if(d->type == SD_Sub_Menu || strstr(curPath, MENU_TEXT_SD_CARD_CONTENT) == curPath){
    		LL_LedGreen_Off();LL_LedBlue_On();	// _mg_ DEBUG
    		if(sd_card_file_list(&curPath[sizeof(MENU_TEXT_SD_CARD_CONTENT) - 1], &dst, &num_menu_entries ))
    			qsort((MENU_ENTRY *)&menu_entries[0], num_menu_entries, sizeof(MENU_ENTRY), entry_compare);
    	}
#endif	// USE_SD_CARD

#ifdef USE_WIFI
    	else if(esp8266_is_connected() == true){
    		*input_field = 0;
    		trim_path = esp8266_file_list(curPath, &dst, &num_menu_entries, plus_store_status, input_field);
    		if(*input_field)
    			menuStatusMessage = STATUS_MESSAGE_STRING;
        }else if(strlen(curPath) == 0){
        	make_menu_entry(&dst, MENU_TEXT_WIFI_RECONNECT, Menu_Action);
    	}
#endif
        if(trim_path){
        	inputActive = 0; // API response trim overrules internal truncate
        	                 // toDo merge trim_path and inputActive ? centralize truncate_curPath() call ?
        	while (trim_path--){
        		truncate_curPath();
        	}
        }
    }

    if(strlen(curPath) == 0)
    {
    	if(menuStatusMessage == STATUS_NONE)
    		menuStatusMessage = STATUS_ROOT;

#ifdef USE_SD_CARD
    	LL_LedBlue_Off(); LL_LedGreen_On();	// _mg_ DEBUG
    	make_menu_entry(&dst, MENU_TEXT_SD_CARD_CONTENT, SD_Sub_Menu);
#ifndef USE_WIFI // todo check how to sort man menu and how to search
    	make_menu_entry(&dst, MENU_TEXT_SEARCH_FOR_ROM, Input_Field);
#endif
#endif
#ifdef MENU_TEXT_OFFLINE_ROMS
    	if(	flash_has_downloaded_roms() )
    		make_menu_entry(&dst, MENU_TEXT_OFFLINE_ROMS, Offline_Sub_Menu);
#endif	// MENU_TEXT_OFFLINE_ROMS
    	make_menu_entry(&dst, MENU_TEXT_SETUP, Setup_Menu);
	}

    if(num_menu_entries == 0){
    	// _mg_ end up here if something above is not correct, e.g. no handler for a sub-menu or action-menu, etc.
		make_menu_entry(&dst, "..", Leave_Menu);
		make_menu_entry(&dst,error_in_main, Leave_Menu);
		make_menu_entry(&dst,"L: " G_STRINGIFY(__LINE__), Leave_Menu);
    }

    return menuStatusMessage;
}
//	END  of  buildMenuFromPath()

int is_pluscart_ace_cartridge(unsigned int image_size, uint8_t *buffer)
{
	return 0;	// _mg_ for now don't support special cartPlus ACE file.
	// also note from cartPlus author:
	// - Other forms of ACE are not supported and could do bad things
}

CART_TYPE cp_identify_cartridge( MENU_ENTRY *d )
{
	CART_TYPE cart_type = { base_type_None, false, false, false, false };

	strcat(curPath, "/");
	append_entry_to_path(d);

	// Test if connected to AP
    if(d->type == Cart_File ){
#ifdef USE_WIFI
    	if(esp8266_is_connected() == false)
#endif
    		return cart_type;
    }
    if(d->type == SD_Cart_File ){
#ifndef USE_SD_CARD
   		return cart_type;
#endif
    }

    // select type by file extension?

    char* flnm;    // _mg_ handle truncated long filenames.
    if( d->full_filename != 0){
    	flnm = d->full_filename;
    } else{
    	flnm = d->entryname;
    }

	char *ext = get_filename_ext(flnm);
	const cp_EXT_TO_CART_TYPE_MAP *p = cp_ext_to_cart_type_map;
	while (p->ext) {
		if (strcasecmp(ext, p->ext) == 0) {
			cart_type.base_type = p->cart_type.base_type;
			cart_type.withSuperChip = p->cart_type.withSuperChip;
			cart_type.uses_ccmram = p->cart_type.uses_ccmram;
			cart_type.uses_systick = p->cart_type.uses_systick;
			break;
		}
		p++;
	}

	// Supercharger cartridges get special treatment, since we don't load the entire
	// file into the buffer here
	if (cart_type.base_type == base_type_None && ( (d->filesize % 8448) == 0 || d->filesize == 6144))
		cart_type.base_type = base_type_AR;
	if (cart_type.base_type == base_type_AR){
		goto close;
	}


	uint32_t bytes_read, bytes_to_read = d->filesize > (BUFFER_SIZE_KB * 1024)?(BUFFER_SIZE_KB * 1024):d->filesize;
	uint8_t tail[16], bytes_read_tail=0;
	if(d->type == Cart_File ){
#ifdef USE_WIFI
		bytes_read = esp8266_PlusStore_API_file_request( buffer, curPath, 0, bytes_to_read );
#endif
	}else if(d->type == SD_Cart_File ){
#ifdef USE_SD_CARD
		bytes_read = sd_card_file_request(buffer, curPath, 0, bytes_to_read);
#endif
	}else{
		bytes_read = flash_file_request( buffer, d->flash_base_address, 0, bytes_to_read );
	}

	if( bytes_read != bytes_to_read ){
		cart_type.base_type = base_type_Load_Failed;
		goto close;
	}
	if(d->filesize >  (BUFFER_SIZE_KB * 1024)){
		cart_type.uses_ccmram = true;
		if(d->type == Cart_File ){
#ifdef USE_WIFI
			bytes_read_tail = (uint8_t)esp8266_PlusStore_API_file_request( tail, curPath, (d->filesize - 16), 16 );
#endif
		}else if(d->type == SD_Cart_File ){
#ifdef USE_SD_CARD
			bytes_read_tail = (uint8_t)sd_card_file_request( tail, curPath, (d->filesize - 16), 16 );
#endif
		}else{
			bytes_read_tail = (uint8_t)flash_file_request( tail, d->flash_base_address, (d->filesize - 16), 16 );
		}
		if( bytes_read_tail != 16 ){
			cart_type.base_type = base_type_Load_Failed;
			goto close;
		}
	}else{
		cart_type.withPlusFunctions = isProbablyPLS(d->filesize, buffer);
		cart_type.withSuperChip =  isProbablySC(d->filesize, buffer);
	}

	// disconnect here or if cart_type != CART_TYPE_NONE
	if (cart_type.base_type != base_type_None) goto close;

	// If we don't already know the type (from the file extension), then we
	// auto-detect the cart type - largely follows code in Stella's CartDetector.cpp

	if (d->filesize <= 64 * 1024 && (d->filesize % 1024) == 0 && isProbably3EPlus(d->filesize, buffer))
	{
		cart_type.base_type = base_type_3EPlus;
	}
	else if (d->filesize == 2*1024)
	{
		if (isProbablyCV(d->filesize, buffer))
			cart_type.base_type = base_type_CV;
		else
			cart_type.base_type = base_type_2K;
	}
	else if (d->filesize == 4*1024)
	{
		cart_type.base_type = base_type_4K;
	}
	else if (d->filesize == 8*1024)
	{
		// First check for *potential* F8
		int f8 = isPotentialF8(d->filesize, buffer);

		if (memcmp(buffer, buffer + 4096, 4096) == 0)
			cart_type.base_type = base_type_4K;
		else if (isProbablyE0(d->filesize, buffer))
			cart_type.base_type = base_type_E0;
		else if (isProbably3E(d->filesize, buffer))
			cart_type.base_type = base_type_3E;
		else if (isProbably3F(d->filesize, buffer))
			cart_type.base_type = base_type_3F;
		else if (isProbablyUA(d->filesize, buffer))
			cart_type.base_type = base_type_UA;
		else if (isProbablyFE(d->filesize, buffer) && !f8)
			cart_type.base_type = base_type_FE;
		else if (isProbably0840(d->filesize, buffer))
			cart_type.base_type = base_type_0840;
		else
			cart_type.base_type = base_type_F8;
	}
	else if (d->filesize == 8*1024 + 3) {
		cart_type.base_type = base_type_PP;
	}
	else if(d->filesize >= 10240 && d->filesize <= 10496)
	{  // ~10K - Pitfall II
		cart_type.uses_ccmram = true;
		cart_type.uses_systick = true;
		cart_type.base_type = base_type_DPC;
	}
	else if (d->filesize == 12*1024)
	{
		cart_type.base_type = base_type_FA;
	}
	else if (d->filesize == 16*1024)
	{
		if (isProbablyE7(d->filesize, buffer))
			cart_type.base_type = base_type_E7;
		else if (isProbably3E(d->filesize, buffer))
			cart_type.base_type = base_type_3E;
		else
			cart_type.base_type = base_type_F6;
	}
	else if (d->filesize == 29*1024)
	{
		if (isProbablyDPCplus(d->filesize, buffer))
			cart_type.base_type = base_type_DPCplus;
	}
	else if (d->filesize == 32*1024)
	{
		if (isProbably3E(d->filesize, buffer))
			cart_type.base_type = base_type_3E;
		else if (isProbably3F(d->filesize, buffer))
			cart_type.base_type = base_type_3F;
		else if (isProbablyDPCplus(d->filesize, buffer)){
			cart_type.base_type = base_type_DPCplus;
			cart_type.uses_systick = true;
		}
		else
			cart_type.base_type = base_type_F4;
	}
	else if (d->filesize == 64*1024)
	{
		if (isProbably3E(d->filesize, buffer))
			cart_type.base_type = base_type_3E;
		else if (isProbably3F(d->filesize, buffer))
			cart_type.base_type = base_type_3F;
		else if (isProbablyEF(d->filesize, buffer))
			cart_type.base_type = base_type_EF;
		else
			cart_type.base_type = base_type_F0;
	}
	else if (d->filesize == 128 * 1024) {
		if (isProbablyDF(tail))
			cart_type.base_type = base_type_DF;
		else if (isProbablyDFSC(tail)){
			cart_type.base_type = base_type_DFSC;
			cart_type.withSuperChip = 1;
		}else
			cart_type.base_type = base_type_SB;
	}
	else if (d->filesize == 256 * 1024)
	{
		if (isProbablyBF(tail))
			cart_type.base_type = base_type_BF;
		else if (isProbablyBFSC(tail)){
			cart_type.base_type = base_type_BFSC;
			cart_type.withSuperChip = 1;
		}else
			cart_type.base_type = base_type_SB;
	}

	close:

	if (cart_type.base_type != base_type_None)
		cart_size_bytes = d->filesize;

	return cart_type;
}


void plus_emulate_cartridge(CART_TYPE cart_type, MENU_ENTRY *d)
{
	int offset = 0;


#ifdef USE_WIFI
	if (cart_type.withPlusFunctions == true ){
 		// Read path and hostname in ROM File from where NMI points to till '\0' and
		// copy to http_request_header
		offset = esp8266_PlusROM_API_connect(cart_size_bytes);
	}
#endif


	if (cart_type.base_type == base_type_2K) {
		cart_size_bytes = 2048;
		emulate_2k_cartridge();		// use UnoCart emulator, it actually works!
//		memcpy(buffer+0x800, buffer, 0x800);
//		emulate_standard_cartridge(offset, cart_type.withPlusFunctions, 0x2000, 0x0000, cart_type.withSuperChip);
	}

	else if (cart_type.base_type ==  base_type_4K)
		emulate_standard_cartridge(offset, cart_type.withPlusFunctions, 0x2000, 0x0000, cart_type.withSuperChip);

	else if (cart_type.base_type == base_type_F8)
		emulate_standard_cartridge(offset, cart_type.withPlusFunctions, 0x1FF8, 0x1FF9, cart_type.withSuperChip);

	else if (cart_type.base_type == base_type_F6)
		emulate_standard_cartridge(offset, cart_type.withPlusFunctions, 0x1FF6, 0x1FF9, cart_type.withSuperChip);

	else if (cart_type.base_type == base_type_F4)
		emulate_standard_cartridge(offset, cart_type.withPlusFunctions, 0x1FF4, 0x1FFB, cart_type.withSuperChip );

	else if (cart_type.base_type == base_type_FE)
		emulate_FE_cartridge();

	else if (cart_type.base_type == base_type_UA)
		emulate_UA_cartridge(cart_size_bytes, buffer);

	else if (cart_type.base_type == base_type_3F)
		emulate_3f_cartridge(NULL, cart_size_bytes, buffer); // _mg_ NULL for filename since we don't want to write to FLASH.

	else if (cart_type.base_type == base_type_3E)	// _mg_ NULL for filename since we don't want to write to FLASH.
		emulate_3e_cartridge(NULL, cart_size_bytes, buffer, 32 /*offset, cart_type.withPlusFunctions  _mg_ */);

	else if (cart_type.base_type == base_type_E0)
		emulate_E0_cartridge();

	else if (cart_type.base_type == base_type_0840)
		emulate_0840_cartridge();

	else if (cart_type.base_type == base_type_CV)
		emulate_CV_cartridge();

	else if (cart_type.base_type == base_type_EF)
		emulate_standard_cartridge(offset, cart_type.withPlusFunctions, 0x1FE0, 0x1FEF, cart_type.withSuperChip);

	else if (cart_type.base_type == base_type_F0)
		emulate_F0_cartridge();

	else if (cart_type.base_type == base_type_FA)
		emulate_FA_cartridge(offset, cart_type.withPlusFunctions);

	else if (cart_type.base_type == base_type_E7)
		emulate_E7_cartridge();

	else if (cart_type.base_type == base_type_DPC)
// _mg_		emulate_DPC_cartridge((uint32_t)cart_size_bytes); // Todo: _mg_ use this cartPlus emulator version???
		emulate_dpc_cartridge(buffer, (uint32_t)cart_size_bytes);

	else if (cart_type.base_type == base_type_AR)
		emulate_ar_cartridge(curPath, cart_size_bytes, buffer, user_settings.tv_mode, d);

	else if (cart_type.base_type == base_type_PP)
		emulate_pp_cartridge( cart_size_bytes, buffer, buffer + 8*1024);

	else if (cart_type.base_type == base_type_DF)
		emulate_df_cartridge(curPath, cart_size_bytes, buffer/* _mg_ , d*/);

	else if (cart_type.base_type == base_type_DFSC)
		emulate_dfsc_cartridge(curPath, cart_size_bytes, buffer/* _mg_ , d*/);

	else if (cart_type.base_type == base_type_BF)
		emulate_bf_cartridge(curPath, cart_size_bytes, buffer/* _mg_ , d*/);

	else if (cart_type.base_type == base_type_BFSC)
		emulate_bfsc_cartridge(curPath, cart_size_bytes, buffer/* _mg_ , d*/);

	else if (cart_type.base_type == base_type_3EPlus)
// _mg_		emulate_3EPlus_cartridge(offset, cart_type.withPlusFunctions);	// todo: _mg_ use this cartPlus emulator version here??
		emulate_3EPlus_cartridge(buffer, (uint32_t)cart_size_bytes);

/* _mg_ remove cartPlus DPC+ emulation.  Original file: cartridge_emulation_dpcp.c
	else if (cart_type.base_type == base_type_DPCplus)
		emulate_DPCplus_cartridge(cart_size_bytes);
_mg_ */

	else if (cart_type.base_type == base_type_SB)
		emulate_SB_cartridge(curPath, cart_size_bytes, buffer, d);

	else if (cart_type.base_type == base_type_ACE)
	{
// _mg_		static unsigned char CCMUsageFinder __attribute__((section(".ccmram#"))); //Method of finding where allocation has reached for CCM RAM
// _mg_		uint32_t* CCMpointer=(uint32_t*)&CCMUsageFinder; //Find address of CCM allocation and cast as a pointer
// _mg_		launch_ace_cartridge(curPath, cart_size_bytes, buffer, d, offset, cart_type.withPlusFunctions,CCMpointer); //Open the ACE bootloader library function
		launch_ace_cartridge(curPath, cart_size_bytes, buffer); //Open the ACE bootloader library function
	}

#ifdef USE_WIFI
	if (cart_type.withPlusFunctions)
		esp8266_PlusStore_API_end_transmission();
#endif

}

/**
 * Trim curPath to last / OR if none, whole path
 */
void truncate_curPath(){
#ifdef KEYBOARD_ENABLE
/* _mg_ remove keyboard stuff if not used */
	for (int selector = 0; keyboards[selector]; selector++){
		for (const char **kbRow = keyboards[selector]; *kbRow; kbRow++) {
			char *kb = strstr(curPath, *kbRow);
			if (kb) {
				*(kb-1) = 0;
				return;
			}
		}
	}
#endif
	// trim to last / OR if none, whole path
	char *sep = strrchr(curPath, PATH_SEPERATOR);
	if (!sep)
		sep = curPath;
	*sep = 0;
}

void system_secondary_init(void){
	//	check user_settings properties that haven't been in user_setting since v1
	if( user_settings.line_spacing >= SPACING_MAX )
		user_settings.line_spacing = SPACING_DEFAULT;

	if( user_settings.font_style >= FONT_MAX )
		user_settings.font_style = FONT_DEFAULT;

	plus_set_menu_status_byte(STATUS_StatusByteReboot, 0);
#ifdef STM32UDID_ENABLE
	generate_udid_string();
#endif
}

void append_entry_to_path(MENU_ENTRY *d){
    char* flnm;    // _mg_ Added handling of truncated long filenames.
    if( d->full_filename){
    	flnm = d->full_filename;
    }else{
    	flnm = d->entryname;
    }
	if(d->type == Cart_File || d->type == Sub_Menu	)
		for (char *p = flnm; *p; p++)
			// FIXME: _mg_ DANGER, no buffer overflow protection on copying to curPath!
			sprintf(curPath + strlen(curPath), strchr(" =+&#%", *p) ? "%%%02X" : "%c", *p);
	else
		strncat(curPath, flnm, 255);
	curPath[255] = 0;
}

// CartPlus version:
bool is_valid_file(char * filename){
	return true;
}


/**
  * @brief  The application entry point.
  * @retval int
  */
int cp_main(void)
{
	/* USER CODE BEGIN 1 */

	uint8_t act_page = 0;
	MENU_ENTRY *d = &menu_entries[0];
	d->full_filename=0;

	LL_LedGreen_On();
	/* USER CODE END 1 */

	/* USER CODE BEGIN 2 */

	user_settings = flash_get_eeprom_user_settings();
	plus_set_tv_mode(user_settings.tv_mode);

	// _mg_ run the 6507 bootstrapper before we do time intensive activities
	int ret = emulate_firmware_cartridge(1);
	if(!no_init_section_init_done)
	{
		init_the_no_init();
	}
	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	enum e_status_message menuStatusMessage = STATUS_ROOT; //, main_status = none;
	while (1)
	{
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */

		// _mg_ When get here, the Atari 2600's 6507 is running code from the 2600's internal RAM.
		// _mg_ Therefore the STM32 has more time to do things here ...
		// FIXME:  _mg_ Workaround: After coming out of firmware emulation, for some reason, the UART3 peripheral is getting corrupted and requires disable/re-enable.
		// Root cause of peripheral corruption is unknown.  Ground bounce on cartridge interface?
		MX_USART3_DeInit();
		MX_USART3_UART_Init();
		myprintf("Cart+ %s\n", MG_VERSION);
		if (ret == CART_CMD_ROOT_DIR)
		{
			system_secondary_init();

			d->type = Root_Menu;
			d->filesize = 0;

			*input_field = *curPath = 0;
			inputActive = 0;
			HAL_Delay(500ul);	// allow SD card to init if just powered on.
			menuStatusMessage = buildMenuFromPath(d);
		}
		else if (ret == CART_CMD_PAGE_DOWN) {
			act_page--;
		}

		else if (ret == CART_CMD_PAGE_UP) {
			act_page++;
		}
		else
		{
			// _mg_ ret has selection # 0 - numMenuItemsPerPage.
			ret += act_page * numMenuItemsPerPage[user_settings.line_spacing];
			d = &menu_entries[ret];

			act_page = 0; // seems to fix the "blank" menus - because page # was not init'd on new menu

			if (d->type == Cart_File || d->type == Offline_Cart_File || d->type == SD_Cart_File)
			{
#ifdef STM32F767xx
				// selection is a rom file
				int flash_sectors = (STM32F7_FLASH_SIZE > 512UL) ? 12 : 8;
#else
				// selection is a rom file
				int flash_sectors = (STM32F4_FLASH_SIZE > 512U) ? 12 : 8;
#endif
				(void) flash_sectors;
				// FIXME: _mg_ Flash memory sector size differnt on F767
				// _mg_ for now for safety, set max size to only RAM buffer.
				int32_t max_romsize ;
				max_romsize = ((BUFFER_SIZE_KB + CCM_SIZE_KB) * 1024)
					/*	+ (flash_sectors - user_settings.first_free_flash_sector ) * 128 * 1024 */;
				if (d->filesize > max_romsize)
				{
					menuStatusMessage = not_enough_menory;
				}
				else
				{
					CART_TYPE cart_type = cp_identify_cartridge(d);
					// Todo: _mg_ UART output the cartridge type from ext Map in: cp_ext_to_cart_type_map[].
					const char* s_type;
					s_type = lookup_ext_from_type(cart_type.base_type);
					myprintf("GoROM: Type: %d [%s]  curPath: [%s]\n", cart_type.base_type, s_type, curPath);
					HAL_Delay(200);

					if (cart_type.base_type == base_type_ACE && !(is_pluscart_ace_cartridge(d->filesize, buffer)))
						menuStatusMessage = romtype_ACE_unsupported;

					else if (cart_type.base_type == base_type_Load_Failed)
						menuStatusMessage = rom_download_failed;

					else if (cart_type.base_type != base_type_None)
					{
						plus_emulate_cartridge(cart_type, d);
						plus_set_menu_status_byte(STATUS_StatusByteReboot, 0);
						menuStatusMessage = exit_emulation;

						if(cart_type.uses_systick)
						{
							SysTick_Config(SystemCoreClock / 1000U);	// 1KHz
						}
					}

					else
						menuStatusMessage = romtype_unknown;
				}

				truncate_curPath();

				d->type = Sub_Menu;
				buildMenuFromPath(d);

			}
			else
			{  // not a cart file...

				// selection is a directory or Menu_Action, or Keyboard_Char
				if (d->type == Leave_Menu)
				{
					inputActive++;
					while(inputActive--)
						truncate_curPath();

					inputActive = 0;
					*input_field = 0;
				}
#ifdef KEYBOARD_ENABLE
				else if (d->type == Leave_SubKeyboard_Menu)
				{
				}
				else if (d->type == Delete_Keyboard_Char)
				{
					unsigned int len = strlen(input_field);
					if (len) {
						input_field[--len] = 0;
						curPath[strlen(curPath) - 1] = 0;
					}

				} else
				{

					if (d->type != Keyboard_Char && strlen(curPath) > 0 ) {
						strcat(curPath, "/");
					}
					else if (d->type == Keyboard_Char && !strcmp(d->entryname, MENU_TEXT_SPACE))
						strcpy(d->entryname, " ");
#else
					else
					{
						if ( strlen(curPath) > 0 ) {
							strcat(curPath, "/");
						}
#endif
						append_entry_to_path(d);
#ifdef KEYBOARD_ENABLE
						if (d->type == Keyboard_Char) {

							if (strlen(input_field) + strlen(d->entryname) < STATUS_MESSAGE_LENGTH - 1)
								strcat(input_field, d->entryname);

						}

						else if (d->type == Input_Field) {
							strcat(curPath, "/");
							inputActive++; // = 1 ???
						}
						else
#endif
						{
							if (d->type == Menu_Action) {
								if(inputActive)
									inputActive += 2; // input + "Enter", if input contains path_sep trim will be corrected by API
								*input_field = 0;
							}
						}
					}
					menuStatusMessage = buildMenuFromPath(d);
				}	// End of Not A Cart File... else clause.
		} // End of ret action checking.


		if (*input_field || menuStatusMessage == STATUS_MESSAGE_STRING) {
			plus_set_menu_status_msg(input_field);
			plus_set_menu_status_byte(STATUS_PageType, (uint8_t) Keyboard);
		}

		else {

			if (menuStatusMessage >= STATUS_ROOT)
				plus_set_menu_status_msg(status_message[menuStatusMessage]);

			if(act_page > (num_menu_entries / numMenuItemsPerPage[user_settings.line_spacing]) )
				act_page = 0;

			plus_set_menu_status_byte(STATUS_PageType, (uint8_t) Directory);
		}
		#if USE_WIFI
		bool is_connected = esp8266_is_connected();
		#else
		bool is_connected = false;
		#endif
		createMenuForAtari(menu_entries, act_page, num_menu_entries, is_connected, plus_store_status );
		HAL_Delay(200);
		// Check in with the 6507 CPU
		ret = emulate_firmware_cartridge(1);
	}	// End of While(1) loop
}

#endif		// CARTPLUS_ENABLE

