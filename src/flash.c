#if defined (STM32F4XX)
#include "stm32f4xx_flash.h"
#elif defined (STM32F767xx)
#include "stm32f7xx_hal.h"
#include "stm32f7xx_hal_flash.h"
#include "stm32f7xx_hal_flash_ex.h"
#endif

#include "flash.h"

extern uint8_t _END_OF_FLASH;
#define FIRST_FREE_BYTE (uint32_t)(&_END_OF_FLASH)


#if defined (STM32F4XX)
#define DEVICE_ID_FLASH_SIZE ((uint16_t*)0x1fff7a22)
#define RESERVED_FLASH_KB	(64)
#define FLASH_SECTORS		(24)
#define FLASH_TIMEOUT			/* not required for STM32F4XX driver */
static const uint8_t flash_sector[] = {
	FLASH_Sector_0,
	FLASH_Sector_1,
	FLASH_Sector_2,
	FLASH_Sector_3,
	FLASH_Sector_4,
	FLASH_Sector_5,
	FLASH_Sector_6,
	FLASH_Sector_7,
	FLASH_Sector_8,
	FLASH_Sector_9,
	FLASH_Sector_10,
	FLASH_Sector_11,
	FLASH_Sector_12,
	FLASH_Sector_13,
	FLASH_Sector_14,
	FLASH_Sector_15,
	FLASH_Sector_16,
	FLASH_Sector_17,
	FLASH_Sector_18,
	FLASH_Sector_19,
	FLASH_Sector_20,
	FLASH_Sector_21,
	FLASH_Sector_22,
	FLASH_Sector_23
};
static const uint32_t sector_boundaries[] = {
	0x08004000,	// 16K
	0x08008000,	// 16K
	0x0800c000,	// 16K
	0x08010000,
	0x08020000,
	0x08040000,
	0x08060000,
	0x08080000,
	0x080a0000,
	0x080c0000,
	0x080e0000,
	0x08100000,
	// STM32F42/STM32F43
	0x08104000,
	0x08108000,
	0x0810c000,
	0x08110000,
	0x08120000,
	0x08140000,
	0x08160000,
	0x08180000,
	0x081a0000,
	0x081c0000,
	0x081e0000,
	0x08200000
};

#elif defined (STM32F767xx)
#define DEVICE_ID_FLASH_SIZE ((uint16_t*)FLASHSIZE_BASE)	// 0x1FF0F442
#define FLASH_COMPLETE		HAL_OK
#define FLASH_TIMEOUT		5000

#define VoltageRange_3	FLASH_VOLTAGE_RANGE_3
// number of sectors and sector addresses are for dual memory bank mode.
// datasheet indicates, single bank mode is the default.
// Todo: _mg_ adjust the sector address table for single bank mode.
//#define FLASH_DUAL_BANK		/* define if MCU is in dual bank mode. */

#ifdef FLASH_DUAL_BANK
// Dual bank mode
#define RESERVED_FLASH_KB	(112)	// space reserved for STM32F767 code.
#define FLASH_SECTORS		(24)

static const uint8_t flash_sector[] = {
		FLASH_SECTOR_0,
		FLASH_SECTOR_1,
		FLASH_SECTOR_2,
		FLASH_SECTOR_3,
		FLASH_SECTOR_4,
		FLASH_SECTOR_5,
		FLASH_SECTOR_6,
		FLASH_SECTOR_7,
		FLASH_SECTOR_8,
		FLASH_SECTOR_9,
		FLASH_SECTOR_10,
		FLASH_SECTOR_11,
		FLASH_SECTOR_12,
		FLASH_SECTOR_13,
		FLASH_SECTOR_14,
		FLASH_SECTOR_15,
		FLASH_SECTOR_16,
		FLASH_SECTOR_17,
		FLASH_SECTOR_18,
		FLASH_SECTOR_19,
		FLASH_SECTOR_20,
		FLASH_SECTOR_21,
		FLASH_SECTOR_22,
		FLASH_SECTOR_23,
};

// upper address+1 of each sector
static const uint32_t sector_boundaries[] = {
		0x08004000,	// 16K
		0x08008000,	// 16K
		0x0800c000,	// 16K
		0x08010000,	// 64K
		0x08020000,	// 64K
		0x08040000,	// 128K
		0x08060000,	// 128K
		0x08080000,
		0x080a0000,
		0x080c0000,
		0x080e0000,
		0x08100000,	// 128K
		// Bank 2
		0x08104000,	// 16K
		0x08108000,	// 16K
		0x0810c000,	// 16K
		0x08110000,	// 64K
		0x08120000,	// 64K
		0x08140000,	// 128K
		0x08160000,	// 128K
		0x08180000,
		0x081a0000,
		0x081c0000,
		0x081e0000,
		0x08200000 	// 128K
};
#else
// single memory bank mode
#define RESERVED_FLASH_KB	(192)	// space reserved for STM32F767 code.
#define FLASH_SECTORS		(12)

static const uint8_t flash_sector[] = {
		FLASH_SECTOR_0,
		FLASH_SECTOR_1,
		FLASH_SECTOR_2,
		FLASH_SECTOR_3,
		FLASH_SECTOR_4,
		FLASH_SECTOR_5,
		FLASH_SECTOR_6,
		FLASH_SECTOR_7,
		FLASH_SECTOR_8,
		FLASH_SECTOR_9,
		FLASH_SECTOR_10,
		FLASH_SECTOR_11,
};
// upper address+1 of each sector
static const uint32_t sector_boundaries[] = {
		0x08008000,	// 32K
		0x08010000,	// 32K
		0x08018000,	// 32K
		0x08020000,	// 32K
		0x08040000,	// 128K
		0x08080000,	// 256K
		0x080C0000,	// 256K
		0x08100000,	// 256K
		0x08140000,	// 256K
		0x08180000,	// 256K
		0x081C0000,	// 256K
		0x08200000,	// 256K
};
#endif	// #ifdef FLASH_DUAL_BANK

#endif	// #elif defined (STM32F767xx)

/**
 * @brief Return total size of MCU flash.
 * @return Number of bytes in flash memory.
 */
uint32_t flash_size_bytes() {
	return (*DEVICE_ID_FLASH_SIZE) * 1024;
}

static uint32_t highest_flash_address() {
	return 0x08000000 + flash_size_bytes() - 1;
}

static uint8_t sector_id_for_address(uint32_t address) {
	if (address < 0x08000000) return 0xff;
	for (uint8_t i = 0; i < FLASH_SECTORS; i++) {
		if (address < sector_boundaries[i]) return i;
	}

	return 0xff;
}

static uint32_t lowest_available_flash_address() {
	uint8_t firmware_size = FIRST_FREE_BYTE - 0x08000000;

	return (RESERVED_FLASH_KB * 1024) > firmware_size ?
		(0x08000000 + RESERVED_FLASH_KB * 1024) :
		FIRST_FREE_BYTE;
}

/**
 * @brief Return size of flash avaialble to store game cartridge data.
 * @return Number of bytes avaialble in flash memory.
 */
uint32_t available_flash() {
	return flash_size_bytes() - (RESERVED_FLASH_KB * 1024);
}


bool prepare_flash(uint32_t size, flash_context* context) {
	if (size > available_flash() || size == 0) return false;

	const uint8_t first_sector_id = sector_id_for_address(lowest_available_flash_address());
	const uint8_t last_sector_id = sector_id_for_address(lowest_available_flash_address() + size);

	if (first_sector_id == 0xff || last_sector_id == 0xff) return false;
	if (first_sector_id <= sector_id_for_address(lowest_available_flash_address() - 1)) return false;

	FLASH_Unlock();
	if (FLASH_WaitForLastOperation(FLASH_TIMEOUT) != FLASH_COMPLETE) goto error;
#ifdef STM32F4XX
	for (uint8_t sector_id = first_sector_id; sector_id <= last_sector_id; sector_id++) {
		if (FLASH_EraseSector(flash_sector[sector_id], VoltageRange_3) != FLASH_COMPLETE) goto error;
	}
#elif defined (STM32F767xx)
//	FLASH_EraseInitTypeDef eType;

	for (uint8_t sector_id = first_sector_id; sector_id <= last_sector_id; sector_id++) {
		// Todo: _mg_ to have fault tolerance could call HAL_FLASHEx_Erase() which returns status.
		FLASH_Erase_Sector(flash_sector[sector_id], VoltageRange_3);
	}
#endif
	FLASH_Lock();

	context->next_write_target = lowest_available_flash_address();
	context->base = (uint8_t*)(context->next_write_target);

	return true;

	error:
		FLASH_Lock();
		return false;
}


bool write_flash(uint32_t byte_count, uint8_t* buffer, flash_context *context) {
	if (context->next_write_target < lowest_available_flash_address()) return false;
	if (context->next_write_target + byte_count - 1 > highest_flash_address()) return false;

	FLASH_Unlock();
	if (FLASH_WaitForLastOperation(FLASH_TIMEOUT) != FLASH_COMPLETE) goto error;
#ifdef STM32F4XX
	if (((uint32_t)buffer & 0x03) == 0x00 && (context->next_write_target & 0x03) == 0x00 && (byte_count & 0x03) == 0x00) {
		for (uint32_t i = 0; i < byte_count >> 2; i++) {
			if (FLASH_ProgramWord(context->next_write_target, *((uint32_t*)buffer)) != FLASH_COMPLETE)
				goto error;

			context->next_write_target += 4;
			buffer += 4;
		}
	}
	else if (((uint32_t)buffer & 0x01) == 0x00 && (context->next_write_target & 0x01) == 0x00 && (byte_count & 0x01) == 0) {
		for (uint32_t i = 0; i < byte_count >> 1; i++) {
			if (FLASH_ProgramHalfWord(context->next_write_target, *((uint16_t*)buffer)) != FLASH_COMPLETE)
				goto error;

			context->next_write_target += 2;
			buffer += 2;
		}
	}
	else  {
		for (uint32_t i = 0; i < byte_count; i++) {
			if (FLASH_ProgramByte(context->next_write_target, *buffer) != FLASH_COMPLETE)
				goto error;

			context->next_write_target++;
			buffer++;
		}
	}
#elif defined (STM32F767xx)
	if (((uint32_t)buffer & 0x03) == 0x00 && (context->next_write_target & 0x03) == 0x00 && (byte_count & 0x03) == 0x00) {
		for (uint32_t i = 0; i < byte_count >> 2; i++) {
			if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, context->next_write_target, *((uint32_t*)buffer)) != FLASH_COMPLETE)
				goto error;

			context->next_write_target += 4;
			buffer += 4;
		}
	}
	else if (((uint32_t)buffer & 0x01) == 0x00 && (context->next_write_target & 0x01) == 0x00 && (byte_count & 0x01) == 0) {
		for (uint32_t i = 0; i < byte_count >> 1; i++) {
			if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, context->next_write_target, *((uint16_t*)buffer)) != FLASH_COMPLETE)
				goto error;

			context->next_write_target += 2;
			buffer += 2;
		}
	}
	else  {
		for (uint32_t i = 0; i < byte_count; i++) {
			if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, context->next_write_target, *buffer) != FLASH_COMPLETE)
				goto error;

			context->next_write_target++;
			buffer++;
		}
	}
#endif
	FLASH_Lock();
	return true;

	error:
		FLASH_Lock();
		return false;
}


// --- BEGIN ---  cartPlus flash items -----
#include "main.h"	// _mg_ To include macro CARTPLUS_ENABLE
#ifdef CARTPLUS_ENABLE

#include <string.h>
#include <stdlib.h>
#include "cartridge_firmware_plus.h"

#define FLASH_TIMEOUT_VALUE       ((uint32_t)50000U)/* 50 s */

// eeprom emulation defines
#define EEPROM_SECTOR_ID             FLASH_SECTOR_1
#define EEPROM_START_ADDRESS         ((uint32_t)sector_boundaries[FLASH_SECTOR_1])
#define EEPROM_SIZE                  ((uint32_t)0x4000)

#define EEPROM_EMPTY_PAGE_HEADER     ((uint32_t)0xFFFFFFFF)
#define EEPROM_ACTIVE_PAGE_HEADER    ((uint32_t)0x5555FFFF)
#define EEPROM_INVALID_PAGE_HEADER   ((uint32_t)0x55555555)

#define EEPROM_EMPTY_ENTRY_HEADER    ((uint16_t)0xFFFF)
#define EEPROM_ACTIVE_ENTRY_HEADER   ((uint16_t)0x55FF)
#define EEPROM_INVALID_ENTRY_HEADER  ((uint16_t)0x5555)

#define EEPROM_PAGE_HEADER_SIZE      sizeof(EEPROM_EMPTY_PAGE_HEADER)
#define EEPROM_ENTRY_HEADER_SIZE     sizeof(EEPROM_EMPTY_ENTRY_HEADER)
#define EEPROM_PAGE_SIZE             512    // 4 byte page header + 508 byte for entries
#define EEPROM_ENTRY_SIZE             39    // 2 byte entry header + 37 byte payload

#define EEPROM_MAX_PAGE_ID           ((uint8_t)(EEPROM_SIZE / EEPROM_PAGE_SIZE ) -1)
#define EEPROM_MAX_ENTRY_ID          ((uint8_t)((EEPROM_PAGE_SIZE - EEPROM_PAGE_HEADER_SIZE) / EEPROM_ENTRY_SIZE) - 1)

// Download Area in Flash
#define FIRST_FREE_SECTOR            FLASH_SECTOR_5
#define DOWNLOAD_AREA_START_ADDRESS  ((uint32_t)ADDR_FLASH_SECTOR_5)
#define TAR_HEADER_SIZE              512
#define TAR_BLOCK_SIZE               512

// reserve eeprom data storage;
unsigned const char eeprom_data[16384] __attribute__((__section__(".eeprom"), used)) = {[0 ... 16383] = 0xff };


/* Private function prototypes -----------------------------------------------*/
HAL_StatusTypeDef FLASH_WaitInRAMForLastOperationWithMaxDelay(void) __attribute__((section(".data#")));

int16_t get_active_eeprom_page(void);
int16_t get_active_eeprom_page_entry(int16_t);
uint32_t get_filesize(uint32_t);


USER_SETTINGS flash_get_eeprom_user_settings(void){
    USER_SETTINGS user_settings = {TV_MODE_DEFAULT, FIRST_FREE_SECTOR, FONT_DEFAULT, SPACING_DEFAULT};
    int16_t act_page_index = get_active_eeprom_page();
    int16_t act_entry_index = -1;
    if( act_page_index != -1 ){
        act_entry_index = get_active_eeprom_page_entry(act_page_index);
    }
    if (act_entry_index != -1){

    	uint32_t dataIndex = (uint32_t) (act_page_index * EEPROM_PAGE_SIZE) + EEPROM_PAGE_HEADER_SIZE;
    	dataIndex += (uint32_t)(act_entry_index * EEPROM_ENTRY_SIZE) + EEPROM_ENTRY_HEADER_SIZE;

    	user_settings = (*(USER_SETTINGS *)(&eeprom_data[dataIndex]));
    }
    return user_settings;
}

void flash_set_eeprom_user_settings(USER_SETTINGS user_settings){
    int16_t act_entry_index = -1;
    int16_t new_entry_index, new_page_index;
    int16_t act_page_index = get_active_eeprom_page();
    if( act_page_index != -1 ){
        act_entry_index = get_active_eeprom_page_entry(act_page_index);
    }
    new_page_index = act_page_index;
    new_entry_index = (int16_t) (act_entry_index + 1);

    HAL_FLASH_Unlock();

    if(act_entry_index != -1){ // && act_page_index != -1 is always true if act_entry_index != -1 is true
        // make last entry invalid!

    	uint32_t dataIndex = (uint32_t) (act_page_index * EEPROM_PAGE_SIZE) + EEPROM_PAGE_HEADER_SIZE;
    	dataIndex += (uint32_t)(act_entry_index * EEPROM_ENTRY_SIZE);

       	HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, EEPROM_START_ADDRESS + dataIndex, ((uint8_t)0x55) );
    }

    // Page full ?
    if( new_entry_index > EEPROM_MAX_ENTRY_ID ){
           new_entry_index = 0;
           // test if flash is full !!
           if(new_page_index > EEPROM_MAX_PAGE_ID ){
               FLASH_Erase_Sector(EEPROM_SECTOR_ID, (uint8_t) FLASH_VOLTAGE_RANGE_3);
               new_page_index = 0;
           }else{ // invalidate act page

			   uint32_t dataIndex = (uint32_t) (act_page_index * EEPROM_PAGE_SIZE);
			   HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, EEPROM_START_ADDRESS + dataIndex, ((uint32_t)EEPROM_INVALID_PAGE_HEADER) );
           }
    }

    // check for start new page !!
    if( new_entry_index == 0 ){ // new_page_index != act_page_index  &&  new_page_index != -1
           new_page_index++;
           // make new activ page header!

       	   uint32_t dataIndex = (uint32_t) (new_page_index * EEPROM_PAGE_SIZE);
           HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, EEPROM_START_ADDRESS + dataIndex, ((uint32_t)EEPROM_ACTIVE_PAGE_HEADER) );
    }

	uint32_t dataIndex = (uint32_t) (new_page_index * EEPROM_PAGE_SIZE) + EEPROM_PAGE_HEADER_SIZE;
	dataIndex += (uint32_t)(new_entry_index * EEPROM_ENTRY_SIZE);

    uint32_t new_entry_flash_address = EEPROM_START_ADDRESS + dataIndex;

    // make new activ entry header!
    new_entry_flash_address++;
    HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, new_entry_flash_address, ((uint8_t)0x55) );
    new_entry_flash_address++;

    for(uint8_t i = 0; i<sizeof(USER_SETTINGS); i++){
        HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, new_entry_flash_address++, ((unsigned char *)&user_settings)[i]);
    }
    HAL_FLASH_Lock();
}

void flash_erase_eeprom(){

    HAL_FLASH_Unlock();
    FLASH_WaitForLastOperation((uint32_t)FLASH_TIMEOUT_VALUE);
    FLASH_Erase_Sector(EEPROM_SECTOR_ID, (uint8_t) FLASH_VOLTAGE_RANGE_3);
    FLASH_WaitForLastOperation((uint32_t)FLASH_TIMEOUT_VALUE);
    CLEAR_BIT(FLASH->CR, (FLASH_CR_SER | FLASH_CR_SNB));
    HAL_FLASH_Lock();
}


/* Private function -----------------------------------------------------------*/

HAL_StatusTypeDef FLASH_WaitInRAMForLastOperationWithMaxDelay(){
  /* Wait for the FLASH operation to complete by polling on BUSY flag to be reset.
     Even if the FLASH operation fails, the BUSY flag will be reset and an error
     flag will be set */
  while(__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET){}

  /* Check FLASH End of Operation flag  */
  if (__HAL_FLASH_GET_FLAG(FLASH_FLAG_EOP) != RESET){
    /* Clear FLASH End of Operation pending bit */
    __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP);
  }
#if defined(FLASH_SR_RDERR)
  if(__HAL_FLASH_GET_FLAG((FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | \
                           FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR | FLASH_FLAG_RDERR)) != RESET)
#elif defined(FLASH_FLAG_PGSERR)
  if(__HAL_FLASH_GET_FLAG((FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | \
                           FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR)) != RESET)
#else
  if(__HAL_FLASH_GET_FLAG((FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | \
                           FLASH_FLAG_PGPERR )) != RESET)
#endif /* FLASH_SR_RDERR */
  {
    /*Save the error code*/
    return HAL_ERROR;
  }

  /* If there is no error flag set */
  return HAL_OK;
}

int16_t get_active_eeprom_page(){
    int16_t index = 0;
    uint32_t eeprom_pointer = (uint32_t) eeprom_data;
    while ( index <  EEPROM_MAX_PAGE_ID &&  *(__IO uint32_t *) eeprom_pointer == EEPROM_INVALID_PAGE_HEADER ){
        index++;
        eeprom_pointer += EEPROM_PAGE_SIZE;
    }
    //
    if( *(__IO uint32_t *) eeprom_pointer != EEPROM_ACTIVE_PAGE_HEADER )
        index = -1;
    return index;
}

int16_t get_active_eeprom_page_entry(int16_t page_index){
    int16_t index = 0;

	uint32_t dataIndex = (uint32_t) (page_index * EEPROM_PAGE_SIZE) + EEPROM_PAGE_HEADER_SIZE;

	uint32_t eeprom_pointer =  (uint32_t) &eeprom_data[dataIndex];
    while( index <  EEPROM_MAX_ENTRY_ID &&  *(__IO uint16_t *) eeprom_pointer == EEPROM_INVALID_ENTRY_HEADER ){
        index++;
        eeprom_pointer += EEPROM_ENTRY_SIZE;
    }
    if( *(__IO uint16_t *) eeprom_pointer != EEPROM_ACTIVE_ENTRY_HEADER )
        index = -1;
    return index;
}

bool flash_has_downloaded_roms(){
    return false; // _mg_   user_settings.first_free_flash_sector > 5;
}


/* write (firmware) to flash from buffer */
void flash_firmware_update(uint32_t filesize)
{
	// dummy function to keep compiler happy.
	(void) filesize;
}

uint32_t flash_file_request(uint8_t *ext_buffer, uint32_t base_address, uint32_t start, uint32_t length )
{

    uint32_t i = 0;

    base_address += TAR_HEADER_SIZE + start;

    for ( i = 0; i < length; i++) {
        ext_buffer[i] = (*(__IO uint8_t*)(base_address + i));
    }
    return length;
}


// ---  END  ---  cartPlus flash items
#endif		// #ifdef CARTPLUS_ENABLE




