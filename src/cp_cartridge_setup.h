/**
 * @file cp_cartridge_setup.h
 *
 *  Created on: Dec 27, 2023
 *      Author: mgiebler
 *
 * From CartPlus project.
 *
 */

#ifndef CP_CARTRIDGE_SETUP_H_
#define CP_CARTRIDGE_SETUP_H_

#include <global.h>
#include <stdint.h>
#include <stdbool.h>


typedef struct {
    uint8_t* banks[64];
} cartridge_layout;

bool setup_cartridge_image(const char*, uint32_t, uint8_t*, cartridge_layout* , MENU_ENTRY *, enum cart_base_type  );

#endif /* CP_CARTRIDGE_SETUP_H_ */
