/**
 * @file arm_cortex_M4_fault_handler.h
 *
 *  Created on: Apr 22, 2018
 *      @author Mark GIebler
 *
 * This is open source code. Covered by Apache 2.0 license.
 *
 * Licensed under the Apache License, Version 2.0 (the License); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 */

#ifndef INC_ARM_CORTEX_M4_FAULT_HANDLER_H_
#define INC_ARM_CORTEX_M4_FAULT_HANDLER_H_

#include <stdint.h>			/* get the C standard typedefs */
#include "mgDebugHelper.h"	// include my stringify macros

/* ----- BEGIN  Hard fault section ----------------------------------------------------------------- */

/* mbed Microcontroller Library
 * Copyright (c) 2006-2018 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// uncomment 	MBED_FAULT_HANDLER_DISABLED		to disable this fault handling code. Todo: _mg_ disable doesn't disable the fault handler functions.
//#define MBED_FAULT_HANDLER_DISABLED


/// Debug configuration items. Set to 1 to enable, 0 to disable.
#define 	config_DEBUG_BREAK_ON_ASSERT_FAIL	(0)
#define 	config_DEBUG_BREAK_AT_HARDFAULT		(0)
#define		config_ENABLE_HARDFAULT_TEST		(0)


/// Define which interrupt vector table entry will be used to vector the software interrupt to the Assert_Handler().
/// Adjust the vector number SW_INT_VEC_ to an empty or unused isr vector table entry.
/// @warning IMPORTANT!
/// The following assembly language declaration needs to be copied into the startup_stm32xxx.s file
/// at an appropriate isr vector table location.
/// An appropriate location is an unused vector AFTER the sysTick vector.
/// -
///	.word	Assert_Handler			/* _mg_ added to the IRQ position @ref SW_INT_VEC_ */
#define SW_INT_VEC_		85UL	// SW_INT_VEC_ is an unused int vector, see IRQn_Type stm32g474xx.h for which ones are used by the MCU

#define ASSERT_IRQn ((IRQn_Type)SW_INT_VEC_)	//!< Define the vector number as an IRQ number type.

/**
 * @brief
 * Macro to trigger a software interrupt vector to the Assert_Handler()
 */
#define EXEC_SW_INT_VEC() *(uint32_t*)( ((0xE000E000UL) +  0x0100UL + 0xE00UL) )=SW_INT_VEC_;/*NVIC->STIR = SW_VEC_;*/


/// Defines how deep to unwind the stack for function call trace
#define ARM_FAULT_STACK_SAVE_SIZE	20

/// Fault context structure.
///
/// ATTENTION: DO NOT CHANGE THIS STRUCT WITHOUT MAKING CORRESPONDING CHANGES in
///	- asm function Fault_Handler() in exception.s file
///	- function print_context_info().
///
/// Offsets to these registers are used by the fault handler in exception.s
typedef struct {
volatile uint32_t arm_fault_type;
volatile uint32_t R0;
volatile uint32_t R1;
volatile uint32_t R2;
volatile uint32_t R3;
volatile uint32_t R4;
volatile uint32_t R5;
volatile uint32_t R6;
volatile uint32_t R7;
volatile uint32_t R8;
volatile uint32_t R9;
volatile uint32_t R10;
volatile uint32_t R11;
volatile uint32_t R12;
volatile uint32_t SP;
volatile uint32_t LR;
volatile uint32_t PC;	/* Program counter - fault address or hopefully close to it. can be off if ARM in imprecise mode  */
volatile uint32_t xPSR;	/* Program status register. */
volatile uint32_t PSP;
volatile uint32_t MSP;
volatile uint32_t EXC_RETURN;
volatile uint32_t CONTROL;
// _mg_  capturing more registers: http://www.chibios.com/forum/viewtopic.php?t=743
volatile uint32_t _SHCSR;
volatile uint32_t _CFSR ;
volatile uint32_t _HFSR; /* see http://infocenter.arm.com/help/topic/com.arm.doc.dui0553a/Cihdjcfc.html */
volatile uint32_t _DFSR ;
volatile uint32_t _MMFAR ;
volatile uint32_t _BFAR ;
volatile uint32_t _AFSR ;	// On M7: Auxiliary BusFault Status Register

// _mg_  capture more data about fault
volatile uint32_t caller_LR;	// address of the calling routine - for Assert code.
volatile uint32_t stack[ARM_FAULT_STACK_SAVE_SIZE];		// save called functions list.

//  Do not add any new parameters above this line in order to maintain backward compatibility between bootloader and upper memory code.
volatile uint32_t reserved[6];	// extra space for any future data.

} arm_fault_context_t;

extern volatile arm_fault_context_t arm_fault_context;
//extern volatile uint32_t arm_fault_type;
#define arm_fault_type arm_fault_context.arm_fault_type

/** @defgroup Fault Exception Code Constants
  * @{
  */

/// @name Fault type definitions
/// ATTENTION: DO NOT CHANGE THESE VALUES WITHOUT MAKING CORRESPONDING CHANGES in exception.s.
/// @{
#define HARD_FAULT_EXCEPTION       (0xDEAD0010) //!< Keep some gap between values for any future insertion/expansion
#define MEMMANAGE_FAULT_EXCEPTION  (0xDEAD0020)
#define BUS_FAULT_EXCEPTION        (0xDEAD0030)
#define USAGE_FAULT_EXCEPTION      (0xDEAD0040)
#define FAULT_TYPE_ASSERT_FAULT	   (0xDEAD0050)
/// @}
/**
 * @}
 */

//This is a handler function called from Fault handler to print the error information out.
//This runs in fault context and uses special functions(defined in mbed_rtx_fault_handler.c) to print the information without using C-lib support.
//void arm_fault_handler (uint32_t fault_type, void *arm_fault_context_in, void *osRtxInfoIn);

/** _mg_ for testing my enhancements for debugging hard faults */
uint32_t hardFault_tester(void);


#ifndef MEMV32u
/// macro to read 32 bit value from memory location adr
#define MEMV32u(adr) (*(uint32_t volatile * volatile)(adr))
#endif

#ifndef READ_REGISTER
/// macro to return a register into a variable.
/// reads the register reg to the variable var
#define READ_REGISTER(var,reg) __asm volatile( "mov %[result], " G_STRINGIFY(reg) "\n\t" : [result] "=r" (var) )
#endif

/**
\brief Get Link Register (return address of calling function)
\details Returns the current value of the Link Register (LR).
\return LR Register value
*/
__attribute__( ( always_inline ) ) static inline uint32_t __get_LR(void)
{
	register uint32_t result;
	READ_REGISTER(result, LR);
	return(result);
}

void _Assert_Catcher( const char * func, int line, uint32_t callerLR, void * data1);

#if(0)	// leave #if(0) as-is.  See below.

//  The configASERT() macros in FreeRTOSConfig.h needs to be replaced with the following replacements: (copy and paste them into the file)

/* -----  BEGIN Copy from arm_cortex_M4_fault_handler.h ------------ */
/* _mg_ redefine  #define configASSERT( x ) if ((x) == 0) {taskDISABLE_INTERRUPTS(); for( ;; );} */
/* _mg_ call function to catch the assert in the debugger. in arm_cortex_M4_fault_handler.c and then trigger a software IRQ to vector SW_INT_VEC_ */
#include "arm_cortex_M4_fault_handler.h"	// for _Assert_Catcher and __get_LR()
#define configASSERT( x ) if ((x) == 0) {taskDISABLE_INTERRUPTS();  _Assert_Catcher(__func__, __LINE__, __get_LR(),0); EXEC_SW_INT_VEC();/*NVIC->STIR = SW_VEC_;*/ taskENABLE_INTERRUPTS();}
#define configASSERT_D1( x, data1 ) if ((x) == 0) {taskDISABLE_INTERRUPTS();  _Assert_Catcher(__func__, __LINE__, __get_LR(),(void *)data1); EXEC_SW_INT_VEC();/*NVIC->STIR = SW_VEC_;*/ taskENABLE_INTERRUPTS();}
/* -----  END Copy  from arm_cortex_M4_fault_handler.h ------------- */

#endif

// --------------------------------------------------------------------
//
// arm fault handler data dumper helper routines.
//
// --------------------------------------------------------------------
void dump_arm_fault( void(*serial_putchar)(char) );
void fault_print_init(void);
void fault_print_str( void (*serial_putchar)(char), char *fmtstr, uint32_t *values );
void hex_to_str(uint32_t value, char *hex_star);
void print_context_info(void (*serial_putchar)(char));

/* ----- END  Hard fault section ----------------------------------------------------------------- */

#endif /* INC_ARM_CORTEX_M4_FAULT_HANDLER_H_ */
