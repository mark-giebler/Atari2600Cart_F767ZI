#ifndef FLASH_H
#define FLASH_H

#include <stdint.h>
#include <stdbool.h>

typedef struct {
    uint8_t* base;
    uint32_t next_write_target;
} flash_context;

uint32_t flash_size_bytes();
uint32_t available_flash();

bool prepare_flash(uint32_t size, flash_context *context);

bool write_flash(uint32_t byte_count, uint8_t* buffer, flash_context *context);

#ifdef STM32F767xx
// map legacy ST library functions to newer HAL names
#define FLASH_Lock()		HAL_FLASH_Lock()
#define FLASH_Unlock()		HAL_FLASH_Unlock()
#define UNIQUE_DEVICE_ID_ADDRESS    UID_BASE	// 0x1FF0F420 /* STM32F7xx unique device ID address */
#define STM32F7_FLASH_SIZE  (*(__IO uint16_t *) FLASHSIZE_BASE)	// 0x1FF0F442UL
#else
#define FLASH_SIZE_ADDRESS          0x1FFF7A22 /* STM32F4xx flash size address */
#define UNIQUE_DEVICE_ID_ADDRESS    0x1FFF7A10 /* STM32F4xx unique device ID address */
#define STM32F4_FLASH_SIZE  (*(__IO uint16_t *) (FLASH_SIZE_ADDRESS))
#endif


/**
 * The STM32 factory-programmed UDID memory.
 * Three values of 32 bits each starting at this address
 * Use like this: STM32_UDID[0], STM32_UDID[1], STM32_UDID[2]
 */
#define STM32_UDID          ((__IO uint32_t *) UNIQUE_DEVICE_ID_ADDRESS )



// --- BEGIN ---  cartPlus flash items -----

#include "global.h"

void flash_firmware_update(uint32_t)__attribute__((section(".data#")));

uint32_t flash_download(char *, uint32_t , uint32_t , bool );

uint32_t flash_file_request( uint8_t *, uint32_t, uint32_t, uint32_t );

bool flash_has_downloaded_roms(void);

void flash_file_list(char *, MENU_ENTRY **, int *);
uint32_t flash_check_offline_roms_size(void);

void flash_erase_storage(uint8_t);

USER_SETTINGS flash_get_eeprom_user_settings(void);
void flash_set_eeprom_user_settings(USER_SETTINGS);
void flash_erase_eeprom();
void flash_firmware_update(uint32_t filesize);
uint32_t flash_file_request(uint8_t *ext_buffer, uint32_t base_address, uint32_t start, uint32_t length );

// ---  END  ---  cartPlus flash items



#endif // FLASH_H
