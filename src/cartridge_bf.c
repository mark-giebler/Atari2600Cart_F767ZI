#include <stdbool.h>

#include "fatfs.h"

#include "cartridge_bf.h"
#include "flash.h"
#include "cartridge_firmware.h"
#include "cartridge_firmware_plus.h"
#include "ram_storage.h"


// get number of 4K banks.
#define RAM_4K_BANKS		((BUFFER_SIZE_KB / 4) - 1)
#define CCM_4K_BANKS		(CCM_SIZE_KB / 4)
#define FLASH_4K_BANKS		(64 - RAM_4K_BANKS - CCM_4K_BANKS)

#define AVAILABLE_RAM_BASE (RAM_4K_BANKS * 4096)


#define CCM_IMAGE_OFFSET (RAM_4K_BANKS * 4096)

#define FLASH_IMAGE_SIZE (FLASH_4K_BANKS * 4096)
#define FLASH_IMAGE_OFFSET ((RAM_4K_BANKS + CCM_4K_BANKS) * 4096)

#define STARTUP_BANK_BF 1
#define STARTUP_BANK_BFSC 15

typedef struct {
    uint8_t* banks[64];
} cartridge_layout;

/**
 * FIXME: _mg_ This emulator always loads the image into flash. Why??
 */
static bool setup_cartridge_image(const char* filename, uint32_t image_size, uint8_t* buffer, cartridge_layout* layout) {
    if (image_size != 256*1024) return false;

    if (FLASH_IMAGE_SIZE > available_flash()) return false;

    flash_context ctx;
    if (!prepare_flash(FLASH_IMAGE_SIZE, &ctx)) return false;

    FATFS fs;
	FIL fil;
	UINT bytes_read;

    if (f_mount(&fs, "", 1) != FR_OK) goto fail_unmount;
	if (f_open(&fil, filename, FA_READ) != FR_OK) goto fail_close;

    if (f_lseek(&fil, FLASH_IMAGE_OFFSET) != FR_OK) goto fail_close;
    uint32_t bytes_written_to_flash = 0;

    while (bytes_written_to_flash < FLASH_IMAGE_SIZE) {
        if (f_read(&fil, CCM_RAM, CCM_SIZE, &bytes_read) != FR_OK) goto fail_close;

        bytes_written_to_flash += bytes_read;
        if (bytes_read < CCM_SIZE && bytes_written_to_flash < FLASH_IMAGE_SIZE) goto fail_close;

        if (!write_flash(bytes_read, CCM_RAM, &ctx)) goto fail_close;
    }

    if (f_lseek(&fil, CCM_IMAGE_OFFSET) != FR_OK) goto fail_close;
    if (f_read(&fil, CCM_RAM, CCM_SIZE, &bytes_read) != FR_OK) goto fail_close;
    if (bytes_read != CCM_SIZE) goto fail_close;

    for (uint8_t i = 0; i < RAM_4K_BANKS; i++) layout->banks[i] = buffer + i * 4096;
    for (uint8_t i = 0; i < CCM_4K_BANKS; i++) layout->banks[RAM_4K_BANKS + i] = CCM_RAM + i * 4096;
    for (uint8_t i = 0; i < FLASH_4K_BANKS; i++) layout->banks[RAM_4K_BANKS + CCM_4K_BANKS + i] = ctx.base + i * 4096;

    f_close(&fil);
	f_mount(0, "", 1);
	return true;

	fail_close:
		f_close(&fil);
	fail_unmount:
		f_mount(0, "", 1);

	return false;
}

void emulate_bfsc_cartridge(const char* filename, uint32_t image_size, uint8_t* buffer) {
    uint8_t *ram_base = buffer + AVAILABLE_RAM_BASE;

    cartridge_layout* layout = (void*)ram_base;
    ram_base += sizeof(cartridge_layout);

    uint8_t* ram = ram_base;

    if (!setup_cartridge_image(filename, image_size, buffer, layout)) return;

    uint8_t *bank = layout->banks[STARTUP_BANK_BFSC];

    if (!reboot_into_cartridge()) return;
    __disable_irq();

    uint16_t addr, addr_prev = 0, addr_prev2 = 0, data = 0, data_prev = 0;

	while (1)
	{
		while (((addr = ADDR_IN) != addr_prev) || (addr != addr_prev2))
		{
			addr_prev2 = addr_prev;
			addr_prev = addr;
		}

        if (!(addr & 0x1000)) continue;

        uint16_t address = addr & 0x0fff;

        if (address < 0x80) {
            while (ADDR_IN == addr) { data_prev = data; data = DATA_IN; }
			data = DATA_IN_SHIFT(data_prev);

            ram[address] = data;
        } else {
            if (address >= 0x0f80 && address <= 0x0fbf) bank = layout->banks[address - 0x0f80];

            data = (address < 0x0100) ? ram[address & 0x7f] : bank[address];

            DATA_OUT = DATA_OUT_SHIFT(((uint16_t)data));
			SET_DATA_MODE_OUT
			// wait for address bus to change
			while (ADDR_IN == addr) ;
			SET_DATA_MODE_IN
        }
    }
}

void emulate_bf_cartridge(const char* filename, uint32_t image_size, uint8_t* buffer) {
    uint8_t *ram_base = buffer + AVAILABLE_RAM_BASE;
    cartridge_layout* layout = (void*)ram_base;

    if (!setup_cartridge_image(filename, image_size, buffer, layout)) return;

    uint8_t *bank = layout->banks[STARTUP_BANK_BF];

    if (!reboot_into_cartridge()) return;
    __disable_irq();

    uint16_t addr, addr_prev = 0, addr_prev2 = 0;

	while (1)
	{
		while (((addr = ADDR_IN) != addr_prev) || (addr != addr_prev2))
		{
			addr_prev2 = addr_prev;
			addr_prev = addr;
		}

        if (!(addr & 0x1000)) continue;

        uint16_t address = addr & 0x0fff;

        if (address >= 0x0f80 && address <= 0x0fbf) bank = layout->banks[address - 0x0f80];

        DATA_OUT = DATA_OUT_SHIFT(((uint16_t)bank[address]));
        SET_DATA_MODE_OUT
        // wait for address bus to change
        while (ADDR_IN == addr) ;
        SET_DATA_MODE_IN

    }
}
