/**
 * @file uart.h
 *
 *  Created on: Dec 5, 2023
 *  @author mgiebler
 */

#ifndef INC_UART_H_
#define INC_UART_H_

#include "stm32f7xx_hal.h"

#include <stdio.h>

// These next set of macros can be overridden by declaring them in main.h
// and including main.h before this header file.
#ifndef ENABLE_STDIO_SUPPORT
/** Set to 1 to enable support for stdio (printf(),scanf(), etc) via UART. */
#define ENABLE_STDIO_SUPPORT	0
#endif


#if (ENABLE_STDIO_SUPPORT == 1)
// set to1  to enable stdio output tests.
#define ENABLE_STDIO_OUT_TEST	1

// set to 1 to enable stdio input tests.
#define ENABLE_STDIO_IN_TEST	1

#endif	// ENABLE_STDIO_SUPPORT


/* ******************* Public prototypes *******************************/
void setupDirectIO(UART_HandleTypeDef *huart);
void sendUARTTest(void);

void sendByte(uint8_t c);
void sendString(const char* str);
void sendStringN(const char* str, uint32_t len);

void stdioTarget(UART_HandleTypeDef *huart);
int stdioReady(void);
void stdioNoBuffers();
void stdioTestOutput(void);
void stdioTestInput(void);

void _myprintf(const char *fmt, ...);
// stdio / myprint macros
#if ENABLE_STDIO_SUPPORT
#define myprintf	printf
#else
#define myprintf	_myprintf
#endif

#endif /* INC_UART_H_ */
