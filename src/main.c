/* ----------------------------------------------------------
 * UnoCart2600 Firmware (c)2018 Robin Edwards (ElectroTrains)
 * ----------------------------------------------------------
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Version History
 * ---------------
 * v1.01 5/2/18 Fixed menu navigation bug after selecting a bad rom file
 * v1.02 8/2/18 Added partial DPC support - Pitfall II works
 * v1.03 19/3/18 Supercharger support thanks to Christian Speckner
 * v1.04 21/3/18 Bug fixes (file extension->cart type)
 * v1.05 29/3/18 Firmware compatible with the 7800 bios
 * v1.18 22/2/22 Add stm32f767zi MCU support. - Mark Giebler.
 * 		Decrease STM32F767ZI start-up time by reducing CopyDataInit time and 0-filling time:
 * 			- Moved some fixed structures to .rodata section to decrease size and copy time of .data section. Search for "_mg_ const added"
 * 			- Moved some variable to .noinit section to reduce 0-filling time.  Search for __attribute__ ((section (".noinit")))
 * v1.19 22/4/13 - Mark Giebler. Start merge in of cartPlus UnoCart project with longer filename support in the 6502 code. (incomplete)
 * v1.20 23/12/11 - Mark Giebler. Merge in my more general purpose fs_utils
 */

#define _GNU_SOURCE

#include <defines.h>
#if defined (STM32F4XX)
#include "main.h"
#include "stm32f4xx.h"

#include "tm_stm32f4_fatfs.h"
#include "tm_stm32f4_delay.h"

#elif defined (STM32F767xx)
#include "main.h"
#include "stm32f7xx.h"
#include "stm32f7xx_hal.h"
#include <stdio.h>
#include <string.h>
#include <stdarg.h> // for va_list var arg functions
#include "fatfs.h"

#ifdef CARTPLUS_ENABLE
#include <global.h>
#endif

void SystemClock_Config(void);

SPI_HandleTypeDef hspi2;
void MX_SPI2_Init(void);

// define		USE_MONOLITHIC_FS_UTILS		to use old non-modular code in main instead of fs_utils.c
//#define 		USE_MONOLITHIC_FS_UTILS

#ifndef USE_MONOLITHIC_FS_UTILS
#define read_directory 		fs_read_directory
#define get_filename_ext 	fs_get_filename_ext
#define test_sd()			test_fs(1)
#endif

// define		TEST_IO_LINES	to enable Address and data bus line tests
//#define 		TEST_IO_LINES

// define		TEST_SD_CARD	to enable SD Card read/write test at start up
//#define 		TEST_SD_CARD	// Todo: _mg_ make this a runtime switch option via DIP switch.

#ifdef TEST_SD_CARD
#ifndef ENABLE_UART
#define ENABLE_UART
#endif
#undef G_DEBUG_PRINT
#define G_DEBUG_PRINT(code)	G_DEBUG(code)
#endif

// define	 	TRACE_SD		to enable runtime trace of SD dir reading via UART
//#define 		TRACE_SD

#ifdef TRACE_SD
#ifndef ENABLE_UART
#define ENABLE_UART
#endif
#undef G_DEBUG_PRINT
#define G_DEBUG_PRINT(code)	G_DEBUG(code)
#endif

// define		TEST_CART_CMD	to enable serial port debug messages during cart cmd processing. Issues with ST-Link dropping USB connection when powering on 2600.
//#define 		TEST_CART_CMD

// define		POR_FORCE_NOP	to enable forcing NOP on cartridge databus at STM32 POR.  For Experimental work in start up timing. Timing Issue was i-cache not enabled.
//#define 		POR_FORCE_NOP


// define		ENABLE_UART		to enable uart and printf()
#define 		ENABLE_UART
#ifdef ENABLE_UART
#include "uart.h"
// FIXME: _mg_ remove the tinyPrintf code elsewhere in this project.... ???  Or use it instead of vsnprintf() ???
UART_HandleTypeDef huart3;	// for st-link and myprintf()
void MX_USART3_UART_Init(void);
void MX_USART3_DeInit(void);
#endif

// See main.h for other special build macros

#endif	// #elif defined (STM32F767xx)


#ifndef USE_MONOLITHIC_FS_UTILS
#include "fs_utils.h"
#endif

#include <ctype.h>

#include "cartridge_io.h"
#include "cartridge_firmware.h"
#include "cartridge_firmware_plus.h"
#include "cartridge_supercharger.h"
#include "cartridge_dpc.h"
#include "cartridge_3f.h"
#include "cartridge_3e.h"
#include "cartridge_3ep.h"
#include "cartridge_bf.h"
#include "cartridge_df.h"
#include "cartridge_ace.h"
#include "cartridge_pp.h"

#include "ram_storage.h"

#if defined (STM32F767xx)

#define VERSION "M.GIEBLER 21"

#else
#ifdef BUS_DUMPER
#define VERSION "BUSDUMPER 17"
#else
#define VERSION "R.EDWARDS 17"
#endif
#endif	// #if defined (STM32F767xx)

/*************************************************************************
 * Cartridge Definitions
 *************************************************************************/
#ifndef CARTPLUS_ENABLE
__attribute__ ((section (".noinit"))) uint8_t buffer[BUFFER_SIZE_KB * 1024];	//_mg_ put this in .noinit section
#endif

__attribute__ ((section (".noinit"))) char cartridge_image_path[256];	// _mg_ put this in .noinit section.
unsigned int cart_size_bytes;
__attribute__ ((section (".noinit"))) int tv_mode; // _mg_ put this in .noinit section.

#ifndef CARTPLUS_ENABLE
#define CART_TYPE_NONE	0
#define CART_TYPE_2K	1
#define CART_TYPE_4K	2
#define CART_TYPE_F8	3	// 8k
#define CART_TYPE_F6	4	// 16k
#define CART_TYPE_F4	5	// 32k
#define CART_TYPE_F8SC	6	// 8k+ram
#define CART_TYPE_F6SC	7	// 16k+ram
#define CART_TYPE_F4SC	8	// 32k+ram
#define CART_TYPE_FE	9	// 8k
#define CART_TYPE_3F	10	// varies (examples 8k)
#define CART_TYPE_3E	11	// varies (only example 32k)
#define CART_TYPE_3EX   12
#define CART_TYPE_E0	13	// 8k
#define CART_TYPE_0840	14	// 8k
#define CART_TYPE_CV	15	// 2k+ram
#define CART_TYPE_EF	16	// 64k
#define CART_TYPE_EFSC	17	// 64k+ram
#define CART_TYPE_F0	18	// 64k
#define CART_TYPE_FA	19	// 12k
#define CART_TYPE_E7	20	// 16k+ram
#define CART_TYPE_DPC	21	// 8k+DPC(2k)
#define CART_TYPE_AR	22  // Arcadia Supercharger (variable size)
#define CART_TYPE_BF	23  // BF
#define CART_TYPE_BFSC	24  // BFSC
#define CART_TYPE_ACE	25  // ARM Custom Executable
#define CART_TYPE_PP    26  // Pink Panther Prototype
#define CART_TYPE_DF    27  // DF
#define CART_TYPE_DFSC  28  // DFSC
#define CART_TYPE_3EP	29	// 3E+ 1-64K + 32K ram
#define CART_TYPE_4KSC  30  // 4k+SC

const typedef struct {	// _mg_ const added so as  to not copy cart type map to RAM at STM32 start-up.
	const char *ext;
	int cart_type;
} EXT_TO_CART_TYPE_MAP;

EXT_TO_CART_TYPE_MAP ext_to_cart_type_map[] = {
	{"ROM", CART_TYPE_NONE},
	{"BIN", CART_TYPE_NONE},
	{"A26", CART_TYPE_NONE},
	{"2K", CART_TYPE_2K},
	{"4K", CART_TYPE_4K},
	{"F8", CART_TYPE_F8},
	{"F6", CART_TYPE_F6},
	{"F4", CART_TYPE_F4},
	{"F8S", CART_TYPE_F8SC},
	{"F6S", CART_TYPE_F6SC},
	{"F4S", CART_TYPE_F4SC},
	{"FE", CART_TYPE_FE},
	{"3F", CART_TYPE_3F},
	{"3E", CART_TYPE_3E},
	{"3EX", CART_TYPE_3E},
	{"3EP", CART_TYPE_3EP},
	{"E0", CART_TYPE_E0},
	{"084", CART_TYPE_0840},
	{"CV", CART_TYPE_CV},
	{"EF", CART_TYPE_EF},
	{"EFS", CART_TYPE_EFSC},
	{"F0", CART_TYPE_F0},
	{"FA", CART_TYPE_FA},
	{"E7", CART_TYPE_E7},
	{"DPC", CART_TYPE_DPC},
	{"AR", CART_TYPE_AR},
	{"BF", CART_TYPE_BF},
	{"BFS", CART_TYPE_BFSC},
	{"ACE", CART_TYPE_ACE},
	{"WD", CART_TYPE_PP},
	{"DF", CART_TYPE_DF},
	{"DFS", CART_TYPE_DFSC},
	{"4KSC", CART_TYPE_4KSC},
	{0,0}
};
#endif	// CARTPLUS_ENABLE

/*************************************************************************
 * Cartridge Type Detection _mg_ moved to cartridge_detection.c
 *************************************************************************/
#include "cartridge_detection.h"


/*************************************************************************
 * File/Directory Handling
 *************************************************************************/

int num_dir_entries = 0; // how many entries in the current directory

#ifndef USE_MONOLITHIC_FS_UTILS
__attribute__ ((section (".noinit"))) DIR_ENTRY *dir_entries;	// _mg_ put this in .noinit section.
#endif


//#define NUM_DIR_ITEMS	200 //80	// Todo: _mg_ make this larger 256? Or a link list of structures dynamically allocated from heap?
							// could go to 85 and still fit in menu_ram[] array. See cartridge_firmware.c and readDirectoryForAtari()

#ifdef USE_MONOLITHIC_FS_UTILS

// _mg_ brought in long filename support from CubeMx FatFs
// define for long file name
#define FATFS_LONG_NAME_ENABLE
#ifdef  FATFS_LONG_NAME_ENABLE
#define THE_FILENAME long_filename
#else
#define THE_FILENAME filename
#endif

/**
 * @brief Define a fs entity structure.
 *
 *
 */
typedef struct {
	char isDir;			//!< 1 if entity is a directory, 0 if a file.
	char filename[13];	//!< Abbreviated name of the entity.
	char long_filename[LONG_FILENAME_LEN+1];	//!< long name of entity. Todo: _mg_ make this a linked list of variable length strings? Dynamically allocate?
} DIR_ENTRY;

__attribute__ ((section (".noinit"))) DIR_ENTRY dir_entries[NUM_DIR_ITEMS];	// _mg_ put this in .noinit section.


/**
 * @brief Compare two file system entities
 *
 * @param p1
 * @param p2
 * @return	return 0 if equal, non-zero if different.
 */
int entry_compare(const void* p1, const void* p2)
{
	DIR_ENTRY* e1 = (DIR_ENTRY*)p1;
	DIR_ENTRY* e2 = (DIR_ENTRY*)p2;
	if (e1->isDir && !e2->isDir) return -1;
	else if (!e1->isDir && e2->isDir) return 1;
	else return strcasecmp(e1->long_filename, e2->long_filename);
}

/**
 * @brief return a pointer to the first extension found in a filename
 *
 * @param filename
 * @return pointer to start of the extension string, or 0 if no extension found.
 */
char *get_filename_ext(char *filename) {
	char *dot = strrchr(filename, '.');
	if(!dot || dot == filename) return "";
	return dot + 1;
}


// single FILINFO structure
__attribute__ ((section (".noinit"))) FILINFO f_info; // _mg_ put this in .noinit section
__attribute__ ((section (".noinit"))) char lfn[_MAX_LFN + 1];   /* Buffer to store the LFN */ // _mg_ put this in .noinit section

#endif	// USE_MONOLITHIC_FS_UTILS

#if !defined( CARTPLUS_ENABLE)
/**
 * @brief Check if file has an extension that is of a known cartridge type.
 *
 * @param filename
 * @return 1 if known, 0 if not known
 */
// Todo: _mg_ Note that cartPlus code always returns true (1) for this function. Important?
static int is_valid_file(char *filename) {
	char *ext = get_filename_ext(filename);
	EXT_TO_CART_TYPE_MAP *p = ext_to_cart_type_map;
	while (p->ext) {
		if (strcasecmp(ext, p->ext) == 0)
			return 1;
		p++;
	}
	return 0;
}
#endif	// CARTPLUS_ENABLE

#ifdef USE_MONOLITHIC_FS_UTILS
/*
 * return 1 if OK, 0 if not.
 */
int read_directory(char *path) {
	int ret = 0;
	num_dir_entries = 0;
	DIR_ENTRY *dst = (DIR_ENTRY *)&dir_entries[0];
	G_DEBUG_PRINT(myprintf("__read_dir: %s\n",path);)
	FATFS FatFs;
	HAL_Delay(100);
	if (f_mount(&FatFs, "", 1) == FR_OK) {
		DIR dir;
		if (f_opendir(&dir, path) == FR_OK) {
			if (strlen(path))
			{	// not root directory, add pseudo ".." to go up a dir
				dst->isDir = 1;
				strcpy(dst->long_filename, "(GO BACK)");
				strcpy(dst->filename, "..");
				num_dir_entries++;
				dst++;
			}
			while (num_dir_entries < NUM_DIR_ITEMS) {
				if (f_readdir(&dir, &f_info) != FR_OK || f_info.fname[0] == 0)
					break;
				if (f_info.fattrib & (AM_HID | AM_SYS))
					continue;
				dst->isDir = f_info.fattrib & AM_DIR ? 1 : 0;
#ifdef FATFS_LONG_NAME_ENABLE
				if (!dst->isDir)
				{
					if (!is_valid_file(f_info.fname[0] ? f_info.fname : f_info.altname))
					{
						G_DEBUG_PRINT(myprintf(" Skip: %s\n",f_info.fname);)
						continue;
					}
				}else
				{
					G_DEBUG_PRINT(myprintf(" Dir:  ");)
				}
#else
				if (!dst->isDir)
				{
					if (!is_valid_file(f_info.fname))
					{
						G_DEBUG_PRINT(myprintf(" Skip: %s\n",f_info.fname);)
						continue;
					}
					G_DEBUG_PRINT(myprintf(" File: ");)
				}else
				{
					G_DEBUG_PRINT(myprintf(" Dir:  ");)
				}
#endif
				// copy file record
				strncpy(dst->filename, f_info.fname,12);
#ifdef FATFS_LONG_NAME_ENABLE
				if (f_info.fname[0]) {
					strncpy(dst->long_filename, f_info.fname, LONG_FILENAME_LEN);
					dst->long_filename[LONG_FILENAME_LEN] = 0;
				}
				else
					strcpy(dst->long_filename, f_info.altname);
#else
					strcpy(dst->long_filename, f_info.fname);
#endif
					G_DEBUG_PRINT(myprintf("%s\n",dst->long_filename);)
				dst++;
				num_dir_entries++;
			}
			f_closedir(&dir);
			qsort((DIR_ENTRY *)&dir_entries[0], num_dir_entries, sizeof(DIR_ENTRY), entry_compare);
			ret = 1;
		}
		f_mount(0, "", 1);
	}
	return ret;
}
#endif	// USE_MONOLITHIC_FS_UTILS

#ifndef CARTPLUS_ENABLE
int identify_cartridge(char *filename, char* long_filename)
{
	FATFS FatFs;
	unsigned int image_size;
	int cart_type = CART_TYPE_NONE;
	FIL fil;

	if (f_mount(&FatFs, "", 1) != FR_OK) return CART_TYPE_NONE;
	if (f_open(&fil, filename, FA_READ) != FR_OK) goto unmount;

	// select type by file extension?
	char *ext = get_filename_ext(long_filename);
	EXT_TO_CART_TYPE_MAP *p = ext_to_cart_type_map;
	while (p->ext) {
		if (strcasecmp(ext, p->ext) == 0) {
			cart_type = p->cart_type;
			break;
		}
		p++;
	}

	image_size = f_size(&fil);

	// Supercharger cartridges get special treatment, since we don't load the entire
	// file into the buffer here
	if (cart_type == CART_TYPE_NONE && (image_size % 8448) == 0)
		cart_type = CART_TYPE_AR;
	if (cart_type == CART_TYPE_AR) goto close;

	// otherwise, read the file into the cartridge buffer
	unsigned int bytes_to_read = image_size > (BUFFER_SIZE_KB * 1024) ? (BUFFER_SIZE_KB * 1024) : image_size;
	UINT bytes_read;
	FRESULT read_result = f_read(&fil, buffer, bytes_to_read, &bytes_read);

	if (read_result != FR_OK || bytes_to_read != bytes_read) {
		cart_type = CART_TYPE_NONE;
		goto close;
	}

	uint8_t tail[16];
	if (f_lseek(&fil, image_size - 16) != FR_OK) {
		cart_type = CART_TYPE_NONE;
		goto close;
	}

	UINT bytes_read_tail;
	read_result = f_read(&fil, tail, 16, &bytes_read_tail);
	if (read_result != FR_OK || 16 != bytes_read_tail) {
		cart_type = CART_TYPE_NONE;
		goto close;
	}

	if (cart_type != CART_TYPE_NONE) goto close;

	// If we don't already know the type (from the file extension), then we
	// auto-detect the cart type - largely follows code in Stella's CartDetector.cpp

	if (is_ace_cartridge(bytes_read, buffer))
	{
		cart_type = CART_TYPE_ACE;
	}
	else if (image_size <= 64 * 1024 && (image_size % 1024) == 0 && isProbably3EPlus(image_size, buffer))
	{
		cart_type = CART_TYPE_3EP;
	}
	else if (image_size == 2*1024)
	{
		if (isProbablyCV(bytes_read, buffer))
			cart_type = CART_TYPE_CV;
		else
			cart_type = CART_TYPE_2K;
	}
	else if (image_size == 4*1024)
	{
		cart_type = isProbably4KSC(buffer) ? CART_TYPE_4KSC : CART_TYPE_4K;
	}
	else if (image_size == 8*1024)
	{
		// First check for *potential* F8
		unsigned char  signature[] = { 0x8D, 0xF9, 0x1F };  // STA $1FF9
		int f8 = searchForBytes(buffer, bytes_read, signature, 3, 2);

		if (isProbablySC(bytes_read, buffer))
			cart_type = CART_TYPE_F8SC;
		else if (memcmp(buffer, buffer + 4096, 4096) == 0)
			cart_type = CART_TYPE_4K;
		else if (isProbablyE0(bytes_read, buffer))
			cart_type = CART_TYPE_E0;
		else if (isProbably3E(bytes_read, buffer))
			cart_type = CART_TYPE_3E;
		else if (isProbably3F(bytes_read, buffer))
			cart_type = CART_TYPE_3F;
		else if (isProbablyFE(bytes_read, buffer) && !f8)
			cart_type = CART_TYPE_FE;
		else if (isProbably0840(bytes_read, buffer))
			cart_type = CART_TYPE_0840;
		else {
			cart_type = CART_TYPE_F8;
		}
	}
	else if (image_size == 8*1024 + 3) {
		cart_type = CART_TYPE_PP;
	}
	else if(image_size >= 10240 && image_size <= 10496)
	{  // ~10K - Pitfall II
		cart_type = CART_TYPE_DPC;
	}
	else if (image_size == 12*1024)
	{
		cart_type = CART_TYPE_FA;
	}
	else if (image_size == 16*1024)
	{
		if (isProbablySC(bytes_read, buffer))
			cart_type = CART_TYPE_F6SC;
		else if (isProbablyE7(bytes_read, buffer))
			cart_type = CART_TYPE_E7;
		else if (isProbably3E(bytes_read, buffer))
			cart_type = CART_TYPE_3E;
		else
			cart_type = CART_TYPE_F6;
	}
	else if (image_size == 32*1024)
	{
		if (isProbablySC(bytes_read, buffer))
			cart_type = CART_TYPE_F4SC;
		else if (isProbably3E(bytes_read, buffer))
			cart_type = CART_TYPE_3E;
		else if (isProbably3F(bytes_read, buffer))
			cart_type = CART_TYPE_3F;
		else
			cart_type = CART_TYPE_F4;
	}
	else if (image_size == 64*1024)
	{
		if (isProbably3E(bytes_read, buffer))
			cart_type = CART_TYPE_3E;
		else if (isProbably3F(bytes_read, buffer))
			cart_type = CART_TYPE_3F;
		else if (isProbablyEF(bytes_read, buffer))
		{
			if (isProbablySC(bytes_read, buffer))
				cart_type = CART_TYPE_EFSC;
			else
				cart_type = CART_TYPE_EF;
		}
		else
			cart_type = CART_TYPE_F0;
	}
	else if (image_size == 128 * 1024) {
		if (isProbablyDF(tail))
			cart_type = CART_TYPE_DF;
		else if (isProbablyDFSC(tail))
			cart_type = CART_TYPE_DFSC;
	}
	else if (image_size == 256 * 1024)
	{
		if (isProbablyBF(tail))
			cart_type = CART_TYPE_BF;
		else if (isProbablyBFSC(tail))
			cart_type = CART_TYPE_BFSC;
	}

	close:
		f_close(&fil);

	unmount:
		f_mount(0, "", 1);

	if (cart_type)
		cart_size_bytes = image_size;

	return cart_type;
}
#endif	// CARTPLUS_ENABLE

/*************************************************************************
 * MCU Initialisation
 *************************************************************************/

GPIO_InitTypeDef  GPIO_InitStructure={0};
#if defined (STM32F4XX)
/* Input/Output data GPIO pins on PE{8..15} */
void config_gpio_data(void) {
	/* GPIOE Periph clock enable */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);

	/* Configure GPIO Settings */
	GPIO_InitStructure.GPIO_Pin =
		GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 |
		GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_25MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOE, &GPIO_InitStructure);
}

/* Input Address GPIO pins on PD{0..15} */
void config_gpio_addr(void) {
	/* GPIOD Periph clock enable */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	/* Configure GPIO Settings */
	GPIO_InitStructure.GPIO_Pin =
		GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 |
		GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7 |
		GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 |
		GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOD, &GPIO_InitStructure);
}

/* Input Signals GPIO pins - PC0, PC1 (PC0=0 PAL60, PC1=0 PAL) */
void config_gpio_sig(void) {
	/* GPIOC Periph clock enable */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);

	/* Configure GPIO Settings */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_25MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
}
#elif defined (STM32F767xx)

/* Input/Output data GPIO pins PD{0..7}
 * Enable pull down
 */
void config_gpio_data(void) {
	DATA_GPIO_CLK_ENABLE();
	/* Configure GPIO Settings */
	GPIO_InitStructure.Pin = DATA_PINS;
	GPIO_InitStructure.Mode = MODE_INPUT;
//	GPIO_InitStructure.Alternate = 0;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_MEDIUM;
	GPIO_InitStructure.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(DATA_PORT, &GPIO_InitStructure);
#ifdef POR_FORCE_NOP
	// output NOPs to the 6502 incase it comes up while we are init'ing.
	// Unlikely given the large cap on it's reset line, but it might after a quick power cycle.
	SET_6502_NOP();
	SET_DATA_MODE_OUT;
#endif
}

/* Input Address GPIO pins on PF{0..15}
 * Enable pull down
 */
void config_gpio_addr(void) {
	/* GPIOF Periph clock enable */
	ADDR_GPIO_CLK_ENABLE();
	/* Configure GPIO Settings */
	GPIO_InitStructure.Pin = ADDR_PINS;
	GPIO_InitStructure.Mode = MODE_INPUT;
//	GPIO_InitStructure.Alternate = 0;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_MEDIUM;	// actually doesn't have affect on input mode.
	GPIO_InitStructure.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(ADDR_PORT, &GPIO_InitStructure);
}

/* Input Signals GPIO pins - PG0, PG1 (PG0=0 PAL60, PG1=0 PAL) */
void config_gpio_sig(void) {
	CONTROL_GPIO_CLK_ENABLE();
	/* Configure GPIO Settings */
	GPIO_InitStructure.Pin = CONTROL_PINS;
	GPIO_InitStructure.Mode = MODE_INPUT;
//	GPIO_InitStructure.Alternate = 0;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_MEDIUM;	// actually doesn't have affect on input mode.
	GPIO_InitStructure.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(CONTROL_PORT, &GPIO_InitStructure);
}
#endif	// #elif defined (STM32F767xx)

/* config User Interface GPIO - blue btn */
void config_gpio_ui(void)
{
#ifdef HAVE_UI_BTN
	UI_BTN_GPIO_CLK_ENABLE();
	/* Configure GPIO Settings */
	GPIO_InitStructure.Pin = UI_BTN_PIN;
	GPIO_InitStructure.Mode = MODE_INPUT;
//	GPIO_InitStructure.Alternate = 0;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_MEDIUM;	// actually doesn't have affect on input mode.
	GPIO_InitStructure.Pull = GPIO_PULLDOWN;
	HAL_GPIO_Init(UI_BTN_PORT, &GPIO_InitStructure);
#endif
}

/*************************************************************************
 * Cartridge Emulation
 *************************************************************************/

#define setup_cartridge_image() \
	if (cart_size_bytes > 0x010000) return; \
	uint8_t* cart_rom = buffer; \
	if (!reboot_into_cartridge()) return;

//FIXME: _mg_ Bug cartPlus uses this, but cart_size_bytes is not set by cartPlus code.
#define setup_cartridge_image_with_ram() \
	if (cart_size_bytes > 0x010000) return; \
	uint8_t* cart_rom = buffer; \
	uint8_t* cart_ram = buffer + cart_size_bytes + (((~cart_size_bytes & 0x03) + 1) & 0x03); \
	if (!reboot_into_cartridge()) return;


void emulate_2k_cartridge() {
	setup_cartridge_image();

	__disable_irq();	// Disable interrupts
	uint16_t addr, addr_prev = 0;
	while (1)
	{
		while ((addr = ADDR_IN) != addr_prev)
			addr_prev = addr;
		// got a stable address
		if (addr & 0x1000)
		{ // A12 high
			DATA_OUT = DATA_OUT_SHIFT(((uint16_t)cart_rom[addr&0x7FF]));
			SET_DATA_MODE_OUT
			// wait for address bus to change
			while (ADDR_IN == addr) ;
			SET_DATA_MODE_IN
		}else
			// _mg_ Atari Internal memory space access.
			check_if_escape_from_cart();
	}
	exit_cartridge(addr);
//	__enable_irq();
}

void emulate_4k_cartridge() {
	setup_cartridge_image();

	__disable_irq();	// Disable interrupts
	uint16_t addr, addr_prev = 0;
	while (1)
	{
		while ((addr = ADDR_IN) != addr_prev)
			addr_prev = addr;
		// got a stable address
		if (addr & 0x1000)
		{ // A12 high
			DATA_OUT = ((uint16_t)cart_rom[addr&0x0FFF]);
			SET_DATA_MODE_OUT
			// wait for address bus to change
			while (ADDR_IN == addr) ;
			SET_DATA_MODE_IN
		}// Todo: _mg_ Bring in cartPlus version AND my emulation escape method. check_if_escape_from_cart()
	}
	__enable_irq();
}

void emulate_4ksc_cartridge()
{
	setup_cartridge_image_with_ram();

	__disable_irq();	// Disable interrupts

	uint16_t addr = 0, addr_prev = 0, data = 0, data_prev = 0;

	while (1)
	{
		while ((addr = ADDR_IN) != addr_prev)
			addr_prev = addr;
		// got a stable address
		if (addr & 0x1000)
		{ // A12 high
			if (addr & 0x0f00) {
				DATA_OUT = DATA_OUT_SHIFT(((uint16_t)cart_rom[addr & 0x0fff]));
				SET_DATA_MODE_OUT
				// wait for address bus to change
				while (ADDR_IN == addr) ;
				SET_DATA_MODE_IN
			} else if (addr & 0x0080) {
				DATA_OUT = DATA_OUT_SHIFT(((uint16_t)cart_ram[addr & 0x007f]));
				SET_DATA_MODE_OUT;
				while (ADDR_IN == addr);
				SET_DATA_MODE_IN;
			} else {
				while (ADDR_IN == addr) { data_prev = data; data = DATA_IN; }
				cart_ram[addr & 0x007f] =  DATA_IN_SHIFT(data_prev);
			}// Todo: _mg_ Bring in cartPlus version AND my emulation escape method. check_if_escape_from_cart()
		}
	}

	__enable_irq();
}

/* 'Standard' Bankswitching
 * ------------------------
 * Used by F8(8k), F6(16k), F4(32k), EF(64k)
 * and F8SC(8k), F6SC(16k), F4SC(32k), EFSC(64k)
 *
 * SC variants have 128 bytes of RAM:
 * RAM read port is $1080 - $10FF, write port is $1000 - $107F.
 *
 * Example cartridge:
 * _mg_ F8: Atari Androman on the Moon. (Not SC, LowBS $1FF8, highBS $1FF9)
 * _mg_ F8: Atari Ateroids (no copyright) (Not SC, LowBS $1FF8, highBS $1FF9)
 */
void emulate_FxSC_cartridge(uint16_t lowBS, uint16_t highBS, int isSC)
{
	setup_cartridge_image_with_ram();

	__disable_irq();	// Disable interrupts
	uint16_t addr, addr_prev = 0, data = 0, data_prev = 0;
	unsigned char *bankPtr = &cart_rom[0];

	while (1)
	{
		while ((addr = ADDR_IN) != addr_prev)
			addr_prev = addr;
		// got a stable address
		if (addr & 0x1000)
		{ // A12 high
			if (addr >= lowBS && addr <= highBS)	// bank-switch
				bankPtr = &cart_rom[(addr-lowBS)*4*1024];

			if (isSC && (addr & 0x1F00) == 0x1000)
			{	// SC RAM access
				if (addr & 0x0080)
				{	// a read from cartridge ram
					DATA_OUT = DATA_OUT_SHIFT(((uint16_t)cart_ram[addr&0x7F]));
					SET_DATA_MODE_OUT
					// wait for address bus to change
					while (ADDR_IN == addr) ;
					SET_DATA_MODE_IN
				}
				else
				{	// a write to cartridge ram
					// read last data on the bus before the address lines change
					while (ADDR_IN == addr) { data_prev = data; data = DATA_IN; }
					cart_ram[addr&0x7F] = DATA_IN_SHIFT(data_prev);
				}
			}
			else
			{	// normal rom access
				DATA_OUT = ((uint16_t)bankPtr[addr&0x0FFF]);
				SET_DATA_MODE_OUT
				// wait for address bus to change
				while (ADDR_IN == addr) ;
				SET_DATA_MODE_IN
			}// Todo: _mg_ Bring in cartPlus version AND my emulation escape method. check_if_escape_from_cart()
		}
	}
	__enable_irq();
}

/* FA (CBS RAM plus) Bankswitching
 * -------------------------------
 * Similar to the FxSC above, but with 3 ROM banks for a total of 12K
 * plus 256 bytes of RAM:
 * RAM read port is $1100 - $11FF, write port is $1000 - $10FF.
 */
void emulate_FA_cartridge()
{
	setup_cartridge_image_with_ram();//FIXME: _mg_ Bug cartPlus uses this, but cart_size_bytes is not set by cartPlus code. Also other emulators here.

	__disable_irq();	// Disable interrupts
	uint16_t addr, addr_prev = 0, data = 0, data_prev = 0;
	unsigned char *bankPtr = &cart_rom[0];

	while (1)
	{
		while ((addr = ADDR_IN) != addr_prev)
			addr_prev = addr;
		// got a stable address
		if (addr & 0x1000)
		{ // A12 high
			if (addr >= 0x1FF8 && addr <= 0x1FFA)	// bank-switch
				bankPtr = &cart_rom[(addr-0x1FF8)*4*1024];

			if ((addr & 0x1F00) == 0x1100)
			{	// a read from cartridge ram
				DATA_OUT = DATA_OUT_SHIFT(((uint16_t)cart_ram[addr&0x00FF]));
				SET_DATA_MODE_OUT
				// wait for address bus to change
				while (ADDR_IN == addr) ;
				SET_DATA_MODE_IN
			}
			else if ((addr & 0x1F00) == 0x1000)
			{	// a write to cartridge ram
				// read last data on the bus before the address lines change
				while (ADDR_IN == addr) { data_prev = data; data = DATA_IN; }
				cart_ram[addr&0x0FF] = DATA_IN_SHIFT(data_prev);
			}
			else
			{	// normal rom access
				DATA_OUT = DATA_OUT_SHIFT(((uint16_t)bankPtr[addr&0x0FFF]));
				SET_DATA_MODE_OUT
				// wait for address bus to change
				while (ADDR_IN == addr) ;
				SET_DATA_MODE_IN
			}// Todo: _mg_ Bring in cartPlus version AND my emulation escape method. check_if_escape_from_cart()
		}
	}
	__enable_irq();
}

/* FE Bankswitching
 * ----------------
 * The text below is quoted verbatim from the source code of the Atari
 * 2600 emulator Stella (https://github.com/stella-emu) which was the
 * best reference that I could find for FE bank-switching.
 * The implementation below is based on this description, and the relevant
 * source files in Stella.
 */

/*
  Bankswitching method used by Activision's Robot Tank and Decathlon.

  This scheme was originally designed to have up to 8 4K banks, and is
  triggered by monitoring the address bus for address $01FE.  All released
  carts had only two banks, and this implementation assumes that (ie, ROM
  is always 8K, and there are two 4K banks).

  The following is paraphrased from the original patent by David Crane,
  European Patent Application # 84300730.3, dated 06.02.84:
  ---------------------------------------------------------------------------
  The twelve line address bus is connected to a plurality of 4K by eight bit
  memories.

  The eight line data bus is connected to each of the banks of memory, also.
  An address comparator is connected to the bus for detecting the presence of
  the 01FE address.  Actually, the comparator will detect only the lowest 12
  bits of 1FE, because of the twelve bit limitation of the address bus.  Upon
  detection of the 01FE address, a one cycle delay is activated which then
  actuates latch connected to the data bus.  The three most significant bits
  on the data bus are latched and provide the address bits A13, A14, and A15
  which are then applied to a 3 to 8 de-multiplexer.  The 3 bits A13-A15
  define a code for selecting one of the eight banks of memory which is used
  to enable one of the banks of memory by applying a control signal to the
  enable, EN, terminal thereof.  Accordingly, memory bank selection is
  accomplished from address codes on the data bus following a particular
  program instruction, such as a jump to subroutine.
  ---------------------------------------------------------------------------

  Note that in the general scheme, we use D7, D6 and D5 for the bank number
  (3 bits, so 8 possible banks).  However, the scheme as used historically
  by Activision only uses two banks.  Furthermore, the two banks it uses
  are actually indicated by binary 110 and 111, and translated as follows:

	binary 110 -> decimal 6 -> Upper 4K ROM (bank 1) @ $D000 - $DFFF
	binary 111 -> decimal 7 -> Lower 4K ROM (bank 0) @ $F000 - $FFFF

  Since the actual bank numbers (0 and 1) do not map directly to their
  respective bitstrings (7 and 6), we simply test for D5 being 0 or 1.
  This is the significance of the test '(value & 0x20) ? 0 : 1' in the code.

  NOTE: Consult the patent application for more specific information, in
		particular *why* the address $01FE will be placed on the address
		bus after both the JSR and RTS opcodes.

  @author  Stephen Anthony; with ideas/research from Christian Speckner and
		   alex_79 and TomSon (of AtariAge)
*/
void emulate_FE_cartridge()
{
	setup_cartridge_image();//FIXME: _mg_ Bug cartPlus uses this, but cart_size_bytes is not set by cartPlus code. Also other emulators here.

	__disable_irq();	// Disable interrupts
	uint16_t addr, addr_prev = 0, data = 0, data_prev = 0;
	unsigned char *bankPtr = &cart_rom[0];
	int lastAccessWasFE = 0;

	while (1)
	{
		while ((addr = ADDR_IN) != addr_prev)
			addr_prev = addr;
		// got a stable address
		if (!(addr & 0x1000))
		{	// A12 low, read last data on the bus before the address lines change
			while (ADDR_IN == addr) { data_prev = data; data = DATA_IN; }
			data = DATA_IN_SHIFT(data_prev);
		}
		else
		{ // A12 high
			data = bankPtr[addr&0x0FFF];
			DATA_OUT = DATA_OUT_SHIFT(data);
			SET_DATA_MODE_OUT
			// wait for address bus to change
			while (ADDR_IN == addr) ;
			SET_DATA_MODE_IN
		}
		// end of cycle
		if (lastAccessWasFE)
		{	// bank-switch - check the 5th bit of the data bus
			if (data & 0x20)
				bankPtr = &cart_rom[0];
			else
				bankPtr = &cart_rom[4 * 1024];
		}// Todo: _mg_ Bring in cartPlus version AND my emulation escape method. check_if_escape_from_cart()
		lastAccessWasFE = (addr == 0x01FE);
	}
	__enable_irq();
}

/* Scheme as described by Eckhard Stolberg. Didn't work on my test 7800, so replaced
 * by the simpler 3F only scheme above.
	while (1)
	{
		while ((addr = ADDR_IN) != addr_prev)
			addr_prev = addr;
		// got a stable address
		if (!(addr & 0x1000))
		{	// A12 low, read last data on the bus before the address lines change
			while (ADDR_IN == addr) { data_prev = data; data = DATA_IN; }
			data = DATA_IN_SHIFT(data_prev);
			if (addr <= 0x003F) newPage = data % cartPages; else newPage = -1;
		}
		else
		{ // A12 high
			if (newPage >=0) {
				bankPtr = &cart_rom[newPage*2048];	// switch bank
				newPage = -1;
			}
			if (addr & 0x800)
				data = fixedPtr[addr&0x7FF];
			else
				data = bankPtr[addr&0x7FF];
			DATA_OUT = DATA_OUT_SHIFT(data);
			SET_DATA_MODE_OUT
			// wait for address bus to change
			while (ADDR_IN == addr) ;
			SET_DATA_MODE_IN
		}
	}
 */

/* E0 Bankswitching
 * ------------------------------
 * The text below is the best description of the mapping scheme I could find,
 * quoted from http://blog.kevtris.org/blogfiles/Atari%202600%20Mappers.txt
 */

/*
Parker Brothers used this, and it was used on one other game (Tooth Protectors).  It
uses 8K of ROM and can map 1K sections of it.

This mapper has 4 1K banks of ROM in the address space.  The address space is broken up
into the following locations:

1000-13FF : To select a 1K ROM bank here, access 1FE0-1FE7 (1FE0 = select first 1K, etc)
1400-17FF : To select a 1K ROM bank, access 1FE8-1FEF
1800-1BFF : To select a 1K ROM bank, access 1FF0-1FF7
1C00-1FFF : This is fixed to the last 1K ROM bank of the 8K

Like F8, F6, etc. accessing one of the locations indicated will perform the switch.
*/
void emulate_E0_cartridge()
{
	setup_cartridge_image();

	__disable_irq();	// Disable interrupts
	uint16_t addr, addr_prev = 0;
	unsigned char curBanks[4] = {0,0,0,7};

	while (1)
	{
		while ((addr = ADDR_IN) != addr_prev)
			addr_prev = addr;
		// got a stable address
		if (addr & 0x1000)
		{ // A12 high

			if (addr >= 0x1FE0 && addr <= 0x1FF7)
			{	// bank-switching addresses
				if (addr <= 0x1FE7)	// switch 1st bank
					curBanks[0] = addr-0x1FE0;
				else if (addr >= 0x1FE8 && addr <= 0x1FEF)	// switch 2nd bank
					curBanks[1] = addr-0x1FE8;
				else if (addr >= 0x1FF0)	// switch 3rd bank
					curBanks[2] = addr-0x1FF0;
			}
			// fetch data from the correct bank
			int target = (addr & 0xC00) >> 10;
			DATA_OUT = DATA_OUT_SHIFT(((uint16_t)cart_rom[curBanks[target]*1024 + (addr&0x3FF)]));
			SET_DATA_MODE_OUT
			// wait for address bus to change
			while (ADDR_IN == addr) ;
			SET_DATA_MODE_IN
		}// Todo: _mg_ Bring in cartPlus version AND my emulation escape method. check_if_escape_from_cart()
	}
	__enable_irq();

}

/* 0840 Bankswitching
 * ------------------------------
 * 8k cartridge with two 4k banks.
 * The following description was derived from:
 * http://blog.kevtris.org/blogfiles/Atari%202600%20Mappers.txt
 *
 * Bankswitch triggered by access to an address matching the pattern below:
 *
 * A12            A0
 * ----------------
 * 0 1xxx xBxx xxxx (x = don't care, B is the bank we select)
 *
 * If address AND $1840 == $0800, then we select bank 0
 * If address AND $1840 == $0840, then we select bank 1
 */
void emulate_0840_cartridge()
{
	setup_cartridge_image();

	__disable_irq();	// Disable interrupts
	uint16_t addr, addr_prev = 0, addr_prev2 = 0;
	unsigned char *bankPtr = &cart_rom[0];

	while (1)
	{
		while (((addr = ADDR_IN) != addr_prev) || (addr != addr_prev2))
		{	// new more robust test for stable address (seems to be needed for 7800)
			addr_prev2 = addr_prev;
			addr_prev = addr;
		}
		// got a stable address
		if (addr & 0x1000)
		{ // A12 high
			DATA_OUT = DATA_OUT_SHIFT(((uint16_t)bankPtr[addr&0x0FFF]));
			SET_DATA_MODE_OUT
			// wait for address bus to change
			while (ADDR_IN == addr) ;
			SET_DATA_MODE_IN
		}
		else
		{
			if ((addr & 0x0840) == 0x0800) bankPtr = &cart_rom[0];
			else if ((addr & 0x0840) == 0x0840) bankPtr = &cart_rom[4*1024];
			// wait for address bus to change
			while (ADDR_IN == addr) ;
		}// Todo: _mg_ Bring in cartPlus version AND my emulation escape method. check_if_escape_from_cart()
	}
	__enable_irq();
}

/* CommaVid Cartridge
 * ------------------------------
 * 2K ROM + 1K RAM
 *  $F000-$F3FF 1K RAM read
 *  $F400-$F7FF 1K RAM write
 *  $F800-$FFFF 2K ROM
 */
void emulate_CV_cartridge()
{
	setup_cartridge_image_with_ram();

	__disable_irq();	// Disable interrupts
	uint16_t addr, addr_prev = 0, data = 0, data_prev = 0;

	while (1)
	{
		while ((addr = ADDR_IN) != addr_prev)
			addr_prev = addr;
		// got a stable address
		if (addr & 0x1000)
		{ // A12 high
			if (addr & 0x0800)
			{	// ROM read
				DATA_OUT = DATA_OUT_SHIFT(((uint16_t)cart_rom[addr&0x7FF]));
				SET_DATA_MODE_OUT
				// wait for address bus to change
				while (ADDR_IN == addr) ;
				SET_DATA_MODE_IN
			}// Todo: _mg_ Bring in cartPlus version AND my emulation escape method. check_if_escape_from_cart()
			else
			{	// RAM access
				if (addr & 0x0400)
				{	// a write to cartridge ram
					// read last data on the bus before the address lines change
					while (ADDR_IN == addr) { data_prev = data; data = DATA_IN; }
					cart_ram[addr&0x3FF] = DATA_IN_SHIFT(data_prev);
				}
				else
				{	// a read from cartridge ram
					DATA_OUT = DATA_OUT_SHIFT(((uint16_t)cart_ram[addr&0x3FF]));
					SET_DATA_MODE_OUT
					// wait for address bus to change
					while (ADDR_IN == addr) ;
					SET_DATA_MODE_IN
				}
			}
		}
	}
	__enable_irq();
}

/* F0 Bankswitching
 * ------------------------------
 * 64K cartridge with 16 x 4K banks. An access to $1FF0 switches to the next
 * bank in sequence.
 */
void emulate_F0_cartridge()
{
	setup_cartridge_image();

	__disable_irq();	// Disable interrupts
	uint16_t addr, addr_prev = 0;
	int currentBank = 0;

	while (1)
	{
		while ((addr = ADDR_IN) != addr_prev)
			addr_prev = addr;
		// got a stable address
		if (addr & 0x1000)
		{ // A12 high
			if (addr == 0x1FF0)
				currentBank = (currentBank + 1) % 16;
			// ROM access
			DATA_OUT = DATA_OUT_SHIFT(((uint16_t)cart_rom[(currentBank * 4096)+(addr&0x0FFF)]));
			SET_DATA_MODE_OUT
			// wait for address bus to change
			while (ADDR_IN == addr) ;
			SET_DATA_MODE_IN
		}// Todo: _mg_ Bring in cartPlus version AND my emulation escape method. check_if_escape_from_cart()
	}
	__enable_irq();
}

/* E7 Bankswitching
 * ------------------------------
 * 16K cartridge with additional RAM.
 * The text below is the best description of the mapping scheme I could find,
 * quoted from http://blog.kevtris.org/blogfiles/Atari%202600%20Mappers.txt
 */

/*
M-network wanted something of their own too, so they came up with what they called
"Big Game" (this was printed on the prototype ASICs on the prototype carts).  It
can handle up to 16K of ROM and 2K of RAM.

1000-17FF is selectable
1800-19FF is RAM
1A00-1FFF is fixed to the last 1.5K of ROM

Accessing 1FE0 through 1FE6 selects bank 0 through bank 6 of the ROM into 1000-17FF.
Accessing 1FE7 enables 1K of the 2K RAM, instead.

When the RAM is enabled, this 1K appears at 1000-17FF.  1000-13FF is the write port, 1400-17FF
is the read port.

1800-19FF also holds RAM. 1800-18FF is the write port, 1900-19FF is the read port.
Only 256 bytes of RAM is accessable at time, but there are four different 256 byte
banks making a total of 1K accessable here.

Accessing 1FE8 through 1FEB select which 256 byte bank shows up.
 */
void emulate_E7_cartridge()
{
	setup_cartridge_image_with_ram();

	__disable_irq();	// Disable interrupts
	uint16_t addr, addr_prev = 0, data = 0, data_prev = 0;
	unsigned char *bankPtr = &cart_rom[0];
	unsigned char *fixedPtr = &cart_rom[(8-1)*2048];
	unsigned char *ram1Ptr = &cart_ram[0];
	unsigned char *ram2Ptr = &cart_ram[1024];
	int ram_mode = 0;

	while (1)
	{
		while ((addr = ADDR_IN) != addr_prev)
			addr_prev = addr;
		// got a stable address
		if (addr & 0x1000)
		{ // A12 high
			if (addr & 0x0800)
			{	// higher 2k cartridge ROM area
				if ((addr & 0x0E00) == 0x0800)
				{	// 256 byte RAM access
					if (addr & 0x0100)
					{	// 1900-19FF is the read port
						DATA_OUT = DATA_OUT_SHIFT(((uint16_t)ram1Ptr[addr&0x0FF]));
						SET_DATA_MODE_OUT
						// wait for address bus to change
						while (ADDR_IN == addr) ;
						SET_DATA_MODE_IN
					}
					else
					{	// 1800-18FF is the write port
						while (ADDR_IN == addr) { data_prev = data; data = DATA_IN; }
						ram1Ptr[addr&0x0FF] = DATA_IN_SHIFT(data_prev);
					}
				}// Todo: _mg_ Bring in cartPlus version AND my emulation escape method. check_if_escape_from_cart()
				else
				{	// fixed ROM bank access
					// check bankswitching addresses
					if (addr >= 0x1FE0 && addr <= 0x1FE7)
					{
						if (addr == 0x1FE7) ram_mode = 1;
						else
						{
							bankPtr = &cart_rom[(addr - 0x1FE0)*2048];
							ram_mode = 0;
						}
					}
					else if (addr >= 0x1FE8 && addr <= 0x1FEB)
						ram1Ptr = &cart_ram[(addr - 0x1FE8)*256];

					DATA_OUT = DATA_OUT_SHIFT(((uint16_t)fixedPtr[addr&0x7FF]));
					SET_DATA_MODE_OUT
					// wait for address bus to change
					while (ADDR_IN == addr) ;
					SET_DATA_MODE_IN
				}
			}
			else
			{	// lower 2k cartridge ROM area
				if (ram_mode)
				{	// 1K RAM access
					if (addr & 0x400)
					{	// 1400-17FF is the read port
						DATA_OUT = DATA_OUT_SHIFT(((uint16_t)ram2Ptr[addr&0x3FF]));
						SET_DATA_MODE_OUT
						// wait for address bus to change
						while (ADDR_IN == addr) ;
						SET_DATA_MODE_IN
					}
					else
					{	// 1000-13FF is the write port
						while (ADDR_IN == addr) { data_prev = data; data = DATA_IN; }
						ram2Ptr[addr&0x3FF] = DATA_IN_SHIFT(data_prev);
					}
				}
				else
				{	// selected ROM bank access
					DATA_OUT = DATA_OUT_SHIFT(((uint16_t)bankPtr[addr&0x7FF]));
					SET_DATA_MODE_OUT
					// wait for address bus to change
					while (ADDR_IN == addr) ;
					SET_DATA_MODE_IN
				}
			}
		}
	}
	__enable_irq();
}


/*************************************************************************
 * UnoCart Main loop/helper functions
 *************************************************************************/

#ifndef CARTPLUS_ENABLE
void emulate_cartridge(int cart_type)
{
	if (cart_type == CART_TYPE_2K)
		emulate_2k_cartridge();
	else if (cart_type == CART_TYPE_4K)
		emulate_4k_cartridge();
	else if (cart_type == CART_TYPE_4KSC)
		emulate_4ksc_cartridge();
	else if (cart_type == CART_TYPE_F8)
		emulate_FxSC_cartridge(0x1FF8, 0x1FF9, 0);
	else if (cart_type == CART_TYPE_F6)
		emulate_FxSC_cartridge(0x1FF6, 0x1FF9, 0);
	else if (cart_type == CART_TYPE_F4)
		emulate_FxSC_cartridge(0x1FF4, 0x1FFB, 0);
	else if (cart_type == CART_TYPE_F8SC)
		emulate_FxSC_cartridge(0x1FF8, 0x1FF9, 1);
	else if (cart_type == CART_TYPE_F6SC)
		emulate_FxSC_cartridge(0x1FF6, 0x1FF9, 1);
	else if (cart_type == CART_TYPE_F4SC)
		emulate_FxSC_cartridge(0x1FF4, 0x1FFB, 1);
	else if (cart_type == CART_TYPE_FE)
		emulate_FE_cartridge();
	else if (cart_type == CART_TYPE_3F)
		emulate_3f_cartridge(cartridge_image_path, cart_size_bytes, buffer);
	else if (cart_type == CART_TYPE_3E)
		emulate_3e_cartridge(cartridge_image_path, cart_size_bytes, buffer, 32);
	else if (cart_type == CART_TYPE_3EX)
		emulate_3e_cartridge(cartridge_image_path, cart_size_bytes, buffer, BUFFER_SIZE_KB);
	else if (cart_type == CART_TYPE_E0)
		emulate_E0_cartridge();
	else if (cart_type == CART_TYPE_0840)
		emulate_0840_cartridge();
	else if (cart_type == CART_TYPE_CV)
		emulate_CV_cartridge();
	else if (cart_type == CART_TYPE_EF)
		emulate_FxSC_cartridge(0x1FE0, 0x1FEF, 0);
	else if (cart_type == CART_TYPE_EFSC)
		emulate_FxSC_cartridge(0x1FE0, 0x1FEF, 1);
	else if (cart_type == CART_TYPE_F0)
		emulate_F0_cartridge();
	else if (cart_type == CART_TYPE_FA)
		emulate_FA_cartridge();
	else if (cart_type == CART_TYPE_E7)
		emulate_E7_cartridge();
	else if (cart_type == CART_TYPE_DPC)
		emulate_dpc_cartridge(buffer, cart_size_bytes);
	else if (cart_type == CART_TYPE_AR)
		emulate_supercharger_cartridge(cartridge_image_path, cart_size_bytes, buffer, tv_mode);
	else if (cart_type == CART_TYPE_BF)
		emulate_bf_cartridge(cartridge_image_path, cart_size_bytes, buffer);
	else if (cart_type == CART_TYPE_BFSC)
		emulate_bfsc_cartridge(cartridge_image_path, cart_size_bytes, buffer);
	else if (cart_type == CART_TYPE_ACE)
	{
		if(launch_ace_cartridge(cartridge_image_path, BUFFER_SIZE_KB * 1024, buffer))
			set_menu_status_msg("GOOD ACE ROM");
		else
			set_menu_status_msg("BAD ACE FILE");
	}
	else if (cart_type == CART_TYPE_PP) {
		emulate_pp_cartridge(cart_size_bytes, buffer, buffer + 8*1024);
	}
	else if (cart_type == CART_TYPE_DF) {
		emulate_df_cartridge(cartridge_image_path, cart_size_bytes, buffer);
	}
	else if (cart_type == CART_TYPE_DFSC) {
		emulate_dfsc_cartridge(cartridge_image_path, cart_size_bytes, buffer);
	}
	else if (cart_type == CART_TYPE_3EP){
		emulate_3EPlus_cartridge(buffer, cart_size_bytes);
    }

}


/**
 * @brief Truncate a long filename to first 12 characters
 * and translate to uppercase.
 */
void convertFilenameForCart(unsigned char *dst, char *src)
{
	memset(dst, ' ', 12);
	for (int i=0; i<12; i++) {
		if (!src[i]) break;
		char c = toupper(src[i]);
		dst[i] = c;
	}
}

/**
 * @brief Read a directory of cartridge names
 * convert long filenames to short names (12 chars) and populate
 * into the cartridge menu RAM bank.
 *
 * @param path to directory to read
 * @return 1 if OK, 0 if not.
 */
int readDirectoryForAtari(char *path)
{
#ifdef USE_MONOLITHIC_FS_UTILS
	int ret = read_directory(path);
#else
	int ret = read_directory(path,FS_NO_TRACE);
	num_dir_entries = fs_get_number_dir_entries();
	dir_entries = fs_get_dir_entries();
#endif
	uint8_t *menu_ram = get_menu_ram();	// _mg_ in cartridge_firmware.c  does size need to track NUM_DIR_ITEMS * 12 ???
	// create a table of entries for the atari to read
	memset(menu_ram, 0, get_menu_ram_size()/*1024*/);
	// FIXME: _mg_  12 char menu entries is hardcoded, use macro.
	for (int i=0; i<num_dir_entries; i++)
	{
		unsigned char *dst = menu_ram + i*12;
		convertFilenameForCart(dst, dir_entries[i].long_filename);
		// set the high-bit of the first character if directory
		if (dir_entries[i].isDir) *dst += 0x80;
	}
	return ret;
}

#endif	// CARTPLUS_ENABLE
/*
 * Dump a capture of the 6502 address bus.
 * Waits for address to change, then capture address bus continously until buffer full.
 * Store buffer to bus_dump.bin on SD card.
 * Output 6502 NOPs while capturing data.
 *
 * Format is:
 * First 32 bit word is 0x12345678  to remind me of endianness of file system writing.
 * Second 32 bit word is start_cycle count of cycles for address to first change.
 * Third 32 bit word is loops per mSec measurement. _mg_ added to get an idea of reset time of Atari.
 * The remainder of data are 16 bit address values.
 *
 * Does not return, blinks blue LED forever when complete.
 * If there was an error with the SD card, red LED blinks forever.
 */
void bus_dumper() {
	__disable_irq();
	register uint32_t start_cycles = 0;

#ifdef BUS_DUMPER_NOPS
	// send NOPs to the 6502
	SET_6502_NOP();
	SET_DATA_MODE_OUT;
#else
	SET_DATA_MODE_IN
#endif
	// use registers for tighter, faster code.
	register __IO uint32_t * p_addr_in = &ADDR_IN;
	register uint16_t addr = *p_addr_in; //ADDR_IN;
	register uint32_t capture_size = CCM_SIZE/2ul;
	while (*p_addr_in == addr)	// 5 ARM instructions per loop cycle
		start_cycles++;
	// start_cycles
	// On my Heavy Sixer;
	// With NOP instruction output:
	// On the STM32F767ZI @ 216 MHz: start_cycles = 0xf361, 0xd423 (about 4 mSec). With faster STM32 start-up: 0x04ada5 ~ 16 mSec
	// On the STM32F767ZI @ 216 MHz:
	// 	6502 Reset vector address read is 12 loops per byte.
	// 	First address after reset vector read is only 12 loops. All following are 24 loops between address change.
	// With no NOP instruction output: Data bus 00 = BRK
	//  Instruction cycles are shorter 5 to 6 samples per address.
	// Above was with I-cache and D-cache disabled.
	// With I-cache and D-cache enabled:
	// Measured 1 mSec time is 0xd294 cycles.
	// Average of 20 samples per address.
	//
	for (register uint32_t i = 7; i < capture_size; i++)	//  7 ARM instuctions per loop.
		CCM_RAM_u16[i] = (*p_addr_in & 0xffff);

	__enable_irq();

	CCM_RAM_u32[0] = 0x12345678;
	CCM_RAM_u32[1] = start_cycles;
	CCM_RAM_u16[6] = addr;

	// Lets get an idea of loops per mSec
	start_cycles = 0;
	register uint32_t start_tm = HAL_GetTick() + 500ul;
	while(start_tm > uwTick){
		start_cycles++;			// on STM32F767ZI this is ~ 0x3497
	}
	start_cycles /= 500ul;;
	CCM_RAM_u32[2] = start_cycles;	// loops per mSec.

	// _mg_ On our Heavy Sixer, there seems to be reset bouncing, there will be a short burst of sequential address
	// then the Reset Vector is read again, this may repeat 3 or 4 time,
	// then exectution is sequential for the rest of the capture file.

	FIL file;
	UINT bytes_written;
	FATFS FatFs;
	FRESULT fres; //Result after operations

	fres = f_mount(&FatFs, "", 1); //1=mount now
	if (fres != FR_OK) {
		BSP_Red_Blink_Forever();
	}

	fres = f_open(&file, "bus_dump.bin", FA_WRITE | FA_OPEN_ALWAYS | FA_CREATE_ALWAYS);
	if (fres != FR_OK) {
		BSP_Red_Blink_Forever();
	}
	f_write(&file, CCM_RAM, CCM_SIZE, &bytes_written);
	f_close(&file);

	f_mount(0, "", 1);

	BSP_Blue_Blink_Forever();
	while (1) {};
}


#if defined(STM32F767xx)

#ifdef USE_MONOLITHIC_FS_UTILS

#ifdef TEST_SD_CARD
/* Todo: _mg_ replace this scan_directories() code with one in fs_utils.c
 *
 * Reentrant function to scan through all directories and dump content out serial port.
 * Initial Caller must setup curpath. e.g. curpath[0]=0.
 * Max directory depth traversable will depend on stack size available,
 * DANGER! DANGER! too deep and this may hard-fault crash.
 *
 * Serial port dump of directory objects happens in calls to readDirectoryForAtari()
 */
void scan_directories(char* curPath)
{
	char newPath[256];
	newPath[0]=0;
	myprintf("\n*** cd to: %s/ \n", curPath);
	if (!readDirectoryForAtari(curPath))
	{
		myprintf("CANT READ SD");
		BSP_Red_Blink_Forever();
	}
	int sel, this_dir_entries;
	this_dir_entries = num_dir_entries;
	myprintf("*** %d items of interest in %s/ \n", this_dir_entries, curPath);

	for( sel = 0; sel < this_dir_entries; sel++)
	{
		DIR_ENTRY *d = &dir_entries[sel];

		if (d->isDir)
		{	// selection is a directory
			if (!strcmp(d->filename, ".."))
			{	// go on
				continue;
			}
			// go into director
			strcpy(newPath, curPath);
			strcat(newPath, "/");
			strcat(newPath, d->THE_FILENAME);
			scan_directories(newPath);
			myprintf("\n<< back to: %s/ \n", curPath);
			// re-read the curPath to restore dir_entries[] to curPath
			if (!readDirectoryForAtari(curPath))
			{
				myprintf("CANT READ SD");
				BSP_Red_Blink_Forever();
			}
		}
	}
}
/* _mg_ replaced this test_sd() code with test_fs() in fs_utils.c
 * @brief Test SD card and dump file system info.
 *
 * This mounts the SD card file system.
 * Execute some read/write tests on the SD card.
 * Then dump some directories if found. -> Enable macro: G_DEBUG_PRINT()
 *
 * Expects to find /read.txt file with some data in it.
 *
 * Creates /write.txt file with some data in it.
 *
 * Does not return, blinks blue LED forever when complete.
 * If there was an error with the SD card, red LED blinks forever.
 */
void test_sd(void)
{
	myprintf("\n~ SD card tests by MarkG ~\n\n");

	HAL_Delay(1000); //a short delay is important to let the SD card settle after power on.

	//some variables for FatFs
	FATFS FatFs; 	//Fatfs handle
	FIL fil; 		//File handle
	FRESULT fres;   //Result after operations

	//Open the file system
	fres = f_mount(&FatFs, "", 1); //1=mount now
	if (fres != FR_OK) {
		myprintf("f_mount error (%i)\n", fres);
		BSP_Red_Blink_Forever();
	}

	//Let's get some statistics from the SD card
	DWORD free_clusters, free_sectors, total_sectors;

	FATFS* getFreeFs;

	fres = f_getfree("", &free_clusters, &getFreeFs);
	if (fres != FR_OK) {
		myprintf("f_getfree error (%i)\n", fres);
		BSP_Red_Blink_Forever();
	}

	//Formula comes from ChaN's documentation
	total_sectors = (getFreeFs->n_fatent - 2) * getFreeFs->csize;
	free_sectors = free_clusters * getFreeFs->csize;

	myprintf("SD card stats:\n%10lu KiB total drive space.\n%10lu KiB available.\n", total_sectors / 2, free_sectors / 2);

	//Read 30 bytes from "read.txt" on the SD card
	BYTE readBuf[30];

	myprintf("f_open: read.txt\n");
	fres = f_open(&fil, "read.txt", FA_READ);
	if (fres != FR_OK) {
		myprintf("f_open error (%i)\n", fres);
	}else
	{
		myprintf("Opened 'read.txt' for reading!\n");
		//We can either use f_read OR f_gets to get data out of files
		//f_gets is a wrapper on f_read that does some string formatting for us
		TCHAR* rres = f_gets((TCHAR*)readBuf, 30, &fil);
		if(rres != 0) {
			myprintf("Read string from 'read.txt' contents: %s\n", readBuf);
		} else {
			myprintf("f_gets error (%i)\n", fres);
		}
		//Be a tidy kiwi - don't forget to close your file!
		f_close(&fil);
	}
	// -----------------------------------------------
	// try and write a file "write.txt"
	// -----------------------------------------------
	fres = f_open(&fil, "write.txt", FA_WRITE | FA_OPEN_ALWAYS | FA_CREATE_ALWAYS);
	if(fres == FR_OK) {
		myprintf("Opened 'write.txt' for writing\n");
	} else {
		myprintf("f_open error (%i)\r\n", fres);
	}

	//Copy in a string
	strncpy((char*)readBuf, "A new file is made!", 19);
	UINT bytesWrote;
	fres = f_write(&fil, readBuf, 19, &bytesWrote);
	if(fres == FR_OK) {
		myprintf("Wrote %i bytes to 'write.txt'!\n", bytesWrote);
	} else {
		myprintf("f_write error (%i)\n", fres);
	}

	//Be a tidy kiwi - don't forget to close your file!
	f_close(&fil);
	myprintf("Closed file: write.txt.\n");

	//We're done, so un-mount the drive
	f_mount(NULL, "", 0);
	myprintf("Drive Unmounted.\n");

	// -----------------------------------------------
	// dump some directories out the serial port
	// -----------------------------------------------
	myprintf("\n*** Start Dump of SD card ***\n");
	char curPath[256];
	curPath[0] = 0;
	// Danger! Danger! This next function is reentrant, might hard-fault if deep directory structure is traversed and stack gets blown.
	scan_directories(curPath);
	f_mount(NULL, "", 0);
	myprintf("\n*** Done with Dump of SD card ***\n");
	BSP_Blue_Blink_Forever();
}
#endif	// #ifdef TEST_SD_CARD

#endif	// #ifdef USE_MONOLITHIC_FS_UTILS

#ifdef TEST_IO_LINES
// Test IO lines one by one via shifting one bit over and over
// to verify wiring is correct.
// Does not return.
void test_io_lines(void)
{
	static int test;
	SET_DATA_MODE_OUT;
	SET_ADDR_MODE_OUT;
	while(1)
	{
		// test data lines.
		for(test = 0; test < 8; test++)
			DATA_OUT = 1<<test;
		DATA_OUT = 0;
		// address lines
		for(test = 0 ; test < 13; test++)
		{
			ADDR_OUT = 1<<test;
		}
		ADDR_OUT = 0;
	}
}
#endif


// newer ST HAL requires init at the top of main().
void systemInit(void)
{
	// added I-cache and D-cache enable 2022-04-12
	/* Enable I-Cache---------------------------------------------------------*/
	SCB_EnableICache();

	/* Enable D-Cache---------------------------------------------------------*/
	SCB_EnableDCache();

	HAL_Init();
	SystemClock_Config();	/* config the PLL and AHB and APB buses clocks. */
	/* In/Out:init data bus pins */
	config_gpio_data();
	/* In: pins for address inputs */
	config_gpio_addr();
	/* In: Other Cart Input Signals - PC{0..1} */
	config_gpio_sig();

	__HAL_RCC_DTCMRAMEN_CLK_ENABLE();
	__HAL_RCC_BKPSRAM_CLK_ENABLE();
	// SPI peripheral for SD Card, including IO Pins (via HAL_SPI_MspInit())
	MX_SPI2_Init();
#ifdef ENABLE_UART
	// UART3 For ST LINK myprintf()
	MX_USART3_UART_Init();
	setupDirectIO(&huart3);
	// stdio routed to UART
	stdioTarget(&huart3);
#endif
	MX_FATFS_Init();	// init the FS library

#if !defined( USE_MONOLITHIC_FS_UTILS) && !defined(CARTPLUS_ENABLE)
	// set atari cartridge file type filter
	fs_set_filename_filter(is_valid_file);
#endif
	// Config LEDs and user button
	//config_gpio_ldx_pin();
#if defined(STM32F767xx)
	BSP_LED_Init(LED_GREEN);
	BSP_LED_Init(LED_BLUE);
	BSP_LED_Init(LED_RED);
	BSP_PB_Init(BUTTON_USER, BUTTON_MODE_GPIO);
#ifdef TEST_SD_CARD
	// delay to allow COM port connection
	// Blink the LED while delaying
	BSP_LED_Blink(LED_GREEN,1200ul,2400ul,2ul);
#endif

#endif
}

// flag to signal blue button is pressed.
volatile int blue_btn_pushed = 0;
volatile int blue_btn_debounce = 0;
#else
void systemInit(void){};
void myprintf(const char *fmt, ...) {(void)fmt;};
#endif	// #if defined(STM32F767xx)

int main(void)
{
	systemInit();
#ifdef ENABLE_UART
#ifndef CARTPLUS_ENABLE
	sendString("\nHi Atari");	// _mg_ short enough to not cause timing problem when running Atari 2600 ROM Emulation on STM32F767ZI
#endif
#endif

#ifdef TEST_IO_LINES
	test_io_lines();
#endif
#ifdef TEST_SD_CARD
	for(int i = 0; i< 2; i++)
	{
		// delay to allow COM port connection
		// Blink the LED while delaying
		BSP_LED_Toggle(LED_GREEN);
		HAL_Delay(1200);
	}
    test_sd();
#endif

#ifdef CARTPLUS_ENABLE
    sendString("\n");
    // finish execution in cartPlus main loop.
    cp_main();
#endif

#ifndef CARTPLUS_ENABLE
    // UnoCart Only loop below.
    // check if bus dumper mode dip switch is on.
    if( is_dumper_mode()){
    	bus_dumper();
    }
#ifndef NTSC_ONLY
	if (is_pal60_mode())
		tv_mode = TV_MODE_PAL60;
	else if ((is_pal_mode()))
		tv_mode = TV_MODE_PAL;
	else
#endif
		tv_mode = TV_MODE_NTSC;

	char curPath[256];
	curPath[0] = 0;		// empty string is root directory.
	int cart_type = CART_TYPE_NONE;

	// malloc the RAM buffer here in order to avoid zero filling 96k of BSS
	// _mg_ I put buffer[] array in .noinit section. Save the malloc time.
	// buffer = (uint8_t*)malloc(BUFFER_SIZE_KB * 1024);

//	init();	// _mg_ was FAT_FS init. not needed for CubeMX FAT_FS

	set_tv_mode(tv_mode);

	// set up status area
	set_menu_status_msg(VERSION);
	set_menu_status_byte(0);

	LL_LedGreen_On();
	int ret = CART_CMD_ROOT_DIR;
	while (1) {
#if defined( TEST_SD_CARD) || defined (TEST_CART_CMD)
		int ret = CART_CMD_ROOT_DIR;
#else
		ret = emulate_firmware_cartridge();
	    // _mg_ When get here, the Atari 2600 6507 is running code from the 2600's internal RAM.
	    // _mg_ Therefore the STM32 has more time to do things here ...
#endif
#ifdef ENABLE_UART
		// FIXME:  _mg_ Workaround: After coming out of firmware emulation, for some reason, the UART3 peripheral is getting corrupted and requires disable/re-enable.
		// Root cause of peripheral corruption is unknown.  Ground bounce on cartridge interface?
		MX_USART3_DeInit();
		MX_USART3_UART_Init();
		myprintf("\nUnoCart %s\n",  VERSION );
#endif
		if (ret == CART_CMD_ROOT_DIR)
		{
#ifdef TRACE_SD
			myprintf("CART_CMD_ROOT_DIR\n");
#endif
			LL_LedBlue_On();
			curPath[0] = 0;
			if (!readDirectoryForAtari(curPath))
				set_menu_status_msg("CANT READ SD");
		}
		else
		{
			int sel = ret - CART_CMD_SEL_ITEM_n;
			DIR_ENTRY *d = &dir_entries[sel];
#ifdef TRACE_SD
			myprintf("sel: %d\n", sel);
#endif
			if (d->isDir)
			{	// selection is a directory
				if (!strcmp(d->filename, ".."))
				{	// go back
					int len = strlen(curPath);
#ifdef TRACE_SD
					myprintf("GoBack: curPath: [%s]  Len: %d\n", curPath, len);
#endif
					while (len && curPath[--len] != '/');
					curPath[len] = 0;
#ifdef TRACE_SD
					myprintf("After: curPath: [%s]\n", curPath);
#endif
				}
				else
				{	// go into director
					strcat(curPath, "/");
					strcat(curPath, d->THE_FILENAME);
#ifdef TRACE_SD
					myprintf("GoDir: curPath: [%s]\n", curPath);
#endif
				}

				if (!readDirectoryForAtari(curPath))
					set_menu_status_msg("CANT READ SD");
				HAL_Delay(200);
			}
			else
			{	// selection is a rom file
				LL_LedBlue_Off();
				strcpy(cartridge_image_path, curPath);
				strcat(cartridge_image_path, "/");
				strcat(cartridge_image_path, d->THE_FILENAME);
				cart_type = identify_cartridge(cartridge_image_path, d->long_filename);
#ifdef TRACE_SD
					myprintf("GoROM: curPath: [%s]  Type: %d\n", curPath, cart_type);
#endif
				HAL_Delay(200);
				if (cart_type != CART_TYPE_NONE)
					emulate_cartridge(cart_type);
				else
					set_menu_status_msg("BAD ROM FILE");
			}
		}
	}
#endif	// #ifndef CARTPLUS_ENABLE
}
