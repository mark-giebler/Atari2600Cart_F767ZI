#ifndef CARTRIDGE_IO_H
#define CARTRIDGE_IO_H

#if defined (STM32F4XX)
#include "stm32f4xx.h"

#define ADDR_IN		GPIOD->IDR
#define DATA_IN		GPIOE->IDR
#define DATA_OUT	GPIOE->ODR
#define CONTROL_IN	GPIOC->IDR
#define PAL60_CONTROL_MASK	0x0001
#define PAL_CONTROL_MASK	0x0002
#define DUMP_CONTROL_MASK	0x0001

#define DATA_IN_SHIFT(dd)	(dd>>8)
#define DATA_OUT_SHIFT(dd)	(dd<<8)
#define SET_DATA_MODE_IN	GPIOE->MODER = 0x00000000;
#define SET_DATA_MODE_OUT	GPIOE->MODER = 0x55550000;

#define DATA_IN_BYTE (DATA_IN & 0xFF00)// for cartPlus code.

#elif defined (STM32F767xx)
#include "stm32f7xx.h"
#include "stm32f7xx_hal_gpio.h"

/* _mg_ our hw configuration for the F767 nucleo board:
		A0 to A12	to	PF0 - PF12	CN11 and CN12
		D0 to D7	to	PD0 - PD7	CN11 and CN12

		Alternate Config:
		D0 to D7	to	PE8 - PE15	CN11 and CN12

		PG0			GND for PAL60
		PG1			GND for PAL
*/
#define ADDR_PORT	GPIOF
#define ADDR_PINS	GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | \
					GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7 | \
					GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_11 | \
					GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15
#define ADDR_GPIO_CLK_ENABLE		__HAL_RCC_GPIOF_CLK_ENABLE

// _mg_ For testing ADDR wiring
#define SET_ADDR_MODE_IN	ADDR_PORT->MODER = 0x00000000;
#define SET_ADDR_MODE_OUT	ADDR_PORT->MODER = 0x55555555;

// define for alternate DATA Pins on port E
//#define DATA_ALTERNATE
#ifdef DATA_ALTERNATE
// 6507 data bus is upper byte DATA on port E
#define DATA_PORT	GPIOE
#define DATA_PINS	GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_11 | \
					GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15
#define DATA_IN_SHIFT(dd)	(dd>>8)
#define DATA_OUT_SHIFT(dd)	(dd<<8)
#define SET_DATA_MODE_IN	DATA_PORT->MODER = 0x00000000;
#define SET_DATA_MODE_OUT	DATA_PORT->MODER = 0x55550000;
#define DATA_GPIO_CLK_ENABLE		__HAL_RCC_GPIOE_CLK_ENABLE

#else
// 6507 data bus is lower byte DATA on port D
#define DATA_PORT	GPIOD
#define DATA_PINS	GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | \
					GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7
#define DATA_IN_SHIFT(dd)	(dd)
#define DATA_OUT_SHIFT(dd)	(dd)
#define SET_DATA_MODE_IN	DATA_PORT->MODER = 0x00000000;
#define SET_DATA_MODE_OUT	DATA_PORT->MODER = 0x00005555;

#define DATA_GPIO_CLK_ENABLE		__HAL_RCC_GPIOD_CLK_ENABLE

#endif	// #ifdef DATA_ALTERNATE

/*
 * Control pins.
 * GPIO Port G
 * PG0	PAL60 enable when low
 * PG1  PAL   enable when low
 * PG2  DIP Switch posistion 1 - Bus Dumper mode when low.
 * PG3  DIP Switch posisiont 2 - Future use.
 * PG4  Atari +5v power on - for debug use.
 */
#define CONTROL_PORT	GPIOG
#define CONTROL_PINS	GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4

#define CONTROL_GPIO_CLK_ENABLE		__HAL_RCC_GPIOG_CLK_ENABLE

#define ADDR_IN 	ADDR_PORT->IDR		// 6507 address bus
#define ADDR_OUT 	ADDR_PORT->ODR		// 6507 address bus - for testing address bus wiring.
#define DATA_IN 	DATA_PORT->IDR		// 6507 data bus read
#define DATA_IN_BYTE (DATA_IN & 0xFF)	// for cartPlus code.
#define DATA_OUT 	DATA_PORT->ODR		// 6507 data bus write
#define CONTROL_IN	CONTROL_PORT->IDR	// For runtime configuration control.
#define PAL60_CONTROL_MASK	GPIO_PIN_0
#define PAL_CONTROL_MASK	GPIO_PIN_1

#define DIP_SW1_Pin			GPIO_PIN_2
#define DUMP_CONTROL_MASK	DIP_SW1_Pin
#define DIP_SW2_Pin 		GPIO_PIN_3
#define A2600_PWR_Pin		GPIO_PIN_4
#define A2600_PWR_MASK		A2600_PWR_Pin


#define SET_6502_NOP()		DATA_OUT=DATA_OUT_SHIFT(0x00EA)

#define HAVE_UI_BTN
#define UI_BTN_PORT	GPIOC
#define UI_BTN_IN	GPIOC->IDR
#define UI_BTN_PIN	GPIO_PIN_13
#define UI_BTN_GPIO_CLK_ENABLE	__HAL_RCC_GPIOC_CLK_ENABLE

#endif	// #elif defined (STM32F767xx)


__attribute__((always_inline)) static inline uint32_t is_pal60_mode(void)
{
	// 0 (GND) on IO pin is mode active
	// return 1 if jumperd to GND
	return ((CONTROL_IN & PAL60_CONTROL_MASK)==PAL60_CONTROL_MASK)? 0:1;
}
__attribute__((always_inline)) static inline uint32_t is_pal_mode(void)
{
	// 0 (GND) on IO pin is mode active
	// return 1 if jumperd to GND
	return ((CONTROL_IN & PAL_CONTROL_MASK)==PAL_CONTROL_MASK)? 0:1;
}

__attribute__((always_inline)) static inline uint32_t is_dumper_mode(void)
{
	// 0 (GND) on DIP switch is mode active.
	// return 1 if switch is ON.
#ifdef BUS_DUMPER
	return ((CONTROL_IN & DUMP_CONTROL_MASK)==DUMP_CONTROL_MASK)? 0:1;
#else
	return 0;	// no DIP switch hardware.
#endif
}

__attribute__((always_inline)) static inline uint32_t is_atari_power_on(void)
{
#ifdef	ATARI_POWER_HW
	// 1 is power on.
	return ((CONTROL_IN & A2600_PWR_MASK)==A2600_PWR_MASK)? 1:0;
#else
	return 1;	// act like power is aways on.
#endif
}

__attribute__((always_inline)) static inline void wait_atari_power_on(void)
{
	uint32_t debounce = 5;
	while(debounce)
	{
		debounce-=is_atari_power_on();
	}
}

extern volatile int blue_btn_pushed;
extern volatile int blue_btn_debounce;
__attribute__((always_inline)) static inline uint32_t ui_btn_pushed(void)
{
#ifdef HAVE_UI_BTN
	// Check is btn pressed
	// Nucleo blue button is active high.
	// return 1 and set flag if button is pushed.
	blue_btn_pushed = ((UI_BTN_IN & UI_BTN_PIN)==UI_BTN_PIN)? 1:0;
	return (blue_btn_pushed);
#else
	return 0;
#endif
}

// From cartPlus: Method to escape a cartridge.

#define SWCHA          (0x280)	// Port A: Joystick inputs
#define SWCHB          (0x282)	// Port B; console switches (read only)
/*
SWCHA:
Normal 4-Direction / 1-Button Joysticks
Configure Port A as input (SWACNT=00h). Joystick data can be then read from SWCHA and INPT4-5 as follows:
  Pin  PlayerP0  PlayerP1  Expl.
  1    SWCHA.4   SWCHA.0   Up     (0=Moved, 1=Not moved)
  2    SWCHA.5   SWCHA.1   Down   ("")
  3    SWCHA.6   SWCHA.2   Left   ("")
  4    SWCHA.7   SWCHA.3   Right  ("")
  6    INPT4.7   INPT5.7   Button (0=Pressed, 1=Not pressed)
Note: Connect all direction switches and push buttons to GND (Pin 8).

Info from: https://problemkaputt.de/2k6specs.htm
*/

/*
SWCHB:
Accessing Console Switches
Configure Port B as input (SWBCNT=00h). Switch/Button data can be then read from SWCHB as follows:
  Bit        Expl.
  SWCHB.0    Reset Switch          (0=Pressed)
  SWCHB.1    Select Switch         (0=Pressed)
  SWCHB.2    Not used
  SWCHB.3    Color Switch          (0=B/W, 1=Color) (Always 0 for SECAM)
  SWCHB.4-5  Not used
  SWCHB.6    P0 Difficulty Switch  (0=Beginner (B), 1=Advanced (A))
  SWCHB.7    P1 Difficulty Switch  (0=Beginner (B), 1=Advanced (A))
Note: Older consoles used switches instead of buttons for reset/select.
SECAM consoles do not include a color switch (Bit 3 is grounded).

Info from: https://problemkaputt.de/2k6specs.htm
*/

// Define method to escape from cartridge emulation loop:

// define USE_MARKS_ESCAPE	to use my method to escape, less cumbersome than holding joystick.
#define USE_MARKS_ESCAPE

#ifdef USE_MARKS_ESCAPE
#define blue_btn_pressed() (((UI_BTN_IN & UI_BTN_PIN)==UI_BTN_PIN))
#define blue_btn_escape_now() if(blue_btn_pressed()) break;

/// Look for 6507 CPU reading from 6532 PIA port A or B - joy stick or console switches
/// and Nucleo blue button held
#define check_if_escape_from_cart()										\
	if((addr&0x1ffc) == SWCHA && blue_btn_pressed())						\
	{	/* _mg_ if nucleo blue button is pressed  escape from game*/	\
		while (ADDR_IN == addr) ;/*{ data_prev = data; data = DATA_IN; }*/	\
			/* _mg_ joy/console read is done & blue BTN pressed.*/		\
			break;	/* out of cart emulation loop */					\
	} 																	\

#else	// USE CartPlus method (more cumbersome and more CPU cycles)
// Look for 6507 CPU reading from 6532 PIA ports;
// Player 0 (left) joystick held RIGHT and Console RESET switch pressed.
#define check_if_escape_from_cart()										\
	/* must declare local var: bool esc_status = false; */				\
	if(addr == SWCHB)													\
	{																	\
		while (ADDR_IN == addr) { data_prev = data; data = DATA_IN; }	\
		if( !((DATA_IN_SHIFT(data_prev) ) & 0x1) && esc_status)			\
		/* _mg_ console reset switch is pressed & joystick was right.*/	\
			break;														\
	}else if(addr == SWCHA)												\
	{																	\
		while (ADDR_IN == addr) { data_prev = data; data = DATA_IN; }	\
		/* _mg_ check if Player P0 (left) joystick is pushed right.*/	\
		esc_status = !((DATA_IN_SHIFT(data_prev)) & 0x80);				\
	}																	\

#endif	// #ifdef USE_MARKS_ESCAPE

// used in conjuction with check_if_escape_from_cart() to exit back to UnoCart firmware cart.
void exit_cartridge(uint16_t addr);


#endif // CARTRIDGE_IO_H
