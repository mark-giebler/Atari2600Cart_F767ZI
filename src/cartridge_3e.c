#include <stdbool.h>

#include "fatfs.h"
#include "string.h"

#include "cartridge_3e.h"
#include "flash.h"
#include "cartridge_firmware.h"
#include "cartridge_firmware_plus.h"
#include "ram_storage.h"

#define MAX_CCM_BANK (CCM_2K_BANKS - 1)

/**
 *
 * Todo: _mg_ Move the cartridge_layout structure to ram_storage.h (copy other one in 3f emulator)
 */
typedef struct {
	uint8_t max_bank;		// index to last 2K bank of the cart image
	uint8_t max_ccm_bank;	// index to last possible 2K bank in RAM buffer for cart image.

	uint8_t *flash_base;	// base address of cart image in MCU FLASH.
} cartridge_layout;

/**
 * @brief Setup the cartridge image for emulation.
 *
 * Copies cart image buffer to CCM_RAM area.
 *
 * If cartridge image too large to fit completely in CCM_RAM, then burn it to FLASH memory.
 *
 * @param filename - cartridge image filename. Only used if image needs to be put in FLASH.
 * @param image_size - image size in units of bytes.
 * @param buffer - pointer to start of RAM buffer that contains the cartridge binary image already loaded.
 * @param layout - pointer to @ref cartridge_layout structure.
 */
static bool setup_cartridge_image(const char* filename, uint32_t image_size, uint8_t *buffer, cartridge_layout* layout) {
	if (image_size == 0) return false;

	image_size = image_size > 512 * 1024 ? 512 * 1024 : image_size;

	uint32_t banks = image_size / 2048;
	if (image_size % 2048) banks++;

	layout->max_bank = (uint8_t)(banks - 1);
	layout->max_ccm_bank = layout->max_bank > MAX_CCM_BANK ? MAX_CCM_BANK : layout->max_bank;

    if (layout->max_bank == layout->max_ccm_bank) {
        memcpy(CCM_RAM, buffer, image_size);
        return true;
    }

    // If cartridge image too large to fit in RAM, then burn it to FLASH memory.
    // Todo: _mg_ Remove writing cartridge to FLASH, why do this?
    // _mg_ Are there actual carts larger than 128K?  I doubt it.
    // _mg_ Also this code was broken using BUFFER_SIZE as units bytes not KB.  Now fixed.
    uint32_t flash_image_size = image_size - CCM_SIZE;
    if (flash_image_size > available_flash()) return false;

    flash_context ctx;

    if (!prepare_flash(flash_image_size, &ctx)) return false;

    layout->flash_base = ctx.base;

    uint32_t flash_from_buffer =
        image_size > BUFFER_SIZE_BYTES ? BUFFER_SIZE_BYTES - CCM_SIZE : image_size - CCM_SIZE;

    if (!write_flash(flash_from_buffer, buffer + CCM_SIZE, &ctx)) return false;

    if (image_size <= BUFFER_SIZE_BYTES) {
        memcpy(CCM_RAM, buffer, CCM_SIZE);
        return true;
    }

    uint32_t flash_from_file = image_size - BUFFER_SIZE_BYTES;
	FATFS fs;
	FIL fil;
	UINT bytes_read;

	if (f_mount(&fs, "", 1) != FR_OK) goto fail_unmount;
	if (f_open(&fil, filename, FA_READ) != FR_OK) goto fail_close;

    if (f_lseek(&fil, BUFFER_SIZE_BYTES) != FR_OK) goto fail_close;

    uint32_t bytes_written_to_flash = 0;

    while (bytes_written_to_flash < flash_from_file) {
        if (f_read(&fil, CCM_RAM, CCM_SIZE, &bytes_read) != FR_OK) goto fail_close;

        bytes_written_to_flash += bytes_read;
        if (bytes_read < CCM_SIZE && bytes_written_to_flash < flash_from_file) goto fail_close;

        if (!write_flash(bytes_read, CCM_RAM, &ctx)) goto fail_close;
    }

    f_close(&fil);
	f_mount(0, "", 1);

    memcpy(CCM_RAM, buffer, CCM_SIZE);
    return true;

	fail_close:
		f_close(&fil);
	fail_unmount:
		f_mount(0, "", 1);

	return false;
}

/**
 * @brief Runs cartridge from CCM_RAM.
 * Writes image to FLASH memory for some reason. Why?
 *
 */
/** 3E (3F + RAM) Bankswitching
 * ------------------------------
 * This scheme supports up to 512k ROM and 256K RAM.
 * However here we only support up to MAX_CART_ROM_SIZE and MAX_CART_RAM_SIZE
 *
 * The text below is the best description of the mapping scheme I could find,
 * quoted from http://blog.kevtris.org/blogfiles/Atari%202600%20Mappers.txt

This works similar to 3F (Tigervision) above, except RAM has been added.  The range of
addresses has been restricted, too.  Only 3E and 3F can be written to now.

1000-17FF - this bank is selectable
1800-1FFF - this bank is the last 2K of the ROM

To select a particular 2K ROM bank, its number is poked into address 3F.  Because there's
8 bits, there's enough for 256 2K banks, or a maximum of 512K of ROM.

Writing to 3E, however, is what's new.  Writing here selects a 1K RAM bank into
1000-17FF.  The example (Boulderdash) uses 16K of RAM, however there's theoretically
enough space for 256K of RAM.  When RAM is selected, 1000-13FF is the read port while
1400-17FF is the write port.

 * _mg_ Emulates the cartridge out of CCM_RAM.
 * _mg_ Writes image to FLASH memory for some reason. Why?

 * @param filename - cartridge image filename. Only used if image needs to be put in FLASH.
 * @param image_size - image size in units of Bytes.
 * @param buffer - pointer to start of RAM buffer that contains the cartridge binary image already loaded.
 * @param ram_banks - number of emulated 1K banks RAM required.
*/

void emulate_3e_cartridge(const char* filename, uint32_t image_size, uint8_t* buffer, uint8_t ram_banks)
{
	cartridge_layout layout;

	if (!setup_cartridge_image(filename, image_size, buffer, &layout)) return;

	uint16_t addr, addr_prev = 0, addr_prev2 = 0, data = 0, data_prev = 0;
	uint8_t *bankPtr = CCM_RAM;
	uint8_t *fixedPtr;
    bool mode_ram = false;

	if (layout.max_bank <= layout.max_ccm_bank) fixedPtr = CCM_RAM + layout.max_bank * 2048;
	else fixedPtr = layout.flash_base + (layout.max_bank - layout.max_ccm_bank - 1) * 2048;

	if (!reboot_into_cartridge()) return;
	__disable_irq();

	while (1)
	{
		while (((addr = ADDR_IN) != addr_prev) || (addr != addr_prev2))
		{
			addr_prev2 = addr_prev;
			addr_prev = addr;
		}

		// got a stable address
		if (addr == 0x3f) {
			while (ADDR_IN == addr) { data_prev = data; data = DATA_IN; }
			data = DATA_IN_SHIFT(data_prev);

			uint8_t bank = data % (layout.max_bank + 1);
            mode_ram = false;

			if (bank <= layout.max_ccm_bank) bankPtr = CCM_RAM + bank * 2048;
			else bankPtr = layout.flash_base + (bank - layout.max_ccm_bank - 1) * 2048;
		}
        else if (addr == 0x3e) {
			while (ADDR_IN == addr) { data_prev = data; data = DATA_IN; }
			data = DATA_IN_SHIFT(data_prev);

            uint8_t bank = data % ram_banks;
            mode_ram = true;

            bankPtr = buffer + bank * 1024;
        }
		else if (addr & 0x1000) {
			if (addr & 0x800) {
				data = fixedPtr[addr & 0x7ff];

                DATA_OUT = DATA_OUT_SHIFT(((uint16_t)data));
	    		SET_DATA_MODE_OUT

                while (ADDR_IN == addr) ;
                SET_DATA_MODE_IN
			} else if (mode_ram && addr & 0x400) {
                while (ADDR_IN == addr) { data_prev = data; data = DATA_IN; }
                data = DATA_IN_SHIFT(data_prev);

                bankPtr[addr & 0x3ff] = data;
            } else {
                data = bankPtr[addr & 0x7ff];

                DATA_OUT = DATA_OUT_SHIFT(((uint16_t)data));
                SET_DATA_MODE_OUT

                while (ADDR_IN == addr) ;
                SET_DATA_MODE_IN
            }
		}
	}
	__enable_irq();
}
