/**
 * @file ram_storage.h
 *
 *  Created on: Mar 19, 2022
 *      @author: mgiebler
 *
 * Declare location and size of RAM used for storing cartridge images.
 * Putting in a header file allows customization based on MCU target.
 */

#ifndef SRC_RAM_STORAGE_H_
#define SRC_RAM_STORAGE_H_

#ifdef STM32F767xx
// The linker ld file creates these __CCMRAM_* variables
//  all three .__CCMRAM_start variables are at the same address.
//  used in place of a union to prevent compiler warnings about punning due to casting char* to other types in bus_dumper()
extern uint8_t	__CCMRAM_start;
// We set the array size to 4 which is arbitrary, but keeps compiler happy in bus_dumper code. (no index exceeds array warning)
extern uint16_t	__CCMRAM_start_uint16_t[4];
extern uint32_t __CCMRAM_start_uint32_t[4];
extern uint8_t	__CCMRAM_size;	// do not use.
extern uint8_t  __CCMRAM_end;

// define memory location for the cartridge data storage area,
#define CCM_RAM			(&__CCMRAM_start)
// shorter names to make them look more familiar.
#define CCM_RAM_u16		__CCMRAM_start_uint16_t
#define CCM_RAM_u32		__CCMRAM_start_uint32_t

#define CCM_SIZE		((uint32_t)(&__CCMRAM_end - &__CCMRAM_start))	// Size in bytes of available CCMRAM area.
#define CCM_2K_BANKS	(CCM_SIZE/2048ul)				// Size in number of 2K banks
#define CCM_SIZE_KB		(CCM_SIZE/1024ul)				// Size in KiB

// allow customization based on MCU
#define MAX_CART_ROM_SIZE	64	// in kilobytes, historical to be removed.
#define MAX_CART_RAM_SIZE	120	// in kilobytes, historical to be removed.  _mg_ I enlarged to 120K, was 32K
#define BUFFER_SIZE			96  // kilobytes Todo: _mg_ BUFFER_SIZE used inconsistantly as KB or Bytes, need to fix.

#define BUFFER_SIZE_KB 		96
#define BUFFER_SIZE_BYTES	(BUFFER_SIZE_KB * 1024)

// next group of externs are created by the linker script.
extern uint32_t _Max_Buffer_Size;
extern uint32_t g_pfnVectors;
extern uint32_t _END_OF_FLASH;
#define max_buffer_size()		((uint32_t)(&_Max_Buffer_Size))
#define flash_code_size()		((uint32_t)(&_END_OF_FLASH - &g_pfnVectors))

#elif defined (STM32F4XX)

#define CCM_RAM ((uint8_t*)0x10000000)
#define CCM_SIZE (64 * 1024)

#define CCM_2K_BANKS 32

// allow customization based on MCU
#define MAX_CART_ROM_SIZE	64	// in kilobytes, historical to be removed
#define MAX_CART_RAM_SIZE	32	// in kilobytes, historical to be removed
#define BUFFER_SIZE			96  // kilobytes
#define BUFFER_SIZE_KB 		96

#endif	// #ifdef STM32F767xx


#endif /* SRC_RAM_STORAGE_H_ */
