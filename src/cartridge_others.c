/*
 * cartridge_others.c
 *
 *  Created on: Dec 29, 2023
 *      Author: mgiebler
 *
 * Additional emulators from cartPlus code.
 *
 *
 * From CartPlus project:
 *  Created on: 03.11.2019
 *      Author: stubig
 *
 */
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "global.h"
#include "cartridge_io.h"
#include "cartridge_firmware_plus.h"
#include "ram_storage.h"
#include "cartridge_others.h"


/*************************************************************************
 * Cartridge Emulation from cartPlus
 *************************************************************************/
#ifdef CARTPLUS_ENABLE
// todo: _mg_ change this to function that copies buffer to ccm_ram like other unocart setup_carts...?
#define setup_cartridge_image() \
		if (cart_size_bytes > 0x010000) return; \
		uint8_t* cart_rom = buffer;

#define setup_cartridge_image_with_ram() \
		if (cart_size_bytes > (0x010000-2048ul)) return; \
		uint8_t* cart_rom = buffer; \
		uint8_t* cart_ram = buffer + cart_size_bytes + (((~cart_size_bytes & 0x03) + 1) & 0x03);

/**
 * @brief Exit an emulated cartridge and back to the UnoCart firmware cartridge.
 * Used in conjunction with the cartridge emulation escape method @ref check_if_escape_from_cart().
 *
 * From: CartPlus code.
 */
void exit_cartridge(uint16_t addr)
{
	DATA_OUT = DATA_OUT_SHIFT(0xEA);		// (NOP) or data for SWCHA/SWCHB
	SET_DATA_MODE_OUT;
	while (ADDR_IN == addr);

	addr = ADDR_IN;
	DATA_OUT = DATA_OUT_SHIFT(0x00);		// (BRK) Fetch next exec address from $FFFE
	while (ADDR_IN == addr);

	emulate_firmware_cartridge(1);			// call cartPlus version in cartridge_firmware_plus.c
}


#include "fatfs.h"
#define RAM_4K_BANKS (BUFFER_SIZE_KB / 4 )				//    24
#define CCM_4K_BANKS (CCM_SIZE_KB / 4)					//    16
#define FLASH_BANKS (64 - RAM_4K_BANKS - CCM_4K_BANKS)	//    24
#define CCM_IMAGE_OFFSET (RAM_4K_BANKS * 4096)			// 98304
#define CCM_IMAGE_SIZE (CCM_4K_BANKS * 4096)			// 26864

#define FLASH_IMAGE_SIZE (FLASH_BANKS * 4096)
#define FLASH_IMAGE_OFFSET ((RAM_4K_BANKS + CCM_4K_BANKS) * 4096)	// _mg_ This seems sketchy

/**
 * CartPlus function.
 * Setup for these cart types:
 * 		base_type_SB
 * 		base_type_DF
 * 		base_type_DFSC
 * 		base_type_BF
 * 		base_type_BFSC
 *
 * NOTE: _mg_ The cartridge_layout used here only works for the one defined in cartridge_other.h and cp_cartridge_setup.h
 *
 */
bool plus_setup_cartridge_image(const char* filename, uint32_t image_size, uint8_t* buffer, cartridge_layout* layout, MENU_ENTRY *d, enum cart_base_type banking_type) {

    switch(banking_type){
        case(base_type_SB):
            if (image_size > 256*1024) return false;
        break;
        case(base_type_DF):
        case(base_type_DFSC):
            if (image_size != 128*1024) return false;
        break;
        case(base_type_BF):
        case(base_type_BFSC):
            if (image_size != 256*1024) return false;
        break;
        // these base types will never appear here, it is just to stop
        // the compiler from nagging!
        case base_type_None:
        case base_type_2K:
        case base_type_4K:
        case base_type_4KSC:
        case base_type_F8:
        case base_type_F6:
        case base_type_F4:
        case base_type_UA:
        case base_type_FE:
        case base_type_3F:
        case base_type_3E:
        case base_type_E0:
        case base_type_0840:
        case base_type_CV:
        case base_type_EF:
        case base_type_F0:
        case base_type_FA:
        case base_type_E7:
        case base_type_DPC:
        case base_type_AR:
        case base_type_PP:
        case base_type_3EPlus:
        case base_type_DPCplus:
        case base_type_ACE:
        case base_type_Load_Failed:
        default:
        	return false;
    }

    // _mg_ Added a runtime sanity check to prevent array overrun
    if(RAM_4K_BANKS > (sizeof(layout->banks)/sizeof(uint8_t*)))
    	Error_Handler_Blink(8ul);

    uint8_t banks = (uint8_t)(image_size / 4096);
    for (uint8_t i = 0; i < RAM_4K_BANKS && i < banks; i++) layout->banks[i] = buffer + i * 4096;

    if(banks > RAM_4K_BANKS){
    	uint32_t bytes_read;
    	uint8_t ccm_banks = (uint8_t) (banks - RAM_4K_BANKS);
    	uint32_t ccm_size = ccm_banks * 4096U;
    	if(d->type == Cart_File ){
#ifdef USE_WIFI
    		bytes_read = esp8266_PlusStore_API_file_request( CCM_RAM, (char*) filename, CCM_IMAGE_OFFSET, ccm_size );
#else
    		bytes_read = 0; // d->type == Cart_File and no WiFi ???
#endif
    	}
    	else if(d->type == SD_Cart_File ){
#ifdef USE_SD_CARD
    		bytes_read = sd_card_file_request( CCM_RAM, (char*) filename, CCM_IMAGE_OFFSET, ccm_size );
#else
    		bytes_read = 0; // d->type == SD_Cart_File and no SD-Card ???
#endif

    	}
 // _mg_    	else
// _mg_   		bytes_read = flash_file_request( CCM_RAM, d->flash_base_address, CCM_IMAGE_OFFSET, ccm_size );

        if (bytes_read != ccm_size)	return false;
        for (uint8_t i = 0; i < ccm_banks; i++) layout->banks[RAM_4K_BANKS + i] = CCM_RAM + i * 4096;
    }

    if(banks > (RAM_4K_BANKS + CCM_4K_BANKS) ){
        uint32_t flash_part_address;
    	if(d->type == Cart_File ){
#ifdef USE_WIFI
    		flash_part_address = flash_download((char*)filename, FLASH_IMAGE_SIZE, FLASH_IMAGE_OFFSET, true);
#else
    		flash_part_address = 0; // d->type == Cart_File and no WiFi ???
#endif
    	}else if(d->type == SD_Cart_File ){
#ifdef USE_SD_CARD
    		flash_part_address = 0; // todo read from SD-Card to flash
#else
    		flash_part_address = 0; // d->type == SD_Cart_File and no SD-Card ???
#endif
    	}else{
    		flash_part_address = d->flash_base_address + FLASH_IMAGE_OFFSET;
    	}

    	if(flash_part_address == 0)
    		return false;

    	// _mg_ Added a runtime sanity check to prevent array overrun
        if((RAM_4K_BANKS + CCM_4K_BANKS + FLASH_BANKS) > (sizeof(layout->banks)/sizeof(uint8_t*)))
        	Error_Handler_Blink(8ul);

        for (uint8_t i = 0; i < FLASH_BANKS; i++) layout->banks[RAM_4K_BANKS + CCM_4K_BANKS + i] = (uint8_t *)(flash_part_address + i * 4096U);
    }

	return true;
}

/* 'Standard' Bankswitching
 * ------------------------
 * Used by 2K, 4K, 4KSC, F8(8k), F6(16k), F4(32k), EF(64k)
 * and F8SC(8k), F6SC(16k), F4SC(32k), EFSC(64k)
 *
 * SC variants have 128 bytes of RAM:
 * RAM read port is $1080 - $10FF, write port is $1000 - $107F.
 *
 * Example cartridges:
 * _mg_ F8: Atari Androman on the Moon. (Not SC, LowBS $1FF8, highBS $1FF9)
 * _mg_ F8: Atari Ateroids (no copyright) (Not SC, LowBS $1FF8, highBS $1FF9)
 * _mg_ F8: Atari Centipede (Not SC, LowBS $1FF8, highBS $1FF9)
 * _mg_ F8: Activision H.E.R.O.
 * _mg_ F6: Atari Radar Lock
 * _mg_ F4: Fantastic Voyage    (Not SC, LowBS  0x2000, highBS 0x0000)
 * _mg_ 2k: Home Run - Baseball (Is  SC, LowBS $1fff, highBS 0x0000) Todo: Doesn't run correctly hangs @ $1fff
 */
void emulate_standard_cartridge(int header_length, bool withPlusFunctions, uint16_t lowBS, uint16_t highBS, int isSC)
{
	setup_cartridge_image_with_ram();

	register uint16_t addr, addr_prev = 0, data = 0, data_prev = 0;
	unsigned char *bankPtr = &cart_rom[0];

	setup_plus_rom_functions();

	if (!reboot_into_cartridge()){ __enable_irq(); return;}
//	__disable_irq();	// Disable interrupts

	while (1)
	{
		while ((addr = ADDR_IN) != addr_prev)
			addr_prev = addr;
		// got a stable address
		if (addr & 0x1000)
		{ // A12 high
#ifdef USE_WIFI
			if(withPlusFunctions && addr > 0x1fef && addr < 0x1ff4){
				if(addr == 0x1ff2 ){// read from receive buffer
					DATA_OUT = DATA_OUT_SHIFT(((uint16_t)receive_buffer[receive_buffer_read_pointer]));
					SET_DATA_MODE_OUT
					// if there is more data on the receive_buffer
					if(receive_buffer_read_pointer != receive_buffer_write_pointer )
						receive_buffer_read_pointer++;
					// wait for address bus to change
					while (ADDR_IN == addr){}
					SET_DATA_MODE_IN
				}else if(addr == 0x1ff1){ // write to send Buffer and start Request !!
					while (ADDR_IN == addr) { data_prev = data; data = DATA_IN; }
					if(huart_state == No_Transmission)
						huart_state = Send_Start;
					out_buffer[out_buffer_write_pointer] = (uint8_t)DATA_IN_SHIFT(data_prev );
				}else if(addr == 0x1ff3){ // read receive Buffer length
					DATA_OUT = DATA_OUT_SHIFT(((uint16_t)(receive_buffer_write_pointer - receive_buffer_read_pointer)));
					SET_DATA_MODE_OUT
					// wait for address bus to change
					while (ADDR_IN == addr){}
					SET_DATA_MODE_IN
				}else{ // if(addr == 0x1ff0){ // write to send Buffer
					while (ADDR_IN == addr) { data_prev = data; data = DATA_IN; }
					out_buffer[out_buffer_write_pointer++] = (uint8_t) DATA_IN_SHIFT(data_prev );
				}
			}else
#endif	// #ifdef USE_WIFI
			{
				if (addr >= lowBS && addr <= highBS)	// bank-switch
					bankPtr = &cart_rom[(addr-lowBS)*4*1024];

				if (isSC && (addr & 0x1F00) == 0x1000)
				{	// SC RAM access. 256 byte RAM
					if (addr & 0x0080)
					{	// a read from cartridge ram
						DATA_OUT = DATA_OUT_SHIFT(((uint16_t)cart_ram[addr&0x7F]));
						SET_DATA_MODE_OUT
						// wait for address bus to change
						while (ADDR_IN == addr) ;
						SET_DATA_MODE_IN
					}
					else
					{	// a write to cartridge ram
						// read last data on the bus before the address lines change
						while (ADDR_IN == addr) { data_prev = data; data = DATA_IN; }
						cart_ram[addr&0x7F] = (uint8_t) DATA_IN_SHIFT(data_prev );
					}
				}
				else
				{	// normal rom access
					DATA_OUT = DATA_OUT_SHIFT(((uint16_t)bankPtr[addr&0xFFF]));
					SET_DATA_MODE_OUT
					// wait for address bus to change
					while (ADDR_IN == addr);
					SET_DATA_MODE_IN
				}

			}
		}else{
			// _mg_ Atari Internal memory space access.
			check_if_escape_from_cart();
		}
	}

	exit_cartridge(addr);
}

/* UA
 *
 *
 */
void emulate_UA_cartridge(uint32_t cart_size_bytes, uint8_t *buffer)
{
	setup_cartridge_image();

	uint16_t addr, addr_prev = 0, data = 0 , data_prev = 0;
	unsigned char *bankPtr = &cart_rom[0];
	(void) data_prev;
	(void) data;

	if (!reboot_into_cartridge()){ __enable_irq(); return;}
//	__disable_irq();	// Disable interrupts

	while (1)
	{
		while ((addr = ADDR_IN) != addr_prev)
			addr_prev = addr;
		// got a stable address
		if (addr & 0x1000)
		{ // A12 high
			// normal rom access
			DATA_OUT = DATA_OUT_SHIFT(((uint16_t)bankPtr[addr&0xFFF]));
			SET_DATA_MODE_OUT;
			// wait for address bus to change
			while (ADDR_IN == addr)
				;
			SET_DATA_MODE_IN;
		}else{
			if (addr == 0x220 ){	// bank-switch
				bankPtr = &cart_rom[0];
			}else if(addr == 0x240){
				bankPtr = &cart_rom[4*1024];
			}else check_if_escape_from_cart();
		}
	}
	exit_cartridge(addr);
}


/* FA (CBS RAM plus) Bankswitching	(This is in main.c)
 * -------------------------------
 * Similar to the FxSC, but with 3 ROM banks for a total of 12K
 * plus 256 bytes of RAM:
 * RAM read port is $1100 - $11FF, write port is $1000 - $10FF.
 */
//void emulate_FA_cartridge(int header_length, bool withPlusFunctions)
//{
	// Todo: _mg_ Bring in cartPlus version ???   AND my emulation escape method. check_if_escape_from_cart()
//}



// Todo: _mg_ Bring in cartPlus version AND my emulation escape method. check_if_escape_from_cart()


void emulate_ar_cartridge(const char* filename, uint32_t image_size, uint8_t* buffer, uint8_t tv_mode, MENU_ENTRY* menu_entry_d)
{
	// Todo: _mg_ Bring in cartPlus version AND my emulation escape method. check_if_escape_from_cart()
}



void emulate_SB_cartridge( const char* filename, uint32_t image_size, uint8_t* buffer, MENU_ENTRY *d  )
{
	// _mg_ The cartridge_layout used here only works for the one defined in cp_cartridge_setup.h
	cartridge_layout * layout = (cartridge_layout *) malloc( sizeof(  cartridge_layout ));

    if (!plus_setup_cartridge_image(filename, image_size, buffer, layout, d, base_type_SB)) return;

    uint8_t banks = (uint8_t)(( image_size / 4096 ) - 1);
    uint8_t *bank = layout->banks[banks];	// Start emulation in last bank

	uint16_t addr, addr_prev = 0, data_prev = 0, data = 0;
	//bool esc_status = false;
	(void)data_prev; (void)data;
	if (!reboot_into_cartridge()){ __enable_irq(); return;}
	__disable_irq();	// Disable interrupts

	while (1)
	{
		while ((addr = ADDR_IN) != addr_prev)
			addr_prev = addr;
		// got a stable address
		if (addr & 0x1000)
		{ // A12 high
			DATA_OUT = DATA_OUT_SHIFT(((uint16_t)bank[addr & 0xFFF]));
			SET_DATA_MODE_OUT
			// wait for address bus to change
			while (ADDR_IN == addr) ;
			SET_DATA_MODE_IN
		}else if (addr & 0x0800 ){
			bank = layout->banks[addr & banks];
			// wait for address bus to change
			while (ADDR_IN == addr) ;
	    }else check_if_escape_from_cart();
	}
	exit_cartridge(addr);

	free(layout);
}

#endif	// CARTPLUS_ENABLE


