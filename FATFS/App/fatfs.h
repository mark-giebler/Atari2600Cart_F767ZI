/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file   fatfs.h
  * @brief  Header for fatfs applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __fatfs_H
#define __fatfs_H
#ifdef __cplusplus
 extern "C" {
#endif

#include "ff.h"
#include "ff_gen_drv.h"
#include "user_diskio.h" /* defines USER_Driver as external */

/* USER CODE BEGIN Includes */
#include "global.h"
/* USER CODE END Includes */

extern uint8_t retUSER;		/* Return value for USER */
extern char USERPath[4];	/* USER logical drive path */
extern FATFS USERFatFS;		/* File system object for USER logical drive */
extern FIL USERFile;		/* File object for USER */

void MX_FATFS_Init(void);

/* USER CODE BEGIN Prototypes */
bool sd_card_file_list( char *path, MENU_ENTRY **dst, int *num_menu_entries );
uint32_t sd_card_file_request(uint8_t *ext_buffer, char *path, uint32_t start_pos, uint32_t length );
bool sd_card_find_file( char *path, char *pattern, MENU_ENTRY **dst, int *num_menu_entries);


/* USER CODE END Prototypes */
#ifdef __cplusplus
}
#endif
#endif /*__fatfs_H */
