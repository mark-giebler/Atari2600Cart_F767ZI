<!-- URL: https://gitlab.com/mark-giebler/Atari2600Cart_F767ZI -->

# Atari 2600 UnoCart

My port of the Atari 2600 multi-cartridge project **UnoCart-2600**
to an ST Microelectronics **STM32F767ZI Nucleo board**.

2022-03-15

Mark Giebler

## Update:

2024-01-02

Finished initial merged in of a large part of the CartPlus code from [here](https://gitlab.com/mark-giebler/atari-2600-pluscart)
to get better font and longer filename display support. Its mostly working.

- I fixed the issue where names longer than 32 character where showing as the mostly useless mangled FAT short name.
- I fixed the potential issue where an interrupt could be serviced in between the end of reboot_into_cartridge() and its return.

BTW: Newer upstream CartPlus code has moved to [here.](https://github.com/Al-Nafuur/United-Carts-of-Atari)
I may have to merge in more changes...


## My Hardware Build

![Image: IsoView](./images/myUnoCart-2600.jpg)

My Sears Tele-Games branded Atari 2600 "heavy sixer" with my build of the UnoCart-2600.

The 3D printed [Atari cartridge](https://www.thingiverse.com/thing:23216)
 and STM32 Nucleo-144 [board enclosure](https://www.thingiverse.com/thing:2421873)
parts are from Thingiverse with slight tweaks I made using the Tinkercad web site.

- Tweaked the Atari Cartridge bottom half to add a gap to allow the ribbon cable to exit the cartridge cleanly.
My Tinkercad mod for the [cartridge bottom is here.](https://www.tinkercad.com/things/jppXV0jvSJD-ataricartbott9mmrev3)
- Tweaked the Atari Cartirdge top half to add clearance recess for the 24 pin DIP ribbon cable header.
My Tinkercad mod for the [catridge top is here.](https://www.tinkercad.com/things/lk6sjLUuG35-ataricarttop11mmrev3branded)
- Tweaked the Atari Cartridge's slot opening tabs to be more robust.
My Tinkercad mod for the [cartridge tabs is here.](https://www.tinkercad.com/things/0J3tu3DaZ1n-ataricartholderfixrev2)
- Tweaked the STM32 Nucleo-144 board enclosure to eliminate an interference with the bottom edge of the Ethernet connector.
[My Tinkercad mod is here.](https://www.tinkercad.com/things/fyMg5MYtqWZ-nucleof429zimg)

### HW Mods

I modified my perfboard and the Nucleo board to reduce the current load on the Atari 5v rail.

- Isolated STM32F767 5v rail and ST-LINK MCU 5v rail using diodes. When no USB is connected, ST-Link side is powered down.
- Removed SB111 on the Nucleo board to isolate the STM32F767 nRST line from ST-Link MCU
- Added 10K pull-up on nRST line (the Nucleo board did NOT have a pull-up on nRST!)

## More Photos

Some more photos of my build are [here.](/docs/photos.md)


## Original Project

This port was derived from the [DirtyHair source code.](https://github.com/DirtyHairy/UnoCart-2600)

The [original UnoCart project](https://github.com/robinhedwards/UnoCart-2600) is the work of
Robin Edwards (electrotrains on AtariAge).

The UnoCart code is open-source under GPLv3.


# Interesting Observations

Some observation I made on some of the 2600 games run via my UnoCart.

**Atari Moon Patrol:**

On the **Atari Moon Patrol** game when the game logo is animating, you can hold the UnoCart STM32 MCU
in reset and the Moon Patrol logo continues to animate. The logo animation must be executing out of
the 2600's internal RAM.


**MoonSweeper:**

When first loaded from the SD card, the **MoonSweeper** video is lacking vertical sync, while horizontal sync is OK.
Pressing the Reset switch on the 2600 console fixes the vertical sync. This is very repeatable.

Is this an issue with the TV I'm using, or something weird with the MoonSweeper game? Hmmm...
