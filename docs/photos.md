# Photos

Additional photos of my build.

## Front View

Front view of my UnoCart slotted into my Atari Heavy Sixer:

![Image: Front](images/myUnoCart-2600_front.jpg)

## Proto-Board

Some views of my proto board wiring.  This board routes the cartridge slot's
address and data signals, the SD card's SPI signals, and a few jumpers inputs
to the appropriate pins on the Nucleo-144 Morpho pins.

Top:
![Image: ProtoTop](images/myUnoCart-2600_ProtoTop.jpg)

Bottom:
![Image: ProtoBot](images/myUnoCart-2600_ProtoBottom.jpg)

